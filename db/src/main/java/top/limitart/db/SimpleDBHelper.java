/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.db.sql.SQL;
import top.limitart.db.sql.Select;
import top.limitart.util.TimeUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据库操作器
 *
 * @author hank
 * @version 2018/4/14 0014 16:47
 */
public class SimpleDBHelper implements DBHelper {
  private static Logger LOGGER = LoggerFactory.getLogger(SimpleDBHelper.class);
  private static long SLOW_SQL = 100;
  private final DataSource dataSource;

  /** @param dataSource */
  public SimpleDBHelper(DBDataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public DataSource dataSource() {
    return this.dataSource;
  }

  @Override
  public <T> List<T> selectList(Select select, QueryResult<T> handler) throws SQLException {
    try (Connection con = connection();
        PreparedStatement preparedStatement = select.withParams(con);
        ResultSet resultSet = preparedStatement.executeQuery()) {
      List<T> result = new ArrayList<>();
      while (resultSet.next()) {
        result.add(handler.parse(resultSet));
      }
      return result;
    }
  }

  @Override
  public <T> T selectOne(Select select, QueryResult<T> handler) throws SQLException {
    try (Connection con = connection();
        PreparedStatement preparedStatement = select.withParams(con);
        ResultSet resultSet = preparedStatement.executeQuery()) {
      if (resultSet.last()) {
        Conditions.args(
            resultSet.getRow() == 1, "multi result? sql:{}", preparedStatement.toString());
        return handler.parse(resultSet);
      }
      return null;
    }
  }

  @Override
  public <T extends SQL> int update(T sql) throws SQLException {
    try (Connection con = connection();
        PreparedStatement statement = sql.withParams(con)) {
      long now = TimeUtil.now();
      int i = statement.executeUpdate();
      if (TimeUtil.now() - now > SLOW_SQL) {
        LOGGER.warn("slow sql,{}", sql.sql());
      }
      return i;
    }
  }

  @Override
  public <T extends SQL> int updateBatchWithPrepare(List<T> sqls) throws SQLException {
    Conditions.args(sqls != null && !sqls.isEmpty());
    T t = sqls.get(0);
    try (Connection con = connection();
        PreparedStatement statement = t.withTemplate(con)) {
      long now = TimeUtil.now();
      int c = 0;
      for (int i = 0; i < sqls.size(); i++) {
        SQL sql = sqls.get(i);
        Conditions.args(
            sql.getClass() == t.getClass(),
            "batch operation must execute on same sql template!!!, {},{}",
            sql.getClass(),
            t.getClass());
        sql.fillWithParams(statement);
        statement.addBatch();
        if (i % 1000 == 0) {
          for (int executeBatch : statement.executeBatch()) {
            c += executeBatch;
          }
          statement.clearBatch();
        }
      }
      for (int executeBatch : statement.executeBatch()) {
        c += executeBatch;
      }
      if (TimeUtil.now() - now > SLOW_SQL) {
        LOGGER.warn("slow batch,{}", t.sql());
      }
      return c;
    }
  }

  @Override
  public <T extends SQL> int updateBatchWithoutPrepare(List<T> sqls) throws SQLException {
    Conditions.args(sqls != null && !sqls.isEmpty());
    try (Connection con = connection();
        Statement statement = con.createStatement()) {
      long now = TimeUtil.now();
      int c = 0;
      for (int i = 0; i < sqls.size(); i++) {
        SQL sql = sqls.get(i);
        statement.addBatch(sql.sql());
        if (i % 1000 == 0) {
          for (int executeBatch : statement.executeBatch()) {
            c += executeBatch;
          }
          statement.clearBatch();
        }
      }
      for (int executeBatch : statement.executeBatch()) {
        c += executeBatch;
      }
      if (TimeUtil.now() - now > SLOW_SQL) {
        LOGGER.warn("slow batch,{}", sqls.get(0).sql());
      }
      return c;
    }
  }

  @Override
  public void close() throws Exception {
    // DO NOTHING
  }
}
