/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.config;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.cache.URLTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import top.limitart.util.ObjectUtil;
import top.limitart.util.TimeUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * ConfigMeta生成器
 *
 * @author hank
 * @version 2018/12/28 0028 13:45
 */
public class ConfigMetaGenerator {
  private Configuration cfg;
  private Template javaTemplate;

  public ConfigMetaGenerator()  {
    cfg = new Configuration(Configuration.VERSION_2_3_28);
    TemplateLoader loader = new ClassTemplateLoader(ConfigMetaGenerator.class,"/");
    cfg.setTemplateLoader(loader);
  }

  public static void main(String[] args) throws IOException, TemplateException {
    ConfigMetaGenerator generator = new ConfigMetaGenerator();
    DataParser parser =
        new DataParser() {
          @Override
          protected void initDataType() {}
        };
    ExcelModel model = ExcelModel.loadFromExcel(parser, "./resources/dat_proto.xlsx");
    generator.javaCode("top.limitart.config", "./demo", model, SheetHead.CellHolder.SERVER);
  }

  private void checkJavaTemplate() throws IOException {
    if (javaTemplate == null) {
      javaTemplate = cfg.getTemplate("dat_model.ftl");
    }
  }

  /**
   * 生成Java模版
   *
   * @param packageName
   * @param outPath
   * @param model
   * @throws IOException
   */
  public void javaCode(
      String packageName, String outPath, ExcelModel model, SheetHead.CellHolder... holder)
      throws IOException, TemplateException {
    checkJavaTemplate();
    List<ConfigTemplateProto> protos = convertToProto(packageName, model, holder);
    for (ConfigTemplateProto info : protos) {
      File outDir =
          new File(outPath + File.separator + info.get_package().replace('.', File.separatorChar));
      if (!outDir.exists()) {
        outDir.mkdirs();
      } else if (!outDir.isDirectory()) {
        throw new IOException("need a directory!");
      }
      try (FileWriter writer =
          new FileWriter(new File(outDir.getPath() + File.separator + info.get_name() + ".java"))) {
        javaTemplate.process(info, writer);
      }
    }
  }

  private List<ConfigTemplateProto> convertToProto(
      String packageName, ExcelModel model, SheetHead.CellHolder... holder) {
    List<ConfigTemplateProto> protos = new LinkedList<>();
    for (SheetHead sheetHead : model.getHeads().values()) {
      protos.add(convertToProto0(packageName, model, sheetHead, holder));
    }
    return protos;
  }

  private ConfigTemplateProto convertToProto0(
      String packageName, ExcelModel model, SheetHead sheetHead, SheetHead.CellHolder... holder) {
    ConfigTemplateProto proto = new ConfigTemplateProto();
    proto.set_fileName(model.getExcelName());
    proto.set_lastModifiedTime(TimeUtil.mills2LocalDateTime(model.getLastModified()).toString());
    proto.set_package(packageName);
    proto.set_name(sheetHead.getSheetName());
    proto.set_explain(sheetHead.getSheetComment());
    proto.set_primary_name(sheetHead.getColumnHeads().get(0).getColumnName());
    proto.set_primary_type(
        ObjectUtil.toBoxName(sheetHead.getColumnHeads().get(0).getDataType().nameOfR()));
    for (SheetHead.ColumnHead columnHead : sheetHead.getColumnHeads()) {
      if (columnHead.isCommentColumn()) {
        continue;
      }
      if (columnHead.getHolders().noneTick(holder)) {
        continue;
      }
      ConfigTemplateProto.ColInfo colInfo = new ConfigTemplateProto.ColInfo();
      colInfo.set_name(columnHead.getColumnName());
      colInfo.set_explain(columnHead.getColumnComment());
      colInfo.set_type(columnHead.getDataType().nameOfR());
      proto.get_cols().add(colInfo);
    }
    return proto;
  }
  /**
   * 配置代码模版协议
   *
   * @author hank
   * @version 2018/12/28 0028 11:01
   */
  public static class ConfigTemplateProto {
    private String _fileName;
    private String _lastModifiedTime;
    private String _package;
    private String _name;
    private String _explain;
    private String _primary_type;
    private String _primary_name;
    private List<ColInfo> _cols = new ArrayList<>();

    public String get_primary_type() {
      return _primary_type;
    }

    public void set_primary_type(String _primary_type) {
      this._primary_type = _primary_type;
    }

    public String get_primary_name() {
      return _primary_name;
    }

    public void set_primary_name(String _primary_name) {
      this._primary_name = _primary_name;
    }

    public String get_lastModifiedTime() {
      return _lastModifiedTime;
    }

    public void set_lastModifiedTime(String _lastModifiedTime) {
      this._lastModifiedTime = _lastModifiedTime;
    }

    public String get_fileName() {
      return _fileName;
    }

    public void set_fileName(String _fileName) {
      this._fileName = _fileName;
    }

    public String get_package() {
      return _package;
    }

    public void set_package(String _package) {
      this._package = _package;
    }

    public String get_name() {
      return _name;
    }

    public void set_name(String _name) {
      this._name = _name;
    }

    public String get_explain() {
      return _explain;
    }

    public void set_explain(String _explain) {
      this._explain = _explain;
    }

    public List<ColInfo> get_cols() {
      return _cols;
    }

    public static class ColInfo {
      private String _name;
      private String _explain;
      private String _type;

      public String get_name() {
        return _name;
      }

      public void set_name(String _name) {
        this._name = _name;
      }

      public String get_explain() {
        return _explain;
      }

      public void set_explain(String _explain) {
        this._explain = _explain;
      }

      public String get_type() {
        return _type;
      }

      public void set_type(String _type) {
        this._type = _type;
      }
    }
  }
}
