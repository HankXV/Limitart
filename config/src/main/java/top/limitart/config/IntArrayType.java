/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/**
 * 一维Int数组<br>
 * 形如[1,2,3,4] Created by Hank on 2018/12/23
 */
public class IntArrayType extends JSONParsingType<int[]> {
  @Override
  public String name() {
    return "intarray";
  }

  @Override
  public String alias() {
    return "i1";
  }

  @Override
  public String defaultSourceIfNull() {
    return "[0]";
  }

  @Override
  public String nameOfR() {
    return "int[]";
  }

  @Override
  public Class<int[]> classOfR() {
    return int[].class;
  }

  @Override
  public void serialize(BinaryMeta buf, int[] value) throws Exception {
    buf.putIntArray(value);
  }

  @Override
  public int[] deserialize(BinaryMeta buf) throws Exception {
    return buf.getIntArray();
  }
}
