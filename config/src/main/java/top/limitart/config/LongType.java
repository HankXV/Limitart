/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/** Long类型 Created by Hank on 2018/12/23 */
public class LongType implements NumericSourceType<Long> {
  @Override
  public String name() {
    return "long";
  }

  @Override
  public String alias() {
    return "l";
  }

  @Override
  public Long parse(Double source) throws Exception {
    return source.longValue();
  }

  @Override
  public Double defaultSourceIfNull() {
    return 0d;
  }

  @Override
  public String nameOfR() {
    return "long";
  }

  @Override
  public Class<Long> classOfR() {
    return long.class;
  }

  @Override
  public void serialize(BinaryMeta buf, Long value) throws Exception {
    buf.putLong(value);
  }

  @Override
  public Long deserialize(BinaryMeta buf) throws Exception {
    return buf.getLong();
  }
}
