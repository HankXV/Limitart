/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/**
 * 二维Int数组<br>
 * 形如[[1,2,3],[2,3,4]] Created by Hank on 2018/12/23
 */
public class IntArray2DType extends JSONParsingType<int[][]> {
  @Override
  public String name() {
    return "intarray2d";
  }

  @Override
  public String alias() {
    return "i2";
  }

  @Override
  public String defaultSourceIfNull() {
    return "[[0,0]]";
  }

  @Override
  public String nameOfR() {
    return "int[][]";
  }

  @Override
  public Class<int[][]> classOfR() {
    return int[][].class;
  }

  @Override
  public void serialize(BinaryMeta buf, int[][] value) throws Exception {
    if (value == null) {
      buf.putShort(-1);
    } else {
      buf.putShort(value.length);
      for (int[] temp : value) {
        buf.putIntArray(temp);
      }
    }
  }

  @Override
  public int[][] deserialize(BinaryMeta buf) throws Exception {
    short length = buf.getShort();
    switch (length) {
      case -1:
        return null;
      case 0:
        return new int[0][];
      default:
        int[][] result = new int[length][];
        for (int i = 0; i < length; ++i) {
          result[i] = buf.getIntArray();
        }
        return result;
    }
  }
}
