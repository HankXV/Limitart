/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.config;

import top.limitart.base.Conditions;
import top.limitart.base.function.Process1;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据容器(相当于Excel的一张Sheet)
 *
 * @author hank
 * @version 2018/10/16 0016 14:51
 */
public class ConfigContainer<T extends ConfigMeta> {
  private List<T> list = new ArrayList<>();
  private Map<Object, T> map = new HashMap<>();

  public void putIfAbsent(@NotNull T v) throws Exception {
    Object primary = v.primaryKey();
    Conditions.args(
        !map.containsKey(primary), "{},primary key duplicated：%s", v.getClass(), primary);
    list.add(v);
    map.put(primary, v);
  }

  public void forEach(Process1<T> process) {
    this.list.forEach(process);
  }

  public List<T> copyList() {
    return new ArrayList<>(list);
  }

  public @Nullable T get(@NotNull Object primary) {
    return map.get(primary);
  }
}
