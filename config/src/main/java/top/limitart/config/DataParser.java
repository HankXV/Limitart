/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.Conditions;
import top.limitart.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/** 解析DataType的类 Created by Hank on 2018/12/25 */
public abstract class DataParser {
  // 数据类型注册
  private Map<String, DataType> dataTypes = new HashMap<>();
  private Map<String, DataType> dataTypeAlias = new HashMap<>();
  private Map<Class<?>, DataType> dataClasses = new HashMap<>();

  public DataParser() {
    // 把系统自带的类型注册了
    this.registerDataType(new BoolType())
        .registerDataType(new DoubleType())
        .registerDataType(new FloatType())
        .registerDataType(new IntArray2DType())
        .registerDataType(new IntArrayType())
        .registerDataType(new IntType())
        .registerDataType(new LongType())
        .registerDataType(new StringType());
    initDataType();
    for (String s : dataTypes.keySet()) {
      DataType dataType = dataTypeAlias.get(s);
      if (dataType != null) {
        DataType origin = dataTypes.get(s);
        Conditions.args(
            origin == dataType,
            "name and alias are same,but DataType not same,one %s another %s",
            origin.getClass().getName(),
            dataType.getClass().getName());
      }
    }
  }

  protected abstract void initDataType();
  /**
   * 注册一个数据类型，以便发现Excel里面的数据并解析
   *
   * @param dataType
   */
  protected DataParser registerDataType(DataType dataType) {
    Conditions.notNull(dataType);
    Conditions.args(StringUtil.notEmpty(dataType.name()), "data type name empty");
    Conditions.args(
        !dataTypes.containsKey(dataType.name()), "duplicated data type name %s", dataType.name());
    if (StringUtil.notEmpty(dataType.alias())) {
      Conditions.args(
          !dataTypeAlias.containsKey(dataType.alias()),
          "duplicated data type alias %s",
          dataType.alias());
      dataTypes.put(dataType.alias(), dataType);
    }
    Conditions.args(
        !dataClasses.containsKey(dataType.classOfR()),
        "class %s duplicated,one class,multi parser?",
        dataType.classOfR().getName());
    dataClasses.put(dataType.classOfR(), dataType);
    dataTypes.put(dataType.name(), dataType);
    return this;
  }

  /**
   * 通过类型名字查找DataType
   *
   * @param name
   * @return
   */
  public DataType findByName(String name) {
    DataType dataType = dataTypes.get(name);
    return dataType != null ? dataType : dataTypeAlias.get(name);
  }

  /**
   * 通过类型名字查找DataType
   *
   * @param name
   * @param alias
   * @return
   */
  public DataType findByName(String name, String alias) {
    DataType byName = findByName(name);
    return byName == null ? findByName(alias) : byName;
  }
  /**
   * 通过java类查找DataType
   *
   * @param clazz
   * @return
   */
  public DataType findByClass(Class<?> clazz) {
    return dataClasses.get(clazz);
  }
}
