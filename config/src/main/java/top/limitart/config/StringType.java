/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/** 字符串类型 Created by Hank on 2018/12/23 */
public class StringType implements StringSourceType<String> {
  @Override
  public String name() {
    return "string";
  }

  @Override
  public String alias() {
    return "s";
  }

  @Override
  public String parse(String source) throws Exception {
    return source;
  }

  @Override
  public String defaultSourceIfNull() {
    return "";
  }

  @Override
  public String nameOfR() {
    return String.class.getName();
  }

  @Override
  public Class<String> classOfR() {
    return String.class;
  }

  @Override
  public void serialize(BinaryMeta buf, String value) throws Exception {
    buf.putString(value);
  }

  @Override
  public String deserialize(BinaryMeta buf) throws Exception {
    return buf.getString();
  }
}
