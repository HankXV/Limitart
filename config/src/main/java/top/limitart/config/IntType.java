/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/** Integer类型 Created by Hank on 2018/12/23 */
public class IntType implements NumericSourceType<Integer> {
  @Override
  public String name() {
    return "int";
  }

  @Override
  public String alias() {
    return "i";
  }

  @Override
  public Integer parse(Double source) throws Exception {
    return source.intValue();
  }

  @Override
  public Double defaultSourceIfNull() {
    return 0d;
  }

  @Override
  public String nameOfR() {
    return "int";
  }

  @Override
  public Class<Integer> classOfR() {
    return int.class;
  }

  @Override
  public void serialize(BinaryMeta buf, Integer value) throws Exception {
    buf.putInt(value);
  }

  @Override
  public Integer deserialize(BinaryMeta buf) throws Exception {
    return buf.getInt();
  }
}
