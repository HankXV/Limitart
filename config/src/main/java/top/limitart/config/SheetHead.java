/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.config;

import top.limitart.base.MultiOption;
import top.limitart.util.StringUtil;

import java.util.List;

/**
 * 数据表头
 *
 * @author hank
 * @version 2018/12/24 0024 14:28
 */
public class SheetHead {
  // 表名
  private String sheetName;
  // 表解释
  private String sheetComment;
  // 列头
  private List<ColumnHead> columnHeads;

  public SheetHead(String sheetName, String sheetComment, List<ColumnHead> columnHeads) {
    this.sheetName = sheetName;
    this.sheetComment = sheetComment;
    this.columnHeads = columnHeads;
  }

  public String getSheetName() {
    return sheetName;
  }

  public String getSheetComment() {
    return sheetComment;
  }

  public List<ColumnHead> getColumnHeads() {
    return columnHeads;
  }

  /** 每一列的头信息 */
  public static class ColumnHead {
    // 数据类型
    private DataType dataType;
    // 列名称
    private String columnName;
    // 列解释
    private String columnComment;
    // 拥有者标记
    private MultiOption<CellHolder> holders;
    // 是否为注释列
    private boolean commentColumn;

    /** 空的列头(注释列) */
    public ColumnHead() {
      this.commentColumn = true;
    }

    /**
     * 实体列
     *
     * @param dataType
     * @param columnName
     * @param columnComment
     * @param holders
     */
    public ColumnHead(
        DataType dataType,
        String columnName,
        String columnComment,
        MultiOption<CellHolder> holders) {
      this.dataType = dataType;
      this.columnName = columnName;
      this.columnComment = columnComment;
      this.holders = holders;
    }

    public DataType getDataType() {
      return dataType;
    }

    public String getColumnName() {
      return columnName;
    }

    public String getColumnComment() {
      return columnComment;
    }

    public MultiOption<CellHolder> getHolders() {
      return holders;
    }

    public boolean isCommentColumn() {
      return commentColumn;
    }
  }

  /** 字段所属 */
  public enum CellHolder {
    /** 客户端 */
    CLIENT,
    /** 服务器 */
    SERVER,
    ;

    public static CellHolder[] all() {
      return CellHolder.values();
    }

    public static CellHolder[] findByName(String name) {
      if (StringUtil.empty(name) || "cs".equalsIgnoreCase(name)) {
        return CellHolder.values();
      }
      if ("c".equalsIgnoreCase(name)) {
        return new CellHolder[] {CellHolder.CLIENT};
      }
      if ("s".equalsIgnoreCase(name)) {
        return new CellHolder[] {CellHolder.SERVER};
      }
      return new CellHolder[0];
    }
  }
}
