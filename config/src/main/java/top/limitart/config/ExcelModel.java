/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.MultiOption;
import top.limitart.collections.ImmutableMap;
import top.limitart.util.FileUtil;
import top.limitart.util.StringUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Excel处理类 <br> 从Excel解析数个SheetHead <br> Created by Hank on 2018/12/24
 */
public class ExcelModel implements Closeable {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExcelModel.class);
  // 文件名
  private String excelName;
  // 文件最后修改时间
  private long lastModified;
  // 无数个表头
  private Map<String, SheetHead> heads;
  // 数据类型解析器
  private DataParser parser;

  private transient Workbook workBook;

  /**
   * 从文件加载Excel
   *
   * @param parser    数据解析器
   * @param excelFile
   * @return
   */
  public static ExcelModel loadFromExcel(DataParser parser, File excelFile) throws IOException {
    return new ExcelModel(parser, excelFile);
  }

  public static ExcelModel loadFromExcel(DataParser parser, String excelName, long lastModified,
      InputStream inputStream) throws IOException {
    return new ExcelModel(parser, excelName, lastModified, inputStream);
  }

  /**
   * 从文件路径加载Excel
   *
   * @param parser    数据解析器
   * @param excelPath
   * @return
   */
  public static ExcelModel loadFromExcel(DataParser parser, String excelPath) throws IOException {
    return new ExcelModel(parser, excelPath);
  }

  private ExcelModel(DataParser parser, String excelPath) throws IOException {
    this(parser, new File(excelPath));
  }

  private ExcelModel(DataParser parser, File excelFile) throws IOException {
    this(parser, excelFile.getName().split("[.]")[0], excelFile.lastModified(),
        createWorkBook(excelFile));
  }

  private ExcelModel(DataParser parser, String excelName, long lastModified,
      InputStream inputStream) throws IOException {
    this(parser, excelName, lastModified,
        createWorkBook(inputStream));
  }

  private ExcelModel(DataParser parser, String excelName, long lastModified, Workbook workBook) {
    this.parser = Conditions.notNull(parser);
    this.excelName = excelName;
    this.lastModified = lastModified;
    int numberOfSheets = workBook.getNumberOfSheets();
    Map<String, SheetHead> heads = new HashMap<>();
    for (int i = 0; i < numberOfSheets; ++i) {
      Sheet sheetAt = workBook.getSheetAt(i);
      parseSheet(sheetAt, heads);
    }
    this.heads = ImmutableMap.of(heads);
    this.workBook = workBook;
  }

  private void parseSheet(Sheet sheet, Map<String, SheetHead> heads) {
    // 解析表名
    String sheetName = sheet.getSheetName();
    // 解析表的第1行：表名注释
    String sheetComment = sheet.getRow(0).getCell(0).getStringCellValue();
    // 解析表的第2，3，4，5行，列头
    Row fieldNames = sheet.getRow(1);
    Row fieldExplains = sheet.getRow(2);
    Row fieldTypes = sheet.getRow(3);
    Row fieldHolders = sheet.getRow(4);
    // 以字段类型为准,如果没有则判定为注释字段
    List<SheetHead.ColumnHead> columnHeads = new ArrayList<>();
    short lastCellNum = fieldExplains.getLastCellNum();
    for (int c = 0; c < lastCellNum; ++c) {
      String name = fieldNames.getCell(c).getStringCellValue();
      String explain = fieldExplains.getCell(c).getStringCellValue();
      String type = fieldTypes.getCell(c).getStringCellValue();
      String holder = fieldHolders.getCell(c).getStringCellValue();
      // 如果4个都为空，则到此结束了
      if (StringUtil.empty(name)
          && StringUtil.empty(explain)
          && StringUtil.empty(type)
          && StringUtil.empty(holder)) {
        LOGGER.warn("find empty column in sheet %s,please check", sheetName);
        break;
      }
      if (StringUtil.empty(type) || StringUtil.empty(name)) {
        // 如果类型或者字段名为空，则没法把他当作一个正常字段，只能为注释列
        columnHeads.add(new SheetHead.ColumnHead());
        continue;
      }
      //  查找这个类型有没有实现
      DataType byName = parser.findByName(type);
      Conditions.notNull(
          byName, "can not find data type %s,please create a DataType for %s", type, type);
      SheetHead.CellHolder[] holders = SheetHead.CellHolder.findByName(holder);
      Conditions.args(
          holders.length > 0,
          "column %s must have one holder or above if it is not a comment-column",
          name);
      columnHeads.add(
          new SheetHead.ColumnHead(byName, name, explain, MultiOption.of().tick(holders)));
    }
    SheetHead head = new SheetHead(sheetName, sheetComment, columnHeads);
    heads.put(sheetName, head);
  }

  /**
   * 读取数据
   *
   * @param holder
   * @return
   */
  public Map<String, Object[][]> readData(SheetHead.CellHolder... holder) {
    int numberOfSheets = workBook.getNumberOfSheets();
    Map<String, Object[][]> datas = new HashMap<>();
    for (int i = 0; i < numberOfSheets; ++i) {
      Sheet sheet = workBook.getSheetAt(i);
      Conditions.args(
          !datas.containsKey(sheet.getSheetName()),
          "duplicated sheet name %s",
          sheet.getSheetName());
      datas.put(sheet.getSheetName(), parseRows(sheet, holder));
    }
    return datas;
  }

  private Object[][] parseRows(Sheet sheet, SheetHead.CellHolder... holder) {
    List<Object[]> list = new ArrayList<>();
    int lastRowNum = sheet.getLastRowNum();
    SheetHead head = heads.get(sheet.getSheetName());
    for (int i = 5; i <= lastRowNum; ++i) {
      Row contentRow = sheet.getRow(i);
      if (isRowEmpty(contentRow)) {
        LOGGER.warn("find empty row in sheet {}", sheet.getSheetName());
        continue;
      }
      list.add(parseRow(head, contentRow, holder));
    }
    return list.toArray(new Object[0][]);
  }

  /**
   * 解析每一行数据
   *
   * @param row
   */
  private Object[] parseRow(SheetHead head, Row row, SheetHead.CellHolder... holder) {
    // 每一行有多少数据
    List<Object> rowData = new ArrayList<>();
    for (int i = 0; i < head.getColumnHeads().size(); ++i) {
      SheetHead.ColumnHead columnHead = head.getColumnHeads().get(i);
      if (columnHead.isCommentColumn()) {
        continue;
      }
      if (columnHead.getHolders().noneTick(holder)) {
        continue;
      }
      Cell cell = row.getCell(i);
      DataType dataType = columnHead.getDataType();
      Object source = null;
      if (cell == null) {
        source = dataType.defaultSourceIfNull();
      } else {
        CellType cellType = cell.getCellType();
        switch (cellType) {
          case NUMERIC:
            source = 0;
            if (cell != null) {
              source = cell.getNumericCellValue();
            }
            break;
          case STRING:
            source = "";
            if (cell != null) {
              source = cell.getStringCellValue();
            }
            break;
          case BOOLEAN:
            source = false;
            if (cell != null) {
              source = cell.getBooleanCellValue();
            }
            break;
          case FORMULA:
          case BLANK:
          case ERROR:
          case _NONE:
            Conditions.args(
                false,
                "wrong cell type from sheet %s,only support Numeric and String",
                row.getSheet().getSheetName());
        }
      }
      try {
        Object parse = dataType.parse(source);
        rowData.add(parse);
      } catch (Exception e) {
        Conditions.args(
            false,
            "can not parse data %s with data type %s in sheet %s",
            source,
            dataType.name(),
            row.getSheet().getSheetName());
      }
    }
    return rowData.toArray(new Object[0]);
  }

  /**
   * 当前行是否为空行
   *
   * @param row
   * @return
   */
  private boolean isRowEmpty(Row row) {
    for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
      Cell cell = row.getCell(c);
      if (cell != null && cell.getCellType() != CellType.BLANK) {
        return false;
      }
    }
    return true;
  }

  private static Workbook createWorkBook(File file) throws IOException {
    Workbook workbook;
    String profix = FileUtil.getFileNameExtention(file.getName());
    // 97-03
    if (profix.equals("xls")) {
      workbook = new HSSFWorkbook(new FileInputStream(file));
    } else if (profix.equals("xlsx")) {
      workbook = new XSSFWorkbook(new FileInputStream(file));
    } else {
      throw new FileNotFoundException("not a xls or xlsx");
    }
    return workbook;
  }

  private static Workbook createWorkBook(InputStream stream) throws IOException {
    Workbook workbook;
    try {
      workbook = new HSSFWorkbook(stream);
    } catch (Exception e) {
      workbook = new XSSFWorkbook(stream);
    }
    return workbook;
  }

  public String getExcelName() {
    return excelName;
  }

  public long getLastModified() {
    return lastModified;
  }

  public Map<String, SheetHead> getHeads() {
    return heads;
  }

  @Override
  public void close() throws IOException {
    if (workBook != null) {
      workBook.close();
      workBook = null;
    }
  }
}
