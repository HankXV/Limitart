/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;

/** Bool类型 Created by Hank on 2018/12/23 */
public class BoolType implements DataType<Boolean, Boolean> {
  @Override
  public String name() {
    return "bool";
  }

  @Override
  public String alias() {
    return "b";
  }

  @Override
  public Boolean parse(Boolean source) throws Exception {
    return source;
  }

  @Override
  public Boolean defaultSourceIfNull() {
    return false;
  }

  @Override
  public String nameOfR() {
    return "boolean";
  }

  @Override
  public Class<Boolean> classOfR() {
    return boolean.class;
  }

  @Override
  public void serialize(BinaryMeta buf, Boolean value) throws Exception {
    buf.putBoolean(value);
  }

  @Override
  public Boolean deserialize(BinaryMeta buf) throws Exception {
    return buf.getBoolean();
  }
}
