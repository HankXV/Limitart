/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.config;

/**
 * 静态数据Bean<br>
 * 一个Bean代表一个Sheet的表头结构
 *
 * @author hank
 * @version 2018/12/26 0026 20:08
 */
public abstract class ConfigMeta<P> {
  /**
   * 获取主键
   *
   * @return
   */
  public abstract P primaryKey();
}
