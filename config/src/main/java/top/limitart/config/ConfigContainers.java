/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.config;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.BinaryMeta;
import top.limitart.base.Conditions;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 配置集合
 *
 * @author hank
 * @version 2018/12/28 0028 21:22
 */
public class ConfigContainers {
  private static Logger LOGGER = LoggerFactory.getLogger(ConfigContainers.class);
  private Map<Class<? extends ConfigMeta>, ConfigContainer> dats = new HashMap<>();

  /**
   * 将数据集合转化为可解析的二进制数据
   *
   * @param dataParser
   * @return
   */
  public byte[] toBytes(DataParser dataParser) throws Exception {
    BinaryMeta byteBuf = new BinaryMeta();
    for (Map.Entry<Class<? extends ConfigMeta>, ConfigContainer> entry : dats.entrySet()) {
      Class<? extends ConfigMeta> javaClass = entry.getKey();
      // 写入容器名称
      byteBuf.putString(javaClass.getSimpleName());
      Field[] declaredFields = javaClass.getDeclaredFields();
      ConfigContainer value = entry.getValue();
      List list = value.copyList();
      byteBuf.putShort(list.size());
      for (Object o : list) {
        for (Field declaredField : declaredFields) {
          declaredField.setAccessible(true);
          Class<?> type = declaredField.getType();
          DataType byClass = dataParser.findByClass(type);
          Conditions.notNull(
              byClass, "can not find DataType from parser by java class %s", javaClass.getName());
          byteBuf.putString(byClass.name());
          byteBuf.putString(byClass.alias());
          byClass.serialize(byteBuf, declaredField.get(o));
        }
      }
    }
    byte[] result = new byte[byteBuf.buffer().readableBytes()];
    byteBuf.buffer().readBytes(result);
    byteBuf.close();
    return result;
  }

  /**
   * 从二进制数据解析为数据集合
   *
   * @param dataParser
   * @param bytes
   * @return
   */
  public static ConfigContainers loadFromBtyes(
      String packageName, DataParser dataParser, byte[] bytes) throws Exception {
    ConfigContainers configContainers = new ConfigContainers();
    ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer();
    byteBuf.writeBytes(bytes);
    BinaryMeta buf = new BinaryMeta(byteBuf);
    while (buf.buffer().readableBytes() > 0) {
      ConfigContainer configContainer = new ConfigContainer();
      String beanName = buf.getString();
      Class<ConfigMeta> aClass = (Class<ConfigMeta>) Class.forName(packageName + "." + beanName);
      Field[] declaredFields = aClass.getDeclaredFields();
      short length = buf.getShort();
      for (int i = 0; i < length; ++i) {
        ConfigMeta o = aClass.newInstance();
        for (Field declaredField : declaredFields) {
          declaredField.setAccessible(true);
          String name = buf.getString();
          String alias = buf.getString();
          DataType byName = dataParser.findByName(name, alias);
          Conditions.notNull(
              byName, "can not find DataType from parser by java class %s,%s", name, alias);
          declaredField.set(o, byName.deserialize(buf));
        }
        configContainer.putIfAbsent(o);
      }
      configContainers.dats.put(aClass, configContainer);
    }
    return configContainers;
  }

  /**
   * 从ExcelModel开始加载数据
   *
   * @param packageName
   * @param modelList
   * @param holder
   * @return
   * @throws Exception
   */
  public static ConfigContainers loadFromExcelModels(
      String packageName, List<ExcelModel> modelList, SheetHead.CellHolder... holder)
      throws Exception {
    ConfigContainers configContainers = new ConfigContainers();
    for (ExcelModel model : modelList) {
      Map<String, Object[][]> map = model.readData(holder);
      for (Map.Entry<String, Object[][]> entry : map.entrySet()) {
        String sheetName = entry.getKey();
        Class<? extends ConfigMeta> beanClss =
            (Class<? extends ConfigMeta>) Class.forName(packageName + "." + sheetName);
        Field[] declaredFields = beanClss.getDeclaredFields();
        Object[][] sheetValue = entry.getValue();
        ConfigContainer<ConfigMeta> configContainer = new ConfigContainer<>();
        for (Object[] rowData : sheetValue) {
          ConfigMeta o = beanClss.newInstance();
          for (int i = 0; i < rowData.length; i++) {
            Object cell = rowData[i];
            Field field = declaredFields[i];
            if (!field.isAccessible()) {
              field.setAccessible(true);
            }
            field.set(o, cell);
          }
          configContainer.putIfAbsent(o);
        }
        configContainers.dats.put(beanClss, configContainer);
      }
    }
    return configContainers;
  }

  public <T extends ConfigMeta> ConfigContainer<T> getContainer(Class<T> beanClass) {
    return dats.get(beanClass);
  }
}
