/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.config;

import top.limitart.base.BinaryMeta;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.Nullable;

/**
 * 数据类型 Created by Hank on 2018/12/23
 *
 * @param <S> 原始数据类型
 * @param <R> 解析后的数据类型
 */
public interface DataType<S, R> {

  /**
   * 数据类型名称(如int,String等)
   *
   * @return
   */
  @NotNull
  String name();

  /**
   * 别名
   *
   * @return
   */
  @Nullable
  String alias();

  /**
   * 解析
   *
   * @param source
   * @return
   */
  @Nullable
  R parse(@Nullable S source) throws Exception;

  @Nullable
  S defaultSourceIfNull();

  /**
   * 泛型R的类名(模版生成用)
   *
   * @return
   */
  String nameOfR();

  /**
   * 泛型R对应的类
   *
   * @return
   */
  Class<R> classOfR();

  /**
   * 将T写入buf中
   *
   * @param buf
   * @param value
   * @throws Exception
   */
  void serialize(BinaryMeta buf, R value) throws Exception;

  /**
   * 冲buf中读出T
   *
   * @param buf
   * @return
   * @throws Exception
   */
  R deserialize(BinaryMeta buf) throws Exception;
}
