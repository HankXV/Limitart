package top.limitart.project.db;

import top.limitart.project.Manager;
import top.limitart.singleton.Singleton;

@Singleton
public class DBManager implements Manager {
    @Override
    public void onStart() {
        System.out.println("DBManager onStart");
    }

    @Override
    public void onReload() {
        System.out.println("DBManager onReload");
    }

    @Override
    public void onDestroy() {
        System.out.println("DBManager onDestroy");
    }
}
