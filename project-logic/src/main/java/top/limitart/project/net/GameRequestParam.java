package top.limitart.project.net;

import top.limitart.net.binary.BinaryRequestParam;
import top.limitart.project.role.Role;

public class GameRequestParam extends BinaryRequestParam {
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
