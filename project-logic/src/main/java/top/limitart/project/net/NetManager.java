package top.limitart.project.net;

import top.limitart.mapping.Router;
import top.limitart.net.AddressPair;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.binary.BinaryEndPoint;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.project.Manager;
import top.limitart.project.Managers;
import top.limitart.project.role.Role;
import top.limitart.project.role.msg.RoleLoginMessage;
import top.limitart.singleton.Singleton;

@Singleton
public class NetManager implements Manager {
    private BinaryEndPoint endPoint;

    @Override
    public void onStart() throws Exception {
        System.out.println("NetManager onStart");
        //这里初始化网络，当然正式环境下，我们不应该这么写，要保证服务器开启完成才能打开网络接口
        endPoint = BinaryEndPoint
                //要联网的服务器端
                .builder(NettyEndPointType.SERVER_REMOTE)
                //取个名字
                .name("logic-net")
                //注册路由器 [消息Class类型,处理器参数Class类型]
                .router(Router.create(BinaryMessage.class, GameRequestParam.class, "top.limitart.project", (o) -> Managers.getInstance().instance(o)))
                //当收到消息时，会传 消息，会话和路由器让你来自定义行为，如果不监听消息接收事件，则路由器自行在当前线程触发方法的执行
                .onMessageIn((msg, session, router)-> {
                    //这里强转为创建router时自定义的路由器上下文,当然如果你所有方法都不关心上下文，也不必设置
                    GameRequestParam gameRequestParam = (GameRequestParam) router.contextInstance();
                    gameRequestParam.session(session);
                    //这里可以给上下文自定义一些参数
                    gameRequestParam.setRole(new Role());
                    //这里自定义执行路由方法的时机，可以立马执行，可以分发到不同线程执行
                    router.request(msg, gameRequestParam, methodInvoker -> methodInvoker.invoke());
                })
                .build();
        endPoint.start(AddressPair.withPort(8888));
        //这里是实验，马上创建一个客户端来通信
        BinaryEndPoint.client()
                .name("logic-net-client")
                //这里客户端暂时不处理消息，所以创建一个空的路由器
                .router(Router.empty(BinaryMessage.class, GameRequestParam.class))
                .onConnected((session,b)->{
            if(b){
                RoleLoginMessage msg = new RoleLoginMessage();
                msg.username="limitart";
                session.writeNow(msg);
            }
        }).build().start(AddressPair.withLocalHost(8888));
    }

    @Override
    public void onReload() {
        System.out.println("NetManager onReload");
    }

    @Override
    public void onDestroy() {
        System.out.println("NetManager onDestroy");
    }
}
