package top.limitart.project.role.msg;

import top.limitart.net.binary.BinaryMessage;

public class RoleLoginMessage extends BinaryMessage {
    public String username;
    public String password;

    @Override
    public short id() {
        return 0;
    }
}
