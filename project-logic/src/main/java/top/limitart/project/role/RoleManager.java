package top.limitart.project.role;

import top.limitart.mapping.Mapper;
import top.limitart.mapping.Request;
import top.limitart.project.Manager;
import top.limitart.project.net.GameRequestParam;
import top.limitart.project.role.msg.RoleLoginMessage;
import top.limitart.singleton.Singleton;

//表明单例
@Singleton
//表明需要网络路由的类
@Mapper
public class RoleManager implements Manager {
    //这里处理网络消息,我关心哪个消息，我就创建这个消息的处理函数,下面是3种参数的方式，Request为必须param为可选
    public void login(@Request RoleLoginMessage msg, GameRequestParam param){
        System.out.println(msg.username+" login!!!!");
    }
//    public void login(GameRequestParam param, @Request RoleLoginMessage msg){
//        System.out.println(msg.username+" login!!!!");
//    }
//    public void login(@Request RoleLoginMessage msg){
//        System.out.println(msg.username+" login!!!!");
//    }
    // 当然一个消息不能处理多次
//    public void login1(@Request RoleLoginMessage msg, GameRequestParam param){
//        System.out.println(msg.username+" login!!!!");
//    }
    @Override
    public void onStart() {
        System.out.println("RoleManager onStart");
    }

    @Override
    public void onReload() {
        System.out.println("RoleManager onReload");
    }

    @Override
    public void onDestroy() {
        System.out.println("RoleManager onDestroy");
    }
}
