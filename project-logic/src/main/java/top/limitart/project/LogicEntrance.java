package top.limitart.project;

import top.limitart.redefinable.RedefinableApplicationContext;
import top.limitart.redefinable.RedefinableModule;

/**
 * 逻辑模块入口类
 */
public class LogicEntrance extends RedefinableModule {
    @Override
    public void onStart(RedefinableApplicationContext context) {
        System.out.println("onStart");
        //这里可以初始化所有单例了
        Managers.getInstance().init();
        Managers.getInstance().onStart();
    }

    @Override
    public void onRedefined(RedefinableApplicationContext context) {
        System.out.println("onRedefined");
        Managers.getInstance().onReload();
    }

    @Override
    public void onDestroy(RedefinableApplicationContext context) {
        System.out.println("onDestroy");
        Managers.getInstance().onDestroy();
    }
}
