package top.limitart.project;

public interface Manager {
    void onStart() throws Exception;

    void onReload() throws Exception;

    void onDestroy() throws Exception;
}
