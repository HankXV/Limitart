package top.limitart.project;

import top.limitart.base.Singleton;
import top.limitart.singleton.Singletons;


public class Managers implements Manager {
    private static Singleton<Managers> INSTANCE = Singleton.create(false, () -> new Managers());
    private Singletons singletons;

    public static Managers getInstance() {
        return INSTANCE.get();
    }

    public void init() {
        singletons = Singletons.builder().withClassStarter(Managers.class).build();
    }

    public <T> T instance(Class<T> clzz) {
        return singletons.instance(clzz);
    }

    @Override
    public void onStart() {
        singletons.forEach(o -> {
            if (o instanceof Manager) {
                try {
                    ((Manager) o).onStart();
                } catch (Exception e) {
                    e.printStackTrace();
                    //理论上应该调用System.exit
                }
            }
        });
    }

    @Override
    public void onReload() {
        singletons.forEach(o -> {
            if (o instanceof Manager) {
                try {
                    ((Manager) o).onReload();
                } catch (Exception e) {
                    e.printStackTrace();
                    //理论上应该调用System.exit
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        singletons.forEach(o -> {
            if (o instanceof Manager) {
                try {
                    ((Manager) o).onDestroy();
                } catch (Exception e) {
                    e.printStackTrace();
                    //理论上应该调用System.exit
                }
            }
        });
    }
}
