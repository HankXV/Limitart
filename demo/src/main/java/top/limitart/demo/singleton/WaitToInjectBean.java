package top.limitart.demo.singleton;

import top.limitart.singleton.Ref;

public class WaitToInjectBean {

  @Ref
  SingletonA singletonA;
  private int a;
  private String b;

  public WaitToInjectBean(int a, String b) {
    this.a = a;
    this.b = b;
  }

  public int getA() {
    return a;
  }

  public String getB() {
    return b;
  }
}
