package top.limitart.demo.singleton;

import top.limitart.singleton.Ref;

public class OtherBean {

  @Ref
  SingletonA singletonA;

  @Ref
  public OtherBean(SingletonB singletonB) {
    System.out.println("c b");
  }
}
