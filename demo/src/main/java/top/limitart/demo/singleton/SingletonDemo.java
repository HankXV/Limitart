package top.limitart.demo.singleton;

import top.limitart.demo.singleton.SingletonA;
import top.limitart.demo.singleton.SingletonB;
import top.limitart.demo.singleton.SingletonC;
import top.limitart.singleton.Ref;
import top.limitart.singleton.Singletons;
import top.limitart.singleton.Singletons.Builder;

public class SingletonDemo {
  @Ref
  SingletonB singletonB;
  @Ref
  SingletonA singletonA;
  @Ref
  SingletonC singletonC;

  public void say() {
    singletonB.say();
    singletonA.say();
  }

  public static void main(String[] args) throws InstantiationException, IllegalAccessException {
    Singletons build = new Builder()
        .withPackage("top.limitart", SingletonDemo.class.getClassLoader())
        .bind(SingletonDemo.class)
        .build();
    SingletonDemo instance = build.instance(SingletonDemo.class);
    instance.say();
    OtherBean bean = build.createBean(OtherBean.class);
    System.out.println(bean);
    WaitToInjectBean waitToInjectBean = new WaitToInjectBean(1,"hank");
    build.injectForInstance(waitToInjectBean);
  }
}
