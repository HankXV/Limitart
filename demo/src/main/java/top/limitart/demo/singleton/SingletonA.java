package top.limitart.demo.singleton;

import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;

@Singleton
public class SingletonA {
  @Ref
  SingletonB singletonB;

  public void say() {
    System.out.println(singletonB);
  }
}
