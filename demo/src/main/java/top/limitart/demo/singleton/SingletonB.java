package top.limitart.demo.singleton;

import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;

@Singleton
public class SingletonB {
  @Ref
  SingletonA singletonA;
  @Ref
  SingletonC singletonC;
  @Ref
  SingletonD singletonD;

  public void say() {
    System.out.println(singletonA);
    System.out.println(singletonC);
    System.out.println(singletonD);
  }
}
