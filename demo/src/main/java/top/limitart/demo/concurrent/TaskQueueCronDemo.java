package top.limitart.demo.concurrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import top.limitart.concurrent.TaskQueue;
import top.limitart.concurrent.TaskQueueGroup;

/**
 * @author hank
 * @version 2018/12/11 0011 13:55
 */
public class TaskQueueCronDemo {
  private static Logger LOGGER = LoggerFactory.getLogger(TaskQueueCronDemo.class);

  public static void main(String[] args) throws ParseException, ExecutionException, InterruptedException {
    TaskQueue queue = TaskQueue.create("test");
    queue.execute(()-> System.out.println("execute"));
    queue.submit(()-> System.out.println("submit")).get();
    queue.schedule(()-> System.out.println("schedule"), 1, TimeUnit.SECONDS);
    queue.schedule("cron1", "0 22 14 11 12 ? 2018", () -> System.out.println("tick1"));
    queue.schedule("cron1", "*/5 * * * * ? *", () -> System.out.println("tick2"));
    queue.schedule("cron1", "0 * * * * ? *", () -> System.out.println("tick3"));

    TaskQueueGroup group = new TaskQueueGroup("limitart",5,s->TaskQueue.create(s));
    group.next().execute(()-> System.out.println("execute1"));
    group.next().execute(()-> System.out.println("execute2"));
    group.next().execute(()-> System.out.println("execute3"));
    group.next().execute(()-> System.out.println("execute4"));
  }
}
