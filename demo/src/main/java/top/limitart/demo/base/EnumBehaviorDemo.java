package top.limitart.demo.base;

import top.limitart.base.EnumBehavior;

/**
 * @author hank
 * @version 2019/5/30 0030 17:28
 */
public enum EnumBehaviorDemo implements EnumBehavior {
  A,
  B,
  C,
}
