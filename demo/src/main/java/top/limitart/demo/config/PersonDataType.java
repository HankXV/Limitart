/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.demo.config;

import top.limitart.base.BinaryMeta;
import top.limitart.config.JSONParsingType;

/** Created by Hank on 2018/12/23 */
public class PersonDataType extends JSONParsingType<Person> {
  @Override
  public String name() {
    return "Person";
  }

  @Override
  public String alias() {
    return "person";
  }

  @Override
  public String nameOfR() {
    return Person.class.getName();
  }

  @Override
  public Class<Person> classOfR() {
    return Person.class;
  }

  @Override
  public void serialize(BinaryMeta buf, Person value) {}

  @Override
  public Person deserialize(BinaryMeta buf) throws Exception {
    return buf.getMessageMeta(Person.class);
  }
}
