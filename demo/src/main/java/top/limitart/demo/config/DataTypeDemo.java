/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.demo.config;

import top.limitart.config.IntArray2DType;
import top.limitart.config.IntArrayType;
import top.limitart.json.JSON;
import top.limitart.util.CollectionUtil;

/** Created by Hank on 2018/12/23 */
public class DataTypeDemo {
  public static void main(String[] args) throws Exception {
    Person person = new Person();
    person.setName("hank");
    person.setAge(18);
    String source = JSON.getDefault().toStr(person);
    PersonDataType parser = new PersonDataType();
    Person parse = parser.parse(source);
    System.out.println(parse);

    String intArray = "[1,2,3,4,5]";
    IntArrayType intArrayType = new IntArrayType();
    int[] parse1 = intArrayType.parse(intArray);
    System.out.println(CollectionUtil.toString(parse1));

    String intArray2 = "[[1,2,3],[2,3,4]]";
    IntArray2DType intArray2DType = new IntArray2DType();
    int[][] parse2 = intArray2DType.parse(intArray2);
    System.out.println(CollectionUtil.toString(parse2));
  }
}
