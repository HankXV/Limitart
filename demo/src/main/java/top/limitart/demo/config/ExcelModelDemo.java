package top.limitart.demo.config;

import java.util.Collections;
import java.util.Map;
import top.limitart.config.ConfigContainers;
import top.limitart.config.DataParser;
import top.limitart.config.ExcelModel;
import top.limitart.config.SheetHead;

/**
 * @author hank
 * @version 2018/12/26 0026 21:20
 */
public class ExcelModelDemo {
  public static void main(String[] args) throws Exception {
    DataParser parser =
        new DataParser() {
          @Override
          protected void initDataType() {}
        };
    ExcelModel excelModel =
        ExcelModel.loadFromExcel(
            parser, ExcelModel.class.getClassLoader().getResource("./dat_proto.xlsx").getPath());
    Map<String, Object[][]> stringMap = excelModel.readData(SheetHead.CellHolder.all());
    ConfigContainers configContainers =
        ConfigContainers.loadFromExcelModels(
            "top.limitart.config",
            Collections.singletonList(excelModel),
            SheetHead.CellHolder.SERVER);
    byte[] bytes = configContainers.toBytes(parser);
    ConfigContainers configContainers1 =
        ConfigContainers.loadFromBtyes("top.limitart.config", parser, bytes);
    excelModel.close();
  }
}
