/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.limitart.demo.config;

import top.limitart.config.ConfigMeta;

/**
 * 胜利场数排行奖励 from dat_proto at 2018-12-24T14:33:18.841
 *
 * @author limitart
 */
public class D_WinRank extends ConfigMeta<Integer> {
  /** 起始排名 */
  private int startRank;
  /** 奖励数量 */
  private long count;
  /** 整型数组测试 */
  private int[] array;
  /** 整型二维数组测试 */
  private int[][] array2;
  /** 起始排名 */
  public int getStartRank() {
    return this.startRank;
  }
  /** 奖励数量 */
  public long getCount() {
    return this.count;
  }
  /** 整型数组测试 */
  public int[] getArray() {
    return this.array;
  }
  /** 整型二维数组测试 */
  public int[][] getArray2() {
    return this.array2;
  }

  @Override
  public Integer primaryKey() {
    return this.startRank;
  }
}
