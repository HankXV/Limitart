package top.limitart.demo.event;

import top.limitart.event.Event;

/**
 * @author hank
 * @version 2019/3/25 0025 14:48
 */
public class EventDemo implements Event {
  private String name;

  public EventDemo(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
