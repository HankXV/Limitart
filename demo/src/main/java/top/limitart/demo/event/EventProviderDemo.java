package top.limitart.demo.event;

import top.limitart.concurrent.TaskQueue;
import top.limitart.event.AbstractEventProvider;
import top.limitart.event.Event;
import top.limitart.event.EventProvider;

/**
 * @author hank
 * @version 2019/3/25 0025 14:49
 */
public class EventProviderDemo extends AbstractEventProvider {
  public static void main(String[] args) {
    TaskQueue executor = TaskQueue.create("event demo thread");
    EventProvider demo = new EventProviderDemo();
    int listen =
        demo.listen(
            EventDemo.class,
            null,
            e -> System.out.println(Thread.currentThread().getName() + "," + e.getName()));
    demo.listen(
        EventDemo.class,
        executor,
        e -> System.out.println(Thread.currentThread().getName() + "," + e.getName()));
    demo.notListen(EventDemo.class, listen);
    demo.post(new EventDemo("event demo"));
    demo.post(new EventDemo("event demo1"));
  }

  @Override
  protected boolean onPost(Event event) {
    return true;
  }
}
