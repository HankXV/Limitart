package top.limitart.demo.redefinable;

import top.limitart.redefinable.RedefinableApplicationContext;
import top.limitart.redefinable.RedefinableModule;

public class ScriptEntrance extends RedefinableModule {
    private HelloRedefine helloRedefine = new HelloRedefine();
    @Override
    public void onStart(RedefinableApplicationContext context) {
        System.out.println("onStart");
        helloRedefine.hello();
    }

    @Override
    public void onRedefined(RedefinableApplicationContext context) {
        System.out.println("onRedefined");
        helloRedefine.hello();
    }

    @Override
    public void onDestroy(RedefinableApplicationContext context) {
        System.out.println("onDestroy");
    }
}
