package top.limitart.demo.redefinable;

import java.io.File;
import top.limitart.redefinable.RedefinableApplication;

public class RedefinableDemo  {
    public static void main(String[] args) throws Exception {
        RedefinableApplication demo = new RedefinableApplication(new File("e://script.jar").toURI(),"top.limitart.redefinable.ScriptEntrance");
        demo.run(args);
    }
}
