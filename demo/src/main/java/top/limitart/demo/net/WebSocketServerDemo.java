/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.demo.net;

import io.netty.channel.EventLoop;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import top.limitart.net.AddressPair;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.NettySession;
import top.limitart.net.Session;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.net.http.HTTPRequest;
import top.limitart.net.http.HTTPSession;
import top.limitart.net.websocket.WebSocketEndPoint;

/** Created by Hank on 2019/2/24 */
public class WebSocketServerDemo {
  public static void main(String[] args) throws Exception {
    new WebSocketEndPoint("web-socket-server", "/", NettyEndPointType.SERVER_REMOTE, 0, 0) {
      @Override
      protected void exceptionThrown(NettySession<Object> session, Throwable cause)
          throws Exception {}

      @Override
      protected void sessionActive(NettySession<Object> session, boolean activeOrNot)
          throws Exception {}

      @Override
      protected void onWebSocketFrameReceived(
          Session<Object, EventLoop> session, WebSocketFrame msg) {

      }

      @Override
      protected void onHttpRequestReceived(HTTPSession session, HTTPRequest msg) {
        System.out.println(msg);
      }
    }.start(AddressPair.withPort(8888));
  }
}
