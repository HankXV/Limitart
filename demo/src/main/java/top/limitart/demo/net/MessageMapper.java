/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.demo.net;

import com.google.protobuf.Message;
import io.netty.channel.EventLoop;
import top.limitart.demo.net.BinaryMessageDemo;
import top.limitart.mapping.Mapper;
import top.limitart.mapping.Request;
import top.limitart.net.Session;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.net.binary.BinaryRequestParam;
import top.limitart.net.protobuf.ProtobufRequestParam;

/** Created by Hank on 2018/10/13 */
@Mapper
public class MessageMapper {
  public void doMessageDemo(@Request BinaryMessageDemo msg,BinaryRequestParam param) {
    Session<BinaryMessage, EventLoop> session = param.session();
    System.out.println(msg.content);
    session.writeNow(msg);
  }

  public void doMessageDemo2(@Request ProtobufMessageDemo.Demo2 msg, ProtobufRequestParam param) {
    Session<Message, EventLoop> session = param.session();
    System.out.println("demo2:" + msg.getId());
  }

  public void doMessageDemo1(@Request ProtobufMessageDemo.Demo1 msg, ProtobufRequestParam param) {
    Session<Message, EventLoop> session = param.session();
    System.out.println("demo1:" + msg.getName());
  }
}
