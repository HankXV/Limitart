package top.limitart.demo.mapping;

import top.limitart.mapping.Mapper;
import top.limitart.mapping.Request;

/**
 * @author hank
 * @version 2018/10/8 0008 21:23
 */
@Mapper
public class StringMapperClass {
  public void onMsg(@Request StringRequest msg, StringQuestParam param) {
    System.out.println(msg.getMsg());
  }
}
