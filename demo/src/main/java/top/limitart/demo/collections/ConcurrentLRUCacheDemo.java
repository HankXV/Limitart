package top.limitart.demo.collections;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import top.limitart.collections.ConcurrentLRUCache;

/**
 * @author hank
 * @version 2018/11/13 0013 21:31
 */
public class ConcurrentLRUCacheDemo {
  public static void main(String[] args) throws InterruptedException {
    ConcurrentLRUCache<Integer, CacheObj> cache = new ConcurrentLRUCache<>(5, true);
    ExecutorService executorService = Executors.newCachedThreadPool();
    AtomicInteger counter = new AtomicInteger(0);
    for (; ; ) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      executorService.execute(
          () -> {
            int andIncrement = counter.getAndIncrement();
            cache.put(andIncrement, new CacheObj(andIncrement));
          });
    }
  }

  public static class CacheObj extends ConcurrentLRUCache.LRUCacheable {
    private int value;

    public CacheObj(int value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return "CacheObj{" + "value=" + value + '}';
    }

    @Override
    protected long aliveTime() {
      return 1000;
    }

    @Override
    protected boolean removable0() {
      return true;
    }

    @Override
    public void onRemoved(int currentSize) {
      System.out.println("removed " + this);
    }
  }
}
