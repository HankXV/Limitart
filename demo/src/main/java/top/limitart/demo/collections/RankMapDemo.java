package top.limitart.demo.collections;

import top.limitart.base.CompareChain;

import java.util.Comparator;
import top.limitart.collections.RankMap;

public class RankMapDemo {
  private static final Comparator<Bean> COMPARATOR =
      CompareChain.build(
          (o1, o2) -> CompareChain.start(o2.price * o2.item.getNum(), o1.price * o1.item.num));

  public static void main(String[] args) {
    RankMap<Long, Bean> rankMap = RankMap.create(COMPARATOR, 100);
    Item i1 = new Item(2);
    Bean b1 = new Bean(1, i1, 100);
    Item i2 = new Item(3);
    Bean b2 = new Bean(2, i2, 100);
    Item i3 = new Item(3);
    Bean b3 = new Bean(3, i3, 500);
    Item i4 = new Item(3);
    rankMap.putIfAbsent(b1);
    rankMap.putIfAbsent(b2);
    System.out.println(rankMap.getAll());
    rankMap.update(b1.key(), v -> v.price = 200);
    System.out.println(rankMap.getAll());
    rankMap.putIfAbsent(b3);
    System.out.println(rankMap.getAll());
    rankMap.updateOrPut(4L, v -> v.price = 1, () -> new Bean(4, i4, 1));
    System.out.println(rankMap.getAll());
  }

  public static class Bean implements RankMap.LongRankObj {
    private long id;
    private Item item;
    private int price;

    public Bean(long id, Item item, int price) {
      this.item = item;
      this.price = price;
      this.id = id;
    }

    public Bean copy() {
      return new Bean(this.id, this.item.copy(), this.price);
    }

    @Override
    public Long key() {
      return id;
    }

    @Override
    public String toString() {
      return "Bean{" + "id=" + id + ", item=" + item + ", price=" + price + '}';
    }
  }

  public static class Item {
    private int num;

    public Item copy() {
      return new Item(num);
    }

    public Item(int num) {
      this.num = num;
    }

    public int getNum() {
      return num;
    }

    public void setNum(int num) {
      this.num = num;
    }

    @Override
    public String toString() {
      return "Item{" + "num=" + num + '}';
    }
  }
}
