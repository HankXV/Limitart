package top.limitart.redefinable;


public class FakeRedefinableApplication extends RedefinableApplication {
    public FakeRedefinableApplication(String entranceClassName) {
        super(null, entranceClassName);
    }

    @Override
    protected RedefinableApplicationContext createContext() {
        return new RedefinableApplicationContext(FakeRedefinableApplication.class.getClassLoader());
    }
}
