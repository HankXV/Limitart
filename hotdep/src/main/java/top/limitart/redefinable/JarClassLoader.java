/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.redefinable;

/**
 * 解析jar包的加载器
 *
 * @author hank
 * @version 2019/8/1 0001 22:10
 */
public class JarClassLoader extends ClassLoader {
  private JarClassNameAndBytes content;

  public JarClassLoader(ClassLoader parent, JarClassNameAndBytes source) {
    super(parent);
    this.content = source;
  }

  @Override
  protected Class<?> findClass(String name) throws ClassNotFoundException {
    byte[] bytes = content.getClass(name);
    if (bytes == null) {
      throw new ClassNotFoundException("no byte source for class " + name);
    }
    return defineClass(name, bytes, 0, bytes.length);
  }
}
