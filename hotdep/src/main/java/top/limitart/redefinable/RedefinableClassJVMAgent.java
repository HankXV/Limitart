/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.redefinable;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Couple;
import top.limitart.base.UncatchableException;
import top.limitart.base.label.CallerSensitive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.management.ManagementFactory;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * 当前程序的代理
 *
 * @author hank
 * @version 2019/8/1 0001 20:31
 */
public class RedefinableClassJVMAgent {
  private static final Logger LOGGER = LoggerFactory.getLogger(RedefinableClassJVMAgent.class);
  private static final Couple<Attributes.Name, Object> MANIFEST_VERSION =
      Couple.ofImmutable(Attributes.Name.MANIFEST_VERSION, "1.0");
  private static final Couple<Attributes.Name, Object> AGENT_CLASS =
      Couple.ofImmutable(
          new Attributes.Name("Agent-Class"), RedefinableClassJVMAgent.class.getName());
  private static final Couple<Attributes.Name, Object> CAN_RETRANSFORM_CLASSES =
      Couple.ofImmutable(new Attributes.Name("Can-Retransform-Classes"), "true");
  private static final Couple<Attributes.Name, Object> CAN_REDEFINE_CLASSES =
      Couple.ofImmutable(new Attributes.Name("Can-Redefine-Classes"), "true");
  private static Instrumentation instrumentation;

  /**
   * 代理入口
   *
   * @param agentArgs
   * @param inst
   */
  @Deprecated
  public static void agentmain(String agentArgs, Instrumentation inst) {
    if (!inst.isRetransformClassesSupported()) {
      throw new UncatchableException(
          System.getProperty("java.version") + ", retransform classes not supported");
    }
    instrumentation = inst;
    LOGGER.info(
        "load agent complete , args: {} , loaded size: {}",
        agentArgs,
        inst.getAllLoadedClasses().length);
  }

  @CallerSensitive
  public static void startRedefine(ClassDefinition... definitions)
      throws UnmodifiableClassException, ClassNotFoundException, IOException,
          CannotCompileException, AttachNotSupportedException, NotFoundException,
          AgentLoadException, AgentInitializationException {
    checkJVMAttach();
    instrumentation.redefineClasses(definitions);
  }

  /**
   * 检查是否与当前进程的虚拟机建立了链接
   *
   * @throws IOException
   * @throws NotFoundException
   * @throws CannotCompileException
   * @throws AttachNotSupportedException
   * @throws AgentLoadException
   * @throws AgentInitializationException
   */
  private static void checkJVMAttach()
      throws IOException, NotFoundException, CannotCompileException, AttachNotSupportedException,
          AgentLoadException, AgentInitializationException {
    if (instrumentation != null) {
      return;
    }
    File agentJarFile =
        File.createTempFile(RedefinableClassJVMAgent.class.getSimpleName().toLowerCase(), ".jar");
    agentJarFile.deleteOnExit();
    LOGGER.info("create tmp agent jar file {}", agentJarFile.getPath());
    Manifest manifest = new Manifest();
    Attributes mainAttributes = manifest.getMainAttributes();
    mainAttributes.put(MANIFEST_VERSION.getLeft(), MANIFEST_VERSION.getRight());
    mainAttributes.put(AGENT_CLASS.getLeft(), AGENT_CLASS.getRight());
    mainAttributes.put(CAN_RETRANSFORM_CLASSES.getLeft(), CAN_RETRANSFORM_CLASSES.getRight());
    mainAttributes.put(CAN_REDEFINE_CLASSES.getLeft(), CAN_REDEFINE_CLASSES.getRight());
    try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(agentJarFile), manifest)) {
      JarEntry agent =
          new JarEntry(RedefinableClassJVMAgent.class.getName().replace('.', '/') + ".class");
      jos.putNextEntry(agent);
      ClassPool pool = ClassPool.getDefault();
      CtClass ctClass = pool.get(RedefinableClassJVMAgent.class.getName());
      jos.write(ctClass.toBytecode());
      jos.closeEntry();
    }
    LOGGER.info("prepare a jar entity for agent jar file");
    String nameOfRunningVM = ManagementFactory.getRuntimeMXBean().getName();
    String pid = nameOfRunningVM.substring(0, nameOfRunningVM.indexOf('@'));
    VirtualMachine vm = VirtualMachine.attach(pid);
    LOGGER.info("local jar agent attached jvm , pid {}", pid);
    vm.loadAgent(agentJarFile.getAbsolutePath(), "");
    LOGGER.info("remote jvm load agent {}", agentJarFile.getPath());
    vm.detach();
    LOGGER.info("detached remote jvm {}", pid);
  }
}
