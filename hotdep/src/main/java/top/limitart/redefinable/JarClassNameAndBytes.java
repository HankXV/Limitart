/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.redefinable;

import top.limitart.base.Conditions;
import top.limitart.base.URIScheme;

import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * 解析jar为bytes
 *
 * @author hank
 * @version 2019/8/2 0002 14:22
 */
public class JarClassNameAndBytes implements Iterable<Map.Entry<String, byte[]>> {

  private Map<String, byte[]> classes = new HashMap<>();
  private Map<String, byte[]> others = new HashMap<>();

  public byte[] getClass(String className) {
    return classes.get(className);
  }

  public byte[] getOther(String fileName) {
    return others.get(fileName);
  }

  public void forEachClass(BiConsumer<String, byte[]> action) {
    classes.forEach(action);
  }

  public void forEachFile(BiConsumer<String, byte[]> action) {
    others.forEach(action);
  }

  /**
   * 清除所有二进制缓存
   */
  public synchronized void clear() {
    classes.clear();
    others.clear();
  }

  public synchronized void addJars(String... localFiles) throws IOException {
    Map<String, byte[]> classContent = new HashMap<>();
    Map<String, byte[]> fileContent = new HashMap<>();
    for (String localFile : localFiles) {
      addJar0(localFile, classContent, fileContent);
    }
    classes = classContent;
    others = fileContent;
  }

  public synchronized void addJars(URI... uris) throws IOException {
    Map<String, byte[]> classContent = new HashMap<>();
    Map<String, byte[]> fileContent = new HashMap<>();
    for (URI uri : uris) {
      addJar0(uri, classContent, fileContent);
    }
    classes = classContent;
    others = fileContent;
  }

  public synchronized void addJars(File... files) throws IOException {
    Map<String, byte[]> classContent = new HashMap<>();
    Map<String, byte[]> fileContent = new HashMap<>();
    for (File file : files) {
      addJar0(file, classContent, fileContent);
    }
    classes = classContent;
    others = fileContent;
  }

  public synchronized void addJars(InputStream... inputStreams) throws IOException {
    Map<String, byte[]> classContent = new HashMap<>();
    Map<String, byte[]> fileContent = new HashMap<>();
    for (InputStream inputStream : inputStreams) {
      addJar0(inputStream, classContent, fileContent);
    }
    classes = classContent;
    others = fileContent;
  }

  private void addJar0(URI uri, Map<String, byte[]> classContent, Map<String, byte[]> fileContent)
      throws IOException {
    URIScheme uriScheme = URIScheme.find(uri);
    if (uriScheme == null) {
      throw new IOException("unsupported scheme " + uri.getScheme());
    }
    addJar0(new ByteArrayInputStream(uriScheme.fetchRes(uri)), classContent, fileContent);
  }

  private void addJar0(String localFile, Map<String, byte[]> classContent,
      Map<String, byte[]> fileContent) throws IOException {
    addJar0(new FileInputStream(new File(localFile)), classContent, fileContent);
  }

  private void addJar0(File file, Map<String, byte[]> classContent, Map<String, byte[]> fileContent)
      throws IOException {
    addJar0(new FileInputStream(file), classContent, fileContent);
  }

  private void addJar0(InputStream inputStream, Map<String, byte[]> classContent,
      Map<String, byte[]> fileContent) throws IOException {
    try (JarInputStream jis = new JarInputStream(inputStream)) {
      JarEntry jarEntry;
      while ((jarEntry = jis.getNextJarEntry()) != null) {
        if (jarEntry.isDirectory()) {
          continue;
        }
        byte[] b = new byte[2048];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int len;
        while ((len = jis.read(b)) > 0) {
          out.write(b, 0, len);
        }
        String entryName = jarEntry.getName();
        if (!entryName.endsWith(".class")) {
          Conditions.args(
              !fileContent.containsKey(entryName),
              "already have one file named %s in this area",
              entryName);

          fileContent.put(entryName, out.toByteArray());
        } else {
          String className = entryName.replace("/", ".").substring(0, entryName.indexOf(".class"));
          Conditions.args(
              !classContent.containsKey(className),
              "already have one class named %s in this area",
              className);
          classContent.put(className, out.toByteArray());
        }
        out.close();
      }
    }
  }

  @Override
  public java.util.Iterator<Map.Entry<String, byte[]>> iterator() {
    return new Iterator(this.classes.entrySet().iterator());
  }

  public class Iterator implements java.util.Iterator<Map.Entry<String, byte[]>> {

    private java.util.Iterator<Map.Entry<String, byte[]>> iterator;

    public Iterator(java.util.Iterator<Map.Entry<String, byte[]>> iterator) {
      this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
      return this.iterator.hasNext();
    }

    @Override
    public Map.Entry<String, byte[]> next() {
      return this.iterator.next();
    }

  }
}
