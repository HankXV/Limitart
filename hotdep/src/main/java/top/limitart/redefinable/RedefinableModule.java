/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.redefinable;

/**
 * 可被重加载模块入口<br>
 * 该类作为主程序{@code RedefinableApplition}即将加载jar包的入口
 *
 * @author hank
 * @code
 * @version 2019/8/2 0002 15:24
 */
public abstract class RedefinableModule {
  /**
   * 当模块被加载时
   *
   * @param context
   */
  public abstract void onStart(RedefinableApplicationContext context);

  /**
   * 当模块被重定义时
   *
   * @param context
   */
  public abstract void onRedefined(RedefinableApplicationContext context);

  /**
   * 当程序退出时
   *
   * @param context
   */
  public abstract void onDestroy(RedefinableApplicationContext context);
}
