/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.redefinable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.ApplicationBootstrap;
import top.limitart.base.label.CallerSensitive;
import top.limitart.concurrent.SingletonThreadFactory;
import top.limitart.concurrent.TaskQueue;

import java.io.IOException;
import java.lang.instrument.ClassDefinition;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

/**
 * 可重加载程序的入口
 *
 * @author hank
 * @version 2019/8/2 0002 13:52
 */
public class RedefinableApplication extends ApplicationBootstrap {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedefinableApplication.class);
    private RedefinableApplicationContext context;
    private RedefinableModule module;
    private Executor executor;
    private JarClassNameAndBytes jarContent = new JarClassNameAndBytes();
    private URI jarUri;
    private String moduleMainClass;

    public RedefinableApplication(URI jarUri, String entranceClassName) {
        this.jarUri = jarUri;
        this.moduleMainClass = entranceClassName;
    }

    protected RedefinableApplicationContext createContext() {
        return new RedefinableApplicationContext(
                new JarClassLoader(RedefinableApplication.class.getClassLoader(), jarContent));
    }

    private synchronized void refreshJarContent() throws IOException {
        if(this.jarUri == null){
            LOGGER.warn("no jar find,ignore this action!");
           return;
        }
        this.jarContent.clear();
        this.jarContent.addJars(this.jarUri);
    }

    @Override
    protected void onStart(String[] args) throws Exception {
        refreshJarContent();
        context = createContext();
        executor =
                TaskQueue.create(
                        new SingletonThreadFactory() {

                            @Override
                            public void onThreadCreated(Thread thread) {
                                thread.setName("Redefinable-Module-Boot");
                                thread.setContextClassLoader(context.getClassLoader());
                            }
                        });
        executor.execute(
                () -> loadModuleEntrance());
    }

    protected void loadModuleEntrance() {
        Class<?> moduleClass = null;
        try {
            moduleClass = context.getClassLoader().loadClass(this.moduleMainClass);
        } catch (ClassNotFoundException e) {
            LOGGER.error("can not find module main class {}", this.moduleMainClass);
            LOGGER.error("exception", e);
            System.exit(1);
        }
        if (moduleClass == null) {
            LOGGER.error("can not find module main class {}", this.moduleMainClass);
            System.exit(1);
        }
        Object instance = null;
        try {
            instance = moduleClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("can not create module instance of main class", e);
            System.exit(1);
        }
        if (instance == null) {
            LOGGER.error("can not create module instance of main class");
            System.exit(1);
        }
        if (!(instance instanceof RedefinableModule)) {
            LOGGER.error("module entrance must instance of " + RedefinableModule.class.getName());
            System.exit(1);
        }
        this.module = (RedefinableModule) instance;
        try {
            this.module.onStart(this.context);
        } catch (Exception e) {
            LOGGER.error("initialize module error", e);
            System.exit(1);
        }
    }

    @Override
    protected void onDestroy(String[] args) {
        executor.execute(
                () -> {
                    try {
                        this.module.onDestroy(context);
                    } catch (Exception e) {
                        LOGGER.error("module destroy error", e);
                    }
                });
    }

    @CallerSensitive
    public void reload() {
        executor.execute(
                () -> {
                    try {
                        refreshJarContent();
                    } catch (IOException e) {
                        LOGGER.error("refresh jar content error", e);
                        return;
                    }
                    List<ClassDefinition> list = new LinkedList<>();
                    try {
                        for (Map.Entry<String, byte[]> entry : this.jarContent) {
                            Class<?> origin = this.context.getClassLoader().loadClass(entry.getKey());
                            list.add(new ClassDefinition(origin, entry.getValue()));
                        }
                    } catch (ClassNotFoundException e) {
                        LOGGER.error("load class error", e);
                    }
                    try {
                        RedefinableClassJVMAgent.startRedefine(list.toArray(new ClassDefinition[0]));
                    } catch (Exception e) {
                        LOGGER.error("redefine error", e);
                    }
                    try {
                        this.module.onRedefined(this.context);
                    } catch (Exception e) {
                        LOGGER.error("on redefined error", e);
                    }
                });
    }
}
