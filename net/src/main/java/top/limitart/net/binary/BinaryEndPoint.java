/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.net.binary;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.function.Process2;
import top.limitart.base.function.Process3;
import top.limitart.base.function.Processes;
import top.limitart.base.label.Necessary;
import top.limitart.base.label.Optional;
import top.limitart.mapping.Router;
import top.limitart.net.NettyEndPoint;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.NettySession;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hank
 * @version 2018/10/9 0009 21:11
 */
public class BinaryEndPoint extends NettyEndPoint<ByteBuf, BinaryMessage> {
  private static Logger LOGGER = LoggerFactory.getLogger(BinaryEndPoint.class);
  private final BinaryDecoder decoder;
  private final BinaryEncoder encoder;
  private final Map<Short, Class<BinaryMessage>> id2Msg = new ConcurrentHashMap<>();

  private final Router<BinaryMessage, BinaryRequestParam> router;
  private final Process3<BinaryMessage, NettySession<BinaryMessage>, Router<BinaryMessage, BinaryRequestParam>> onMessageIn;
  private final Process2<NettySession<BinaryMessage>, Boolean> onConnected;
  private final Process2<NettySession<BinaryMessage>, Throwable> onExceptionThrown;

  public static Builder client() {
    return builder(false);
  }

  public static Builder server() {
    return builder(true);
  }

  public static Builder builder(boolean server) {
    return new Builder(server);
  }

  public static Builder builder(NettyEndPointType type) {
    return new Builder(type);
  }

  public BinaryEndPoint(Builder builder) {
    super(builder.name, builder.type, builder.autoReconnect, builder.timeoutSeconds);
    this.decoder = Conditions.notNull(builder.decoder, "decoder");
    this.encoder = Conditions.notNull(builder.encoder, "encoder");
    this.router = Conditions.notNull(builder.router, "router");
    this.onMessageIn = builder.onMessageIn;
    this.onConnected = builder.onConnected;
    this.onExceptionThrown = builder.onExceptionThrown;
    // 初始化消息
    router.foreachRequestClass(
        c -> {
          BinaryMessage binaryMessage = null;
          try {
            binaryMessage = router.requestInstance(c);
          } catch (Exception e) {
            LOGGER.error("mapping message id error", e);
          }
          id2Msg.put(binaryMessage.id(), c);
        });
  }

  @Override
  protected void beforeTranslatorPipeline(ChannelPipeline pipeline) {
    pipeline.addLast(
        new LengthFieldBasedFrameDecoder(
            decoder.getMaxFrameLength(),
            decoder.getLengthFieldOffset(),
            decoder.getLengthFieldLength(),
            decoder.getLengthAdjustment(),
            decoder.getInitialBytesToStrip()));
  }

  @Override
  protected void afterTranslatorPipeline(ChannelPipeline pipeline) {
    // DO NOTHING
  }

  @Override
  protected void exceptionThrown(NettySession<BinaryMessage> session, Throwable cause) {
    Processes.invoke(onExceptionThrown, session, cause);
  }

  @Override
  protected void sessionActive(NettySession<BinaryMessage> session, boolean activeOrNot) {
    Processes.invoke(onConnected, session, activeOrNot);
  }

  @Override
  protected void messageReceived(NettySession<BinaryMessage> session, Object arg) throws Exception {
    BinaryMessage msg = (BinaryMessage) arg;
    if (onMessageIn != null) {
      try {
        onMessageIn.run(msg, session, router);
      } catch (Exception e) {
        LOGGER.error(session.remoteAddress() + " cause:", e);
        Processes.invoke(onExceptionThrown, session, e);
      }
    } else {
      BinaryRequestParam binaryRequestParam = router.contextInstance();
      binaryRequestParam.session(session);
      router.request(msg, binaryRequestParam, Router.MethodInvoker::invoke);
      LOGGER.warn("can not find callback onMessageIn to deal msg,it will invoke by current THREAD!!!");
      //            throw new IllegalArgumentException("can not find handler to deal msg");
    }
  }

  @Override
  protected NettySession<BinaryMessage> createSession(Channel channel) {
    return new NettySession<>(channel);
  }

  @Override
  public BinaryMessage toOutputFinal(ByteBuf byteBuf) throws Exception {
    // 消息id
    short messageId = decoder.readMessageId(byteBuf);
    Class<BinaryMessage> aClass = id2Msg.get(messageId);
    if (aClass == null) {
      throw new BinaryMessageCodecException(
          name() + " message empty,id:" + BinaryMessages.ID2String(messageId));
    }
    BinaryMessage msg = router.requestInstance(aClass);
    if (msg == null) {
      throw new BinaryMessageCodecException(
          name() + " message empty,id:" + BinaryMessages.ID2String(messageId));
    }
    msg.replaceBuffer(byteBuf);
    try {
      msg.deserialize();
    } catch (Exception e) {
      LOGGER.error("message id:" + BinaryMessages.ID2String(messageId) + " decode error!");
      throw new BinaryMessageCodecException(e);
    }
    msg.softClose();
    return msg;
  }

  @Override
  public ByteBuf toInputFinal(BinaryMessage msg) throws Exception {
    ByteBuf buffer = Unpooled.buffer();
    encoder.beforeWriteBody(buffer, msg.id());
    msg.replaceBuffer(buffer);
    try {
      msg.serialize();
    } catch (IllegalAccessException e) {
      LOGGER.error("message encode error!", e);
    }
    encoder.afterWriteBody(buffer);
    msg.softClose();
    return buffer;
  }

  public static class Builder {
    private String name;
    private NettyEndPointType type;
    private int autoReconnect;
    private int timeoutSeconds;
    private BinaryDecoder decoder;
    private BinaryEncoder encoder;
    private Router<BinaryMessage, BinaryRequestParam> router;
    private Process3<BinaryMessage, NettySession<BinaryMessage>, Router<BinaryMessage, BinaryRequestParam>> onMessageIn;
    private Process2<NettySession<BinaryMessage>, Boolean> onConnected;
    private Process2<NettySession<BinaryMessage>, Throwable> onExceptionThrown;

    public Builder(boolean server) {
      this(server ? NettyEndPointType.defaultServer() : NettyEndPointType.defaultClient());
    }

    public Builder(NettyEndPointType type) {
      this.type = type;
      this.name = "Limitart-Binary";
      this.timeoutSeconds = 60;
      this.decoder = BinaryDecoder.BinaryDefaultDecoder.ME;
      this.encoder = BinaryEncoder.BinaryDefaultEncoder.ME;
    }

    /**
     * 构建服务器
     *
     * @return
     * @throws Exception
     */
    public BinaryEndPoint build() {
      return new BinaryEndPoint(this);
    }

    @Optional
    public Builder timeoutSeconds(int timeoutSeconds) {
      this.timeoutSeconds = timeoutSeconds;
      return this;
    }

    /**
     * 自定义解码器
     *
     * @param decoder
     * @return
     */
    @Optional
    public Builder decoder(BinaryDecoder decoder) {
      this.decoder = decoder;
      return this;
    }

    /**
     * 自定义编码器
     *
     * @param encoder
     * @return
     */
    @Optional
    public Builder encoder(BinaryEncoder encoder) {
      this.encoder = encoder;
      return this;
    }

    /**
     * 名称
     *
     * @param name
     * @return
     */
    @Optional
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    /**
     * 消息工厂
     *
     * @param router
     * @return
     */
    @Necessary
    public Builder router(Router<BinaryMessage, BinaryRequestParam> router) {
      this.router = router;
      return this;
    }

    /**
     * 消息接收处理
     *
     * @param onMessageIn
     * @return
     */
    @Optional
    public Builder onMessageIn(
            Process3<BinaryMessage, NettySession<BinaryMessage>, Router<BinaryMessage, BinaryRequestParam>>
            onMessageIn) {
      this.onMessageIn = onMessageIn;
      return this;
    }

    /**
     * 链接创建处理
     *
     * @param onConnected
     * @return
     */
    @Optional
    public Builder onConnected(Process2<NettySession<BinaryMessage>, Boolean> onConnected) {
      this.onConnected = onConnected;
      return this;
    }

    /**
     * 自动重连尝试间隔(秒)
     *
     * @param autoReconnect
     * @return
     */
    @Optional
    public Builder autoReconnect(int autoReconnect) {
      this.autoReconnect = autoReconnect;
      return this;
    }

    /**
     * 服务器抛异常处理
     *
     * @param onExceptionThrown
     * @return
     */
    @Optional
    public Builder onExceptionThrown(
        Process2<NettySession<BinaryMessage>, Throwable> onExceptionThrown) {
      this.onExceptionThrown = onExceptionThrown;
      return this;
    }
  }
}
