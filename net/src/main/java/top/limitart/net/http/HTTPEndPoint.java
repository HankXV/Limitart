/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.net.http;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.FileUpload;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.ssl.SslHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.function.Function2;
import top.limitart.base.function.Process2;
import top.limitart.base.function.Processes;
import top.limitart.base.label.Optional;
import top.limitart.net.NettyEndPoint;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.NettySession;
import top.limitart.net.Session;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpMethod.POST;

/** HTTP端点实现 Created by Hank on 2018/10/13 */
public class HTTPEndPoint extends NettyEndPoint<HttpMessage, HttpMessage> {
  private static Logger LOGGER = LoggerFactory.getLogger(HTTPEndPoint.class);
  public static int MESSAGE_MAX_SIZE = 1024 * 1024;
  private SSLContext sslContext;
  private Process2<NettySession<HttpMessage>, HttpMessage> onMessageOverSize;
  private Function2<NettySession<HttpMessage>, HTTPRequest, byte[]> onMessageIn;
  private Supplier<Executor> whoDeal;
  private Process2<NettySession<HttpMessage>, Boolean> onConnected;
  private Process2<NettySession<HttpMessage>, Throwable> onExceptionThrown;

  public static Builder builder() {
    return new Builder();
  }

  public HTTPEndPoint(Builder builder) {
    super(builder.name, NettyEndPointType.SERVER_REMOTE, 0, 60);
    this.sslContext = builder.sslContext;
    this.onMessageOverSize = builder.onMessageOverSize;
    this.onMessageIn = builder.onMessageIn;
    this.onConnected = builder.onConnected;
    this.onExceptionThrown = builder.onExceptionThrown;
    this.whoDeal = builder.whoDeal;
  }

  @Override
  protected NettySession<HttpMessage> createSession(Channel channel) {
    return new HTTPSession(channel);
  }

  @Override
  protected void beforeTranslatorPipeline(ChannelPipeline pipeline) {
    if (this.sslContext != null) {
      SSLEngine sslEngine = sslContext.createSSLEngine();
      sslEngine.setUseClientMode(false);
      sslEngine.setNeedClientAuth(false);
      sslEngine.setWantClientAuth(false);
      pipeline.addLast(new SslHandler(sslEngine));
    }
    pipeline
        .addLast(new HttpServerCodec())
        .addLast(
            new HttpObjectAggregator(MESSAGE_MAX_SIZE) {
              @Override
              protected void handleOversizedMessage(
                  ChannelHandlerContext ctx, HttpMessage oversized) throws Exception {
                LOGGER.error(
                    "{} message oversize :{},max :{}", ctx.channel(), oversized, MESSAGE_MAX_SIZE);
                Processes.invoke(onMessageOverSize, getSession(ctx.channel()), oversized);
              }
            });
  }

  @Override
  protected void afterTranslatorPipeline(ChannelPipeline pipeline) {}

  @Override
  protected void exceptionThrown(NettySession<HttpMessage> session, Throwable cause)
      throws Exception {
    Processes.invoke(onExceptionThrown, session, cause);
  }

  @Override
  protected void sessionActive(NettySession<HttpMessage> session, boolean activeOrNot)
      throws Exception {
    Processes.invoke(onConnected, session, activeOrNot);
  }

  public static HTTPRequest toHTTPRequest(HTTPSession session, FullHttpRequest msg) {
    QueryStringDecoder queryStringDecoder = new QueryStringDecoder(msg.uri());
    String url = queryStringDecoder.path();
    if (!msg.decoderResult().isSuccess()) {
      session.response(HttpResponseStatus.BAD_REQUEST, "bad request");
      return null;
    }
    LOGGER.info("host:{},uri:{},method:{}", session.getChannel(), url, msg.method());
    if ("/2016info".equals(url)) {
      session.response(HttpResponseStatus.OK, "hello~stupid!");
      return null;
    }
    Map<String, String> params = new HashMap<>();
    params.putAll(
        queryStringDecoder
            .parameters()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get(0))));
    if (msg.method() == GET) {
      // DO NOTHING
    } else if (msg.method() == POST) {
      try {
        HttpPostRequestDecoder postDecoder = new HttpPostRequestDecoder(msg);
        List<InterfaceHttpData> postData = postDecoder.getBodyHttpDatas();
        for (InterfaceHttpData data : postData) {
          if (data instanceof Attribute) {
            Attribute at = (Attribute) data;
            String name = at.getName();
            String value = at.getValue();
            if (params.containsKey(name)) {
              LOGGER.error("param duplicated %s", name);
              session.response(HttpResponseStatus.OK, "param duplicated " + name);
              return null;
            }
            params.put(name, value);
          } else if (data instanceof FileUpload) {
            //                        FileUpload fileUpload = (FileUpload) data;
            //                        int readableBytes = fileUpload.content().readableBytes();
            //                        // 没内容的文件GG掉
            //                        if (readableBytes > 0) {
            //                            String name = fileUpload.getFilename();
            //                            byte[] file = new byte[readableBytes];
            //                            fileUpload.content().readBytes(file);
            //                            message.getFiles().put(name, file);
            //                        }
            // 不支持上传文件
            session.response(HttpResponseStatus.FORBIDDEN, "file upload not allowed");
            return null;
          }
        }
      } catch (Exception e) {
        LOGGER.error("decode http request error", e);
        session.response(HttpResponseStatus.INTERNAL_SERVER_ERROR, "decode error", true);
        return null;
      }
    } else {
      session.response(HttpResponseStatus.METHOD_NOT_ALLOWED, "method not allowed");
      return null;
    }
    return new HTTPRequest(msg.method(), url, params);
  }

  @Override
  protected void messageReceived(NettySession<HttpMessage> s, Object arg) throws Exception {
    if (this.whoDeal == null) {
      messageReceived0(s, arg);
    } else {
      this.whoDeal.get().execute(() -> messageReceived0(s, arg));
    }
  }

  private void messageReceived0(Session<HttpMessage, EventLoop> s, Object arg) {
    HTTPSession session = (HTTPSession) s;
    FullHttpRequest msg = (FullHttpRequest) arg;
    HTTPRequest request = toHTTPRequest(session, msg);
    if (onMessageIn != null) {
      byte[] run = onMessageIn.apply(session, request);
      if (run == null) {
        session.response(HttpResponseStatus.NOT_FOUND, "no handler", true);
      } else {
        session.response(HttpResponseStatus.OK, ContentTypes.application_json, run, true);
      }
    } else {
      session.response(HttpResponseStatus.NOT_FOUND, "no handler", true);
    }
  }

  @Override
  public HttpMessage toOutputFinal(HttpMessage message) throws Exception {
    return message;
  }

  @Override
  public HttpMessage toInputFinal(HttpMessage message) throws Exception {
    return message;
  }

  public static class Builder {
    private String name;
    private SSLContext sslContext;
    private Process2<NettySession<HttpMessage>, HttpMessage> onMessageOverSize;
    private Function2<NettySession<HttpMessage>, HTTPRequest, byte[]> onMessageIn;
    private Supplier<Executor> whoDeal;
    private Process2<NettySession<HttpMessage>, Boolean> onConnected;
    private Process2<NettySession<HttpMessage>, Throwable> onExceptionThrown;

    public Builder() {
      this.name = "Limitart-HTTP";
    }

    /**
     * 构建服务器
     *
     * @return
     * @throws Exception
     */
    public HTTPEndPoint build() {
      return new HTTPEndPoint(this);
    }

    /**
     * 名称
     *
     * @param name
     * @return
     */
    @Optional
    public HTTPEndPoint.Builder name(String name) {
      this.name = name;
      return this;
    }

    /**
     * 消息接收处理
     *
     * @param onMessageIn
     * @return
     */
    @Optional
    public HTTPEndPoint.Builder onMessageIn(
        Function2<NettySession<HttpMessage>, HTTPRequest, byte[]> onMessageIn,
        Supplier<Executor> whoDeal) {
      this.onMessageIn = onMessageIn;
      this.whoDeal = whoDeal;
      return this;
    }

    /**
     * 链接创建处理
     *
     * @param onConnected
     * @return
     */
    @Optional
    public HTTPEndPoint.Builder onConnected(
        Process2<NettySession<HttpMessage>, Boolean> onConnected) {
      this.onConnected = onConnected;
      return this;
    }

    /**
     * 服务器抛异常处理
     *
     * @param onExceptionThrown
     * @return
     */
    @Optional
    public HTTPEndPoint.Builder onExceptionThrown(
        Process2<NettySession<HttpMessage>, Throwable> onExceptionThrown) {
      this.onExceptionThrown = onExceptionThrown;
      return this;
    }

    @Optional
    public HTTPEndPoint.Builder onMessageOverSize(
        Process2<NettySession<HttpMessage>, HttpMessage> onMessageOverSize) {
      this.onMessageOverSize = onMessageOverSize;
      return this;
    }

    @Optional
    public HTTPEndPoint.Builder sslContext(SSLContext sslContext) {
      this.sslContext = sslContext;
      return this;
    }
  }
}
