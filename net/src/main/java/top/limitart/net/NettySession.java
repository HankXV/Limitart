/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoop;
import top.limitart.base.Conditions;
import top.limitart.base.function.Process2;
import top.limitart.base.function.Processes;
import top.limitart.net.binary.BinaryMessageIOException;

import java.net.SocketAddress;

/**
 * @author hank
 * @version 2018/10/8 0008 17:06
 */
public class NettySession<M> extends AbstractSession<M, EventLoop> {
  private final Channel channel;

  public NettySession(Channel channel) {
    Conditions.notNull(channel, "channel");
    this.channel = channel;
  }

  /**
   * 立即写出数据
   *
   * @param buf
   * @param resultCallback
   */
  @Override
  public void writeNow(M buf, Process2<Boolean, Throwable> resultCallback) {
    Conditions.notNull(buf, "buf");
    if (!writable()) {
      Processes.invoke(resultCallback, false, new BinaryMessageIOException("unwritable"));
      return;
    }
    this.channel
        .writeAndFlush(buf)
        .addListener(
            (ChannelFutureListener)
                arg0 -> Processes.invoke(resultCallback, arg0.isSuccess(), arg0.cause()));
  }

  @Override
  public void writerCache(M buf, Process2<Boolean, Throwable> resultCallback) {
    Conditions.notNull(buf, "buf");
    if (!writable()) {
      Processes.invoke(resultCallback, false, new BinaryMessageIOException("unwritable"));
      return;
    }
    this.channel
            .write(buf)
            .addListener(
                    (ChannelFutureListener)
                            arg0 -> Processes.invoke(resultCallback, arg0.isSuccess(), arg0.cause()));
  }

  @Override
  public void flushCache() {
    this.channel.flush();
  }

  /**
   * 是否可写
   *
   * @return
   */
  @Override
  public boolean writable() {
    return this.channel.isWritable();
  }

  /**
   * 关闭会话
   *
   * @param resultCallback
   */
  @Override
  public void close(Process2<Boolean, Throwable> resultCallback) {
    this.channel
        .close()
        .addListener(
            (ChannelFutureListener)
                arg0 -> Processes.invoke(resultCallback, arg0.isSuccess(), arg0.cause()));
  }

  /**
   * 远程地址
   *
   * @return
   */
  @Override
  public SocketAddress remoteAddress() {
    return this.channel.remoteAddress();
  }

  /**
   * 本地地址
   *
   * @return
   */
  @Override
  public SocketAddress localAddress() {
    return this.channel.localAddress();
  }

  @Override
  public EventLoop thread() {
    return channel.eventLoop();
  }

  @Override
  public String toString() {
    return channel.toString();
  }

  public Channel getChannel() {
    return channel;
  }
}
