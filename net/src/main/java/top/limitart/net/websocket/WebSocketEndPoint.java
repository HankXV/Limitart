/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.net.websocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.util.ReferenceCountUtil;
import top.limitart.base.Conditions;
import top.limitart.net.NettyEndPoint;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.NettySession;
import top.limitart.net.Session;
import top.limitart.net.http.HTTPEndPoint;
import top.limitart.net.http.HTTPRequest;
import top.limitart.net.http.HTTPSession;

/** WebSocket协议端点 Created by Hank on 2019/2/24 */
public abstract class WebSocketEndPoint extends NettyEndPoint<Object, Object> {
  private String webSocketServerUrl;

  public WebSocketEndPoint(
      String name,
      String webSocketServerUrl,
      NettyEndPointType type,
      int autoReconnect,
      int timeoutSeconds) {
    super(name, type, autoReconnect, timeoutSeconds);
    Conditions.args(type.server(), "client unsupported!");
    this.webSocketServerUrl = webSocketServerUrl;
  }

  @Override
  protected void beforeTranslatorPipeline(ChannelPipeline pipeline) {
    pipeline.addLast(new HttpServerCodec());
    pipeline.addLast(new HttpObjectAggregator(HTTPEndPoint.MESSAGE_MAX_SIZE));
    pipeline.addLast(new WebSocketServerProtocolHandler(this.webSocketServerUrl));
  }

  @Override
  protected void afterTranslatorPipeline(ChannelPipeline pipeline) {}

  @Override
  protected void messageReceived(NettySession<Object> session, Object msg) throws Exception {
    if (msg instanceof FullHttpRequest) {
      HTTPSession httpSession = new HTTPSession(session.getChannel());
      HTTPRequest request = HTTPEndPoint.toHTTPRequest(httpSession, (FullHttpRequest) msg);
      onHttpRequestReceived(httpSession, request);
    } else if (msg instanceof WebSocketFrame) {
      onWebSocketFrameReceived(session, (WebSocketFrame) msg);
    } else {
      throw new DecoderException("unsupported msg " + msg.getClass().getName());
    }
  }

  protected abstract void onWebSocketFrameReceived(
      Session<Object, EventLoop> session, WebSocketFrame msg);

  protected abstract void onHttpRequestReceived(HTTPSession session, HTTPRequest msg);

  @Override
  public Object toOutputFinal(Object msg) throws Exception {
    return msg;
  }

  @Override
  public Object toInputFinal(Object msg) throws Exception {
    ReferenceCountUtil.retain(msg);
    return msg;
  }

  @Override
  protected NettySession<Object> createSession(Channel channel) {
    return new NettySession<>(channel);
  }
}
