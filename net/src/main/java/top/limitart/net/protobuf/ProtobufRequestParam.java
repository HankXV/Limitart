package top.limitart.net.protobuf;

import com.google.protobuf.Message;
import top.limitart.mapping.NettyRequestContext;

/**
 * Protobuf端点路由参数
 *
 * @author hank
 * @version 2018/10/12 0012 21:05
 */
public abstract class ProtobufRequestParam extends NettyRequestContext<Message> {
}
