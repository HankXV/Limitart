/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.mapping;

import top.limitart.base.function.Function1;
import top.limitart.base.function.Process1;

import java.io.IOException;
import java.util.function.Consumer;

/**
 *  路由器
 *
 *  @author hank
 *  @version 2018/10/8 0008 20:08
 * @param <MESSAGE> 要路由的消息类型
 * @param <CONTEXT> 包装消息的上下文
 */
public interface Router<MESSAGE, CONTEXT extends RequestContext> {

  /**
   * 创造一个空的消息工厂
   *
   * @return
   */
  static <MESSAGE, CONTEXT extends RequestContext> Router empty(Class<MESSAGE> mClass, Class<CONTEXT> cClass) {
    return RouterImpl.empty(mClass, cClass);
  }

  /**
   * 通过包扫描创建消息工厂
   *
   * @param scanPackage 包名
   * @return
   * @throws ReflectiveOperationException
   * @throws IOException
   */
  static <M, C extends RequestContext> Router create(
          Class<M> mClass, Class<C> cClass, String scanPackage)
      throws RequestDuplicatedException, ReflectiveOperationException, IOException {
    return RouterImpl.create(mClass, cClass, scanPackage);
  }

  /**
   * 通过扫描包创建消息工厂
   *
   * @param scanPackage
   * @param confirmInstance 指定manager的 实例
   * @return
   * @throws IOException
   * @throws ReflectiveOperationException
   */
  static <MESSAGE, CONTEXT extends RequestContext> Router create(
          Class<MESSAGE> mClass,
          Class<CONTEXT> cClass,
          String scanPackage,
          Function1<Class<?>, Object> confirmInstance)
      throws Exception {
    return RouterImpl.create(mClass, cClass, scanPackage, confirmInstance);
  }

  /**
   * 注册一个manager
   *
   * @param mapperClass
   */
  Router<MESSAGE, CONTEXT> registerMapperClass(Class<?> mapperClass) throws Exception;

  /**
   * 注册一个manager
   *
   * @param mapperClass 类
   * @param confirmInstance 指定manager的实例
   */
  Router<MESSAGE, CONTEXT> registerMapperClass(
          Class<?> mapperClass, Function1<Class<?>, Object> confirmInstance) throws Exception;

  /**
   * 替换掉manager的实例
   *
   * @param request
   * @param newInstance
   */
  void replaceInstance(Class<?> mapperClass, MESSAGE request, Object newInstance) throws Exception;

  void foreachRequestClass(Consumer<Class<MESSAGE>> consumer);

  /**
   * 根据ID获取一个消息实例
   *
   * @param requestClass
   * @return
   * @throws ReflectiveOperationException
   */
  MESSAGE requestInstance(Class<MESSAGE> requestClass) throws Exception;

  CONTEXT contextInstance();

  void request(MESSAGE msg, CONTEXT context, Process1<MethodInvoker> proc);

  interface MethodInvoker {
    Object invoke();
  }
}
