/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.mapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.function.Function1;
import top.limitart.base.function.Process1;
import top.limitart.reflectasm.ConstructorAccess;
import top.limitart.reflectasm.MethodAccess;
import top.limitart.util.ReflectionUtil;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * 方法路由器实现
 *
 * @author hank
 * @version 2018/10/8 0008 20:48
 */
public class RouterImpl<MESSAGE, CONTEXT extends RequestContext>
    implements Router<MESSAGE, CONTEXT> {
  private static final Logger LOGGER = LoggerFactory.getLogger(RouterImpl.class);
  // !!这里的asm应用经测试在JAVA8下最优
  private final Map<Class<MESSAGE>, Context> msgs = new ConcurrentHashMap<>();
  private final Map<Class<?>, Object> managerInstances = new ConcurrentHashMap<>();
  private Class<MESSAGE> mClass;
  private Class<CONTEXT> cClass;
  private final ConstructorAccess contextMethod;

  /**
   * 创造一个空的消息工厂
   *
   * @return
   */
  public static <MESSAGE, CONTEXT extends RequestContext> Router empty(
      Class<MESSAGE> mClass, Class<CONTEXT> cClass) {
    return new RouterImpl(mClass, cClass);
  }

  /**
   * 通过扫描包创建消息工厂
   *
   * @param scanPackage
   * @param confirmInstance 指定manager的 实例
   * @return
   * @throws IOException
   * @throws ReflectiveOperationException
   */
  public static <MESSAGE, CONTEXT extends RequestContext> Router create(
      Class<MESSAGE> mClass,
      Class<CONTEXT> cClass,
      String scanPackage,
      Function1<Class<?>, Object> confirmInstance)
      throws Exception {
    Conditions.notNull(scanPackage, "scanPackage");
    RouterImpl factory = new RouterImpl(mClass, cClass);
    List<Class<?>> classesByPackage =
        ReflectionUtil.getClassesBySuperClass(scanPackage, Object.class);
    for (Class<?> clazz : classesByPackage) {
      factory.registerMapperClass(clazz, confirmInstance);
    }
    return factory;
  }

  /**
   * 通过包扫描创建消息工厂
   *
   * @param scanPackage 包名
   * @return
   * @throws ReflectiveOperationException
   * @throws IOException
   */
  public static <MESSAGE, CONTEXT extends RequestContext> Router create(
      Class<MESSAGE> mClass, Class<CONTEXT> cClass, String scanPackage)
      throws IOException, ReflectiveOperationException, RequestDuplicatedException {
    Conditions.notNull(scanPackage, "scanPackage");
    RouterImpl factory = new RouterImpl(mClass, cClass);
    List<Class<?>> classesByPackage =
        ReflectionUtil.getClassesBySuperClass(scanPackage, Object.class);
    for (Class<?> clazz : classesByPackage) {
      if (clazz.getAnnotation(Mapper.class) == null) {
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("{} has no @Mapper annotation", clazz.getName());
        }
        continue;
      }
      factory.registerMapperClass(clazz, null);
    }
    return factory;
  }

  public RouterImpl(Class<MESSAGE> mClass, Class<CONTEXT> cClass) {
    this.mClass = mClass;
    this.cClass = cClass;
    this.contextMethod = ConstructorAccess.get(this.cClass);
  }

  /**
   * 注册一个manager
   *
   * @param mapperClass
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  @Override
  public Router<MESSAGE, CONTEXT> registerMapperClass(Class<?> mapperClass)
      throws IllegalAccessException, InstantiationException, RequestDuplicatedException {
    return registerMapperClass(mapperClass, null);
  }

  /**
   * 注册一个manager
   *
   * @param mapperClass 类
   * @param confirmInstance 指定manager的 实例
   */
  public Router<MESSAGE, CONTEXT> registerMapperClass(
      Class<?> mapperClass, Function1<Class<?>, Object> confirmInstance)
      throws IllegalAccessException, InstantiationException, RequestDuplicatedException {
    Mapper manager = mapperClass.getAnnotation(Mapper.class);
    if (manager == null) {
      throw new InstantiationException(mapperClass.getName() + " has no @Mapper annotation");
    }
    // 扫描方法
    MethodAccess methodAccess = MethodAccess.get(mapperClass);
    for (int i = 0; i < methodAccess.getMethods().size(); ++i) {
      Method method = methodAccess.getMethods().get(i);
      // 扫描该方法的参数
      Class<?>[] parameterTypes = method.getParameterTypes();
      Annotation[][] parameterAnnotations = method.getParameterAnnotations();
      // 找到标记为@Request的参数和RequestContext的位置，Request必须，因为要判定是什么消息，Conext非必须
      List<Class<?>> paramList = new ArrayList<>(2);
      Class<?> msgType = null;
      Class<?> contextType = null;
      for (int j = 0; j < parameterTypes.length; j++) {
        Class<?> parameterType = parameterTypes[j];

        for (Annotation annotation : parameterAnnotations[j]) {
          if (annotation.annotationType() == Request.class) {
            if (msgType != null) {
              throw new RequestDuplicatedException("one method more than one Request(@Request)???");
            }
            msgType = parameterType;
            paramList.add(msgType);
            break;
          }
        }
        if (ReflectionUtil.isInheritFrom(parameterType, RequestContext.class)) {
          contextType = parameterType;
          paramList.add(contextType);
        }
      }
      if (msgType == null) {
        continue;
      }
      // 没有找到必要的消息类型就返回
      if (!Modifier.isPublic(method.getModifiers())) {
        throw new IllegalAccessError(
            "method must be PUBLIC:"
                + mapperClass.getName()
                + "."
                + ReflectionUtil.getMethodOverloadName(method));
      }
      if (!mClass.isAssignableFrom(msgType)) {
        LOGGER.info("{} IS NOT a {},IGNORE.", msgType.getName(), mClass.getName());
        continue;
      }
      if (contextType != null && !cClass.isAssignableFrom(contextType)) {
        throw new IllegalAccessException(
            mapperClass.getName()
                + "."
                + ReflectionUtil.getMethodOverloadName(method)
                + " param can only be ASSIGNABLE from "
                + cClass.getName());
      }
      ConstructorAccess<MESSAGE> constructorAccess =
          (ConstructorAccess<MESSAGE>) ConstructorAccess.get(msgType);
      if (msgs.containsKey(msgType)) {
        throw new RequestDuplicatedException(msgType.getName());
      }
      Context messageContext = new Context();
      messageContext.conAccess = constructorAccess;
      messageContext.managerClazz = mapperClass;
      messageContext.methodName = method.getName();
      messageContext.paramTypes = paramList.toArray(new Class<?>[0]);
      messageContext.methodAccess = methodAccess;
      msgs.put((Class<MESSAGE>) msgType, messageContext);
      if (!managerInstances.containsKey(mapperClass)) {
        managerInstances.put(
            mapperClass,
            confirmInstance != null
                ? confirmInstance.apply(mapperClass)
                : mapperClass.newInstance());
      }
      LOGGER.info("register request " + msgType.getName() + " at " + mapperClass.getName());
    }
    return this;
  }

  /**
   * 替换掉manager的实例
   *
   * @param request
   * @param newInstance
   */
  public void replaceInstance(Class<?> mapperClass, MESSAGE request, Object newInstance) {
    Conditions.notNull(mapperClass, "mapperClass");
    Conditions.notNull(request, "request");
    Conditions.notNull(newInstance, "newInstance");
    if (managerInstances.containsKey(mapperClass)) {
      managerInstances.put(mapperClass, newInstance);
    }
  }

  @Override
  public void foreachRequestClass(Consumer<Class<MESSAGE>> consumer) {
    msgs.keySet().forEach(consumer);
  }

  /**
   * 根据ID获取一个消息实例
   *
   * @param requestClass
   * @return
   */
  @Override
  public MESSAGE requestInstance(Class<MESSAGE> requestClass) {
    if (!msgs.containsKey(requestClass)) {
      return null;
    }
    Context messageContext = msgs.get(requestClass);
    return messageContext.conAccess.newInstance();
  }

  /**
   * 根据ID获取一个消息实例
   *
   * @return
   */
  @Override
  public CONTEXT contextInstance() {
    return (CONTEXT) contextMethod.newInstance();
  }

  @Override
  public void request(MESSAGE msg, CONTEXT context, Process1<MethodInvoker> proc) {
    Conditions.notNull(msg, "request");
    Context messageContext = msgs.get(msg.getClass());
    if (messageContext == null) {
      LOGGER.error("request empty,id:" + msg.getClass().getName());
      // 消息上下文不存在
      return;
    }
    Object object = managerInstances.get(messageContext.managerClazz);
    proc.accept(
        new Invoker(
            messageContext.methodAccess,
            object,
            messageContext.methodName,
            messageContext.paramTypes,
            msg,
            context));
  }

  private class Context {
    private ConstructorAccess<MESSAGE> conAccess;
    private MethodAccess methodAccess;
    private String methodName;
    private Class<?>[] paramTypes;
    private Class<?> managerClazz;
  }

  public class Invoker implements MethodInvoker {
    private MethodAccess methodAccess;
    private Object object;
    private Class<?>[] paramTypes;
    private MESSAGE msg;
    private String methodName;
    private CONTEXT param;

    public Invoker(
        MethodAccess methodAccess,
        Object object,
        String methodName,
        Class<?>[] paramTypes,
        MESSAGE msg) {
      this(methodAccess, object, methodName, paramTypes, msg, null);
    }

    public Invoker(
        MethodAccess methodAccess,
        Object object,
        String methodName,
        Class<?>[] paramTypes,
        MESSAGE msg,
        CONTEXT param) {
      this.methodAccess = methodAccess;
      this.object = object;
      this.methodName = methodName;
      this.msg = msg;
      this.param = param;
      this.paramTypes = paramTypes;
    }

    @Override
    public Object invoke() {
      // 这里就是通过paramtype的位置确定参数位置，后面如果参数继续灵活增多，这里方法需要修改
      Object[] paramInstance = new Object[paramTypes.length];
      for (int i = 0; i < paramTypes.length; i++) {
        if (paramTypes[i] != null) {
          if (paramTypes[i] == msg.getClass()) {
            paramInstance[i] = msg;
          } else if (param != null && paramTypes[i] == param.getClass()) {
            paramInstance[i] = param;
          }
        }
      }
      return methodAccess.invoke(object, methodName, paramTypes, paramInstance);
    }
  }
}
