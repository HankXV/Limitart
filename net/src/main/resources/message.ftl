/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ${_package};

import top.limitart.net.binary.*;
import top.limitart.base.BinaryMeta;

/**
 * ${_explain}
 * 
 * @author limitart
 *
 */
public class ${_name} {
	private static final byte PACKAGE_ID = ${_id};

<#list _metas as _meta>
   	/**
	 * ${_meta._explain}
	 *
	 * @author limitart
	 *
	 */
	public static class ${_meta._name} extends BinaryMeta {

	<#list _meta._fields as _field>
		<#if (_field._isList==1)>
		/**
		 * ${_field._explain}
		 */
		public java.util.List<${_field._type}> ${_field._name} = new java.util.ArrayList<>();
		<#else>
		/**
		 * ${_field._explain}
		 */
		public ${_field._type} ${_field._name};
		</#if>
	</#list>
	}
</#list>

<#list _enums as _enum>
	/**
	 * ${_enum._explain}
	 *
	 * @author limitart
	 *
	 */
	public static class ${_enum._name} extends BinaryByteMessage {

	<#list _enum._fields as _field>
		/**
		 * ${_field._explain}
		 */
		public static final ${_enum._name} ${_field._name?upper_case} = new ${_enum._name}((byte)${_field_index});
	</#list>

        public ${_enum._name}() {
        }

        public ${_enum._name}(byte code) {
            super(code);
        }

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, ${_enum._messageID});
		}

	}
</#list>

<#list _messages as _message>
	/**
	 * ${_message._explain}
	 *
	 * @author limitart
	 *
	 */
	public static class ${_message._name} extends BinaryMessage {

	<#list _message._fields as _field>
		<#if (_field._isList==1)>
		/**
		 * ${_field._explain}
		 */
		public java.util.List<${_field._type}> ${_field._name} = new java.util.ArrayList<>();
		<#else>
		/**
		 * ${_field._explain}
		 */
		public ${_field._type} ${_field._name};
		</#if>
	</#list>

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, ${_message._messageID});
		}

	}
</#list>
}
