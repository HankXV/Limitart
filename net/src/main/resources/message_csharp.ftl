/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using Cimex.Base;
using Cimex.Net.Binary;

/// <summary>
/// ${_explain}
/// </summary>
public class ${_name} {
	private const byte PACKAGE_ID = ${_id};

<#list _metas as _meta>
    /// <summary>
    /// ${_meta._explain}
    /// </summary>
	public class ${_meta._name} : BinaryMeta {

	<#list _meta._fields as _field>
		<#if (_field._isList==1)>
		/// <summary>
        /// ${_field._explain}
        /// </summary>
		public List<${_field._type}> ${_field._name?cap_first}
        {
            get { return ${_field._name?cap_first} ?? (${_field._name?cap_first} = new List<${_field._type}>()); }
            set
            {
			${_field._name?cap_first} = value;
            }
        }
		<#else>
	    /// <summary>
	    /// ${_field._explain}
        /// </summary>
		public ${_field._type} ${_field._name?cap_first}{ get; set; }
		</#if>
	</#list>
	}
</#list>

<#list _enums as _enum>

    /// <summary>
    /// ${_enum._explain}
    /// </summary>
	public class ${_enum._name} : BinaryEnumMessage {

	<#list _enum._fields as _field>
		/// <summary>
        /// ${_field._explain}
        /// </summary>
		public static ${_enum._name} ${_field._name?upper_case} = new ${_enum._name}((byte)${_field_index});
	</#list>

		public ${_enum._name}()
		{
		}

		public ${_enum._name}(byte code):base(code)
		{
		}

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, ${_enum._messageID});
		}

	}
</#list>

<#list _messages as _message>
    /// <summary>
    /// ${_message._explain}
    /// </summary>
	public class ${_message._name} : BinaryMessage {

	<#list _message._fields as _field>
		<#if (_field._isList==1)>
		/// <summary>
        /// ${_field._explain}
        /// </summary>
		public List<${_field._type}> ${_field._name?cap_first}
        {
            get { return ${_field._name?cap_first} ?? (${_field._name?cap_first} = new List<${_field._type}>()); }
            set
            {
			${_field._name?cap_first} = value;
            }
        }
		<#else>
		/// <summary>
        /// ${_field._explain}
        /// </summary>
		public ${_field._type} ${_field._name?cap_first}{ get; set; }
		</#if>
	</#list>

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, ${_message._messageID});
		}

	}
</#list>
}
