/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.function.Process2;
import top.limitart.base.label.ThreadSafe;
import top.limitart.util.TimeUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * LRU线程安全缓存
 *
 * @author hank
 * @version 2018/11/13 0013 20:48
 */
@ThreadSafe
public class ConcurrentLRUCache<K, V extends ConcurrentLRUCache.LRUCacheable>
    implements Cache<K, V> {
  private static Logger LOGGER = LoggerFactory.getLogger(ConcurrentLRUCache.class);
  private final int capacity;
  private final boolean forceCollect;
  private ReadWriteLock lock = new ReentrantReadWriteLock();
  private Lock readLock = lock.readLock();
  private Lock writeLock = lock.writeLock();
  private Map<K, V> map;
  private long lastCollectTriggered;

  /**
   * @param capacity
   * @param forceCollect 当size>capacity的时候是否忽略过期时间(但是不忽略用户自定义条件)，强制回收
   */
  public ConcurrentLRUCache(int capacity, boolean forceCollect) {
    Conditions.positive(capacity);
    map = new HashMap<>(capacity + 1, 0.75f);
    this.capacity = capacity;
    this.forceCollect = forceCollect;
  }

  public ConcurrentLRUCache(int capacity) {
    this(capacity, false);
  }

  @Override
  public V get(K key) {
    Conditions.notNull(key);
    try {
      readLock.lock();
      V v = map.get(key);
      if (v != null) {
        v.onUsed();
      }
      return v;
    } finally {
      readLock.unlock();
    }
  }

  @Override
  public V put(K key, V value) {
    Conditions.notNull(key);
    Conditions.notNull(value);
    try {
      writeLock.lock();
      checkDead();
      V old = map.put(key, value);
      value.onUsed();
      return old;
    } finally {
      writeLock.unlock();
    }
  }

  @Override
  public V remove(K key) {
    Conditions.notNull(key);
    try {
      writeLock.lock();
      V remove = map.remove(key);
      if (remove != null) {
        remove.onRemoved(size());
      }
      return remove;
    } finally {
      writeLock.unlock();
    }
  }

  /** 检查过期元素 */
  private void checkDead() {
    if (size() > capacity) {
      long now = TimeUtil.now();
      // 回收间隔小于一秒给与警告
      if (now - lastCollectTriggered < 1000) {
        LOGGER.warn("collecting triggers too frequently,may be Memory Leak or Highly Concurrent");
      }
      Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
      for (; iterator.hasNext(); ) {
        Map.Entry<K, V> next = iterator.next();
        V value = next.getValue();
        // 用户自定义不允许回收，就不允许回收
        if (!value.removable0()) {
          continue;
        }
        // 如果强制回收或者
        if (!forceCollect && !value.removable()) {
          continue;
        }
        iterator.remove();
        value.onRemoved(size());
      }
      lastCollectTriggered = now;
    }
  }

  @Override
  public boolean containsKey(K key) {
    try {
      readLock.lock();
      return map.containsKey(key);
    } finally {
      readLock.unlock();
    }
  }

  @Override
  public int size() {
    try {
      readLock.lock();
      return map.size();
    } finally {
      readLock.unlock();
    }
  }

  @Override
  public void forEach(Process2<K, V> process) {
    try {
      readLock.lock();
      map.forEach(process);
    } finally {
      readLock.unlock();
    }
  }

  public abstract static class LRUCacheable implements Cacheable {
    private long updateTime;

    /**
     * 多长时间过期(毫秒)
     *
     * @return
     */
    protected abstract long aliveTime();

    protected abstract boolean removable0();

    @Override
    public void onUsed() {
      updateTime = TimeUtil.now();
    }

    @Override
    public boolean removable() {
      return TimeUtil.now() - updateTime > aliveTime();
    }
  }
}
