/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.function.Function0;
import top.limitart.base.function.Process1;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.ThreadUnsafe;

import java.util.*;

/**
 * 高频率读取排行结构 主要用于读取频率远远大于写入频率,数量级为万级以下<br>
 * 注：每次更新排行信息都会有删除和插入操作，对于数组来说，虽然有内存拷贝，但是还是会比较慢<br>
 * 获取第几名是谁，获取谁是第几名，获取范围非常快<br>
 * 特别注意：更新影响排名相关的数据必须调用此类的方法进行更新，不能从外部更新，否则排名会错乱,数据量级达到千万级性能会急速下降！
 *
 * @param <K>
 * @param <V>
 * @author hank
 */
@ThreadUnsafe
public class StandardRankMap<K, V extends RankMap.RankObj<K>>
    implements RankMap<K, V> {
  private static final Logger LOGGER = LoggerFactory.getLogger(StandardRankMap.class);
  private static final int DEFAULT_SIZE = 128;
  private final List<V> list;
  private final Map<K, V> map;
  private final Comparator<V> comparator;
  private final int capacity;

  public static <K, V extends RankObj<K>> StandardRankMap<K, V> create(
      @NotNull Comparator<V> comparator, int capacity) {
    return new StandardRankMap(comparator, capacity);
  }

  public static <K, V extends RankObj<K>> StandardRankMap<K, V> create(
      @NotNull Comparator<V> comparator) {
    return new StandardRankMap(comparator, 0);
  }

  private StandardRankMap(@NotNull Comparator<V> comparator, int capacity) {
    this.map = new HashMap<>(capacity > 0 ? (int) (capacity / 0.75F + 1) : DEFAULT_SIZE);
    this.comparator = Conditions.notNull(comparator, "comparator");
    this.list = new ArrayList<>(capacity > 0 ? (capacity + 1) : DEFAULT_SIZE);
    this.capacity = capacity;
    if (this.capacity > 10000) {
      LOGGER.warn("excessive capacity leads to a serious decline in efficienc (suggest < 10000)");
    }
  }

  @Override
  public V get(@NotNull K key) {
    return map.get(key);
  }

  @Override
  public boolean containsKey(K key) {
    return map.containsKey(key);
  }

  @Override
  public V remove(@NotNull K key) {
    V old = map.remove(key);
    if (old != null) {
      listRemove(old);
    }
    return old;
  }

  @Override
  public V update(K key, Process1<V> consumer) {
    V old = map.get(key);
    Conditions.notNull(old, "key(%s) pufIfAbsent first!", key);
    return update0(old, consumer);
  }

  private V update0(V value, Process1<V> consumer) {
    // 在更新前先找到老值的位置
    listRemove(value);
    consumer.accept(value);
    int newIndex = binarySearch(value, true);
    list.add(newIndex, value);
    return value;
  }

  @Override
  public V putIfAbsent(V value) {
    V v = get(value.key());
    if (v != null) {
      return v;
    }
    rawPut(value);
    return null;
  }

  private void rawPut(V value) {
    Conditions.notNull(value, "value");
    int binarySearch = 0;
    if (size() > 0) {
      binarySearch = binarySearch(value, true);
    }
    map.put(value.key(), value);
    list.add(binarySearch, value);
    while (capacity > 0 && list.size() > this.capacity) {
      V pollLast = list.remove(list.size() - 1);
      Conditions.notNull(map.remove(pollLast.key()), "remove fail????");
    }
    Conditions.args(map.size() == list.size(), "state error!");
  }

  @Override
  public V updateOrPut(K key, Process1<V> consumer, Function0<V> instance) {
    V v = get(key);
    if (v == null) {
      v = instance.get();
      consumer.accept(v);
      rawPut(v);
    } else {
      update0(v, consumer);
    }
    return v;
  }

  @Override
  public void clear() {
    list.clear();
    map.clear();
  }

  @Override
  public int size() {
    return list.size();
  }

  @Override
  public int getIndex(@NotNull K key) {
    if (!this.map.containsKey(key)) {
      return -1;
    }
    V v = map.get(key);
    return binarySearch(v, false);
  }

  @Override
  public List<V> getAll() {
    return new ArrayList<>(list);
  }

  @Override
  public List<V> getRange(int startIndex, int endIndex) {
    List<V> temp = new ArrayList<>();
    int start = startIndex;
    int end = endIndex + 1;
    int size = size();
    if (size == 0) {
      return temp;
    }
    if (start < 0) {
      start = 0;
    }
    if (end < start) {
      end = start;
    }
    if (end >= size) {
      end = size;
    }
    if (start == end) {
      V at = getAt(start);
      if (at != null) {
        temp.add(at);
      }
      return temp;
    }
    temp.addAll(list.subList(start, end));
    return temp;
  }

  @Override
  public V getAt(int at) {
    int size = size();
    if (size == 0) {
      return null;
    }
    if (at < 0) {
      return null;
    }
    if (at >= size) {
      return null;
    }
    return list.get(at);
  }

  @Override
  public String toString() {
    return list.toString();
  }

  private void listRemove(V old) {
    int i = binarySearch(old, false);
    V remove = list.remove(i);
    Conditions.args(
        old == remove, "remove obj not equals,comparator or key comparator must be error!");
  }

  private int binarySearch(V v, boolean similar) {
    int low = 0;
    int high = size() - 1;
    while (low <= high) {
      int mid = (low + high) >>> 1;
      V midVal = list.get(mid);
      int cmp = this.comparator.compare(midVal, v);
      if (cmp == 0) {
        cmp = midVal.compareKey(v.key());
      }
      if (cmp < 0) low = mid + 1;
      else if (cmp > 0) high = mid - 1;
      else return mid;
    }
    if (similar) {
      return low;
    }
    throw new IllegalStateException("can not find pos,maybe change the value without this map???");
  }
}
