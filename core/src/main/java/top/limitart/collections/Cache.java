package top.limitart.collections;

import top.limitart.base.function.Process2;

/**
 * 缓存
 *
 * @author hank
 * @version 2018/11/13 0013 20:41
 */
public interface Cache<K, V extends Cache.Cacheable> {
  /**
   * 获取值
   *
   * @param key
   * @return
   */
  V get(K key);

  /**
   * 放入缓存
   *
   * @param key
   * @param value
   * @return
   */
  V put(K key, V value);

  /**
   * 移除元素
   *
   * @param key
   * @return
   */
  V remove(K key);
  /**
   * 是否包含键
   *
   * @param key
   * @return
   */
  boolean containsKey(K key);

  /**
   * 当前实际大小
   *
   * @return
   */
  int size();

  /**
   * 遍历
   *
   * @param process
   */
  void forEach(Process2<K, V> process);

  interface Cacheable {
    /**
     * 是否可以移除
     *
     * @return
     */
    boolean removable();

    /** 当元素被使用时 */
    void onUsed();

    /** 当被移除时 */
    void onRemoved(int currentSize);
  }
}
