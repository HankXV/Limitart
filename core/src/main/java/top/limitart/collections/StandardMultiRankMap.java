package top.limitart.collections;

import top.limitart.base.Conditions;
import top.limitart.base.function.Function0;
import top.limitart.base.function.Process1;
import top.limitart.base.label.ThreadUnsafe;

import java.util.*;

/**
 * 复合排行Map
 *
 * @author hank
 * @version 2018/4/17 0017 16:41
 */
@ThreadUnsafe
public class StandardMultiRankMap<K, V extends RankMap.RankObj<K>> implements MultiRankMap<K, V> {
  private static final int DEFAULT_SIZE = 100;
  private final Map<K, V> map;
  private final Map<Comparator<V>, List<V>> list;

  @SafeVarargs
  public static <K, V extends RankMap.RankObj<K>> StandardMultiRankMap<K, V> create(
      Comparator<V>... comparators) {
    return new StandardMultiRankMap<>(comparators);
  }

  @SafeVarargs
  private StandardMultiRankMap(Comparator<V>... comparators) {
    Conditions.args(comparators != null && comparators.length > 0, "comparators needed!");
    list = new HashMap<>(DEFAULT_SIZE);
    map = new HashMap<>(DEFAULT_SIZE);
    for (Comparator comparator : comparators) {
      Conditions.args(!list.containsKey(comparator), "comparator duplicated:%s", comparator);
      list.put(comparator, new ArrayList<>(DEFAULT_SIZE));
    }
  }

  @Override
  public void clear() {
    map.clear();
    list.values().forEach(List::clear);
  }

  @Override
  public V get(K key) {
    return map.get(key);
  }

  @Override
  public boolean containsKey(K key) {
    return map.containsKey(key);
  }

  @Override
  public V remove(K key) {
    V remove = map.remove(key);
    if (remove != null) {
      list.forEach((c, l) -> listRemove(c, l, remove));
    }
    return remove;
  }

  @Override
  public V update(K key, Process1<V> consumer) {
    V old = map.get(key);
    Conditions.notNull(old, "key(%s) pufIfAbsent first!", key);
    return update0(old, consumer);
  }

  private V update0(V value, Process1<V> consumer) {
    // 在更新前先找到老值的位置
    list.forEach((c, l) -> listRemove(c, l, value));
    consumer.accept(value);
    list.forEach((c, l) -> l.add(binarySearch(c, l, value, true), value));
    return value;
  }

  @Override
  public V putIfAbsent(V value) {
    Conditions.notNull(value, "value");
    V v = map.get(value.key());
    if (v != null) {
      return v;
    }
    rawPut(value);
    return null;
  }

  private void rawPut(V value) {
    Conditions.notNull(value, "value");
    map.put(value.key(), value);
    boolean needSearch = size() > 0;
    list.forEach(
        (c, l) -> {
          int binarySearch = 0;
          // 这里必须要用列表的size而不是map的size
          if (needSearch) {
            binarySearch = binarySearch(c, l, value, true);
          }
          l.add(binarySearch, value);
        });
  }

  @Override
  public V updateOrPut(K key, Process1<V> consumer, Function0<V> instance) {
    V v = get(key);
    if (v == null) {
      v = instance.get();
      consumer.accept(v);
      rawPut(v);
    } else {
      update0(v, consumer);
    }
    return v;
  }

  @Override
  public int size() {
    return map.size();
  }

  @Override
  public int getIndex(Comparator<V> comparator, K key) {
    if (!this.map.containsKey(key)) {
      return -1;
    }
    V v = map.get(key);
    return binarySearch(comparator, list.get(comparator), v, false);
  }

  @Override
  public List<V> getRange(Comparator<V> comparator, int startIndex, int endIndex) {
    List<V> vs = list.get(comparator);
    Conditions.notNull(vs, "comparator not exist", comparator);
    List<V> temp = new ArrayList<>();
    int start = startIndex;
    int end = endIndex + 1;
    int size = vs.size();
    if (size == 0) {
      return temp;
    }
    if (start < 0) {
      start = 0;
    }
    if (end < start) {
      end = start;
    }
    if (end >= size) {
      end = size;
    }
    if (start == end) {
      V at = getAt(comparator, start);
      if (at != null) {
        temp.add(at);
      }
      return temp;
    }
    temp.addAll(vs.subList(start, end));
    return temp;
  }

  @Override
  public List<V> getAll(Comparator<V> comparator) {
    List<V> vs = list.get(comparator);
    Conditions.notNull(vs, "comparator not exist", comparator);
    return new ArrayList<>(vs);
  }

  @Override
  public V getAt(Comparator<V> comparator, int at) {
    List<V> vs = list.get(comparator);
    Conditions.notNull(vs, "comparator not exist", comparator);
    int size = vs.size();
    if (size == 0) {
      return null;
    }
    if (at < 0) {
      return null;
    }
    if (at >= size) {
      return null;
    }
    return vs.get(at);
  }

  private void listRemove(Comparator<V> comparator, List<V> list, V old) {
    int i = binarySearch(comparator, list, old, false);
    V remove = list.remove(i);
    Conditions.args(
        old == remove, "remove obj not equals,comparator or key comparator must be error!");
  }

  private int binarySearch(Comparator<V> comparator, List<V> list, V v, boolean similar) {
    int low = 0;
    int high = list.size() - 1;
    while (low <= high) {
      int mid = (low + high) >>> 1;
      V midVal = list.get(mid);
      int cmp = comparator.compare(midVal, v);
      if (cmp == 0) {
        cmp = midVal.compareKey(v.key());
      }
      if (cmp < 0) low = mid + 1;
      else if (cmp > 0) high = mid - 1;
      else return mid;
    }
    if (similar) {
      return low;
    }
    throw new IllegalStateException("can not find pos,maybe change the value without this map???");
  }
}
