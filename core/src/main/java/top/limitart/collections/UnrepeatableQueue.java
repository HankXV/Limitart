/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.collections;

import top.limitart.base.function.Process1;
import top.limitart.base.function.Processes;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.Nullable;
import top.limitart.base.label.ThreadSafe;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 不重复队列
 *
 * @author hank
 */
@ThreadSafe
public class UnrepeatableQueue<V> {
  private ReadWriteLock lock = new ReentrantReadWriteLock();
  private Lock readLock = lock.readLock();
  private Lock writeLock = lock.writeLock();
  private final Set<V> set = new HashSet<>();
  private final Queue<V> queue = new LinkedList<>();

  /**
   * 当前队列大小
   *
   * @return
   */
  public int size() {
    try {
      readLock.lock();
      return set.size();
    } finally {
      readLock.unlock();
    }
  }

  /**
   * 是否为空
   *
   * @return
   */
  public boolean isEmpty() {
    try {
      readLock.lock();
      return set.isEmpty();
    } finally {
      readLock.unlock();
    }
  }

  /** 清除全部元素 */
  public void clear() {
    try {
      writeLock.lock();
      set.clear();
      queue.clear();
    } finally {
      writeLock.unlock();
    }
  }

  /**
   * 是否包含元素
   *
   * @param value
   * @return
   */
  public boolean contains(V value) {
    try {
      readLock.lock();
      return set.contains(value);
    } finally {
      readLock.unlock();
    }
  }

  /**
   * 压入队列
   *
   * @param value
   */
  public boolean offer(@NotNull V value) {
    try {
      if (!contains(value)) {
        writeLock.lock();
        set.add(value);
        queue.offer(value);
        return true;
      }
      return false;
    } finally {
      writeLock.unlock();
    }
  }

  /**
   * 取出一个元素
   *
   * @return
   */
  public @Nullable V poll() {
    try {
      writeLock.lock();
      V poll = queue.poll();
      if (poll != null) {
        set.remove(poll);
      }
      return poll;
    } finally {
      writeLock.unlock();
    }
  }

  /**
   * 按指定数量出队列
   *
   * @param pollCount
   * @param proc
   */
  public void pollTo(int pollCount, @Nullable Process1<V> proc) {
    try {
      writeLock.lock();
      V temp;
      for (int count = 0; count <= pollCount && (temp = poll()) != null; ++count) {
        Processes.invoke(proc, temp);
      }
    } finally {
      writeLock.unlock();
    }
  }
}
