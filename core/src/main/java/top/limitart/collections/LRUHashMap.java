/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.collections;

import top.limitart.base.Conditions;
import top.limitart.base.function.Process2;
import top.limitart.base.function.Processes;
import top.limitart.base.function.Test2;
import top.limitart.base.function.Tests;
import top.limitart.base.label.ThreadUnsafe;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * LRU非线程安全型Map
 *
 * @param <K>
 * @param <V>
 * @author hank
 */
@ThreadUnsafe
public class LRUHashMap<K, V> extends LinkedHashMap<K, V> {
  private transient Process2<K, V> onRemove;
  private transient Test2<K, V> canRemove;

  public LRUHashMap(int initSize, Test2<K, V> canRemove, Process2<K, V> onRemove) {
    super((int) Math.ceil(initSize / 0.75f) + 2, 0.75f, true);
    Conditions.positive(initSize);
    this.canRemove = Conditions.notNull(canRemove);
    this.onRemove = Conditions.notNull(onRemove);
  }

  @Override
  protected boolean removeEldestEntry(Entry<K, V> eldest) {
    if (Tests.invoke(canRemove, eldest.getKey(), eldest.getValue())) {
      Processes.invoke(onRemove, eldest.getKey(), eldest.getValue());
      return true;
    }
    return false;
  }

  @Override
  public void clear() {
    for (Entry<K, V> entry : super.entrySet()) {
      Processes.invoke(onRemove, entry.getKey(), entry.getValue());
    }
    super.clear();
  }

  @Override
  public V remove(Object key) {
    V remove = super.remove(key);
    if (remove != null) {
      Processes.invoke(onRemove, (K) key, remove);
    }
    return remove;
  }
}
