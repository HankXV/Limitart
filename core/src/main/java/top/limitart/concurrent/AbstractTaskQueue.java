/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.concurrent;

import top.limitart.base.Conditions;
import top.limitart.base.label.NotNull;
import top.limitart.util.TimeUtil;

import java.text.ParseException;
import java.util.concurrent.*;

/**
 * 抽象任务队列
 *
 * @author hank
 */
public abstract class AbstractTaskQueue implements TaskQueue {
  private static final ScheduledExecutorService scheduledExecutorService =
      Executors.newSingleThreadScheduledExecutor(
          new NamedThreadFactory() {
            @Override
            public String namePrefix() {
              return "task-queue-scheduler";
            }
          });

  @Override
  public CronCommand schedule(
      @NotNull String name, @NotNull String cronExpression, @NotNull Runnable command)
      throws ParseException {
    Conditions.notNull(name, "must have a cmd name");
    Conditions.notNull(cronExpression, "must have a cron expression");
    Conditions.notNull(command, "must have a command");
    CronCommand cmd = CronCommand.createCommand(name, cronExpression, command);
    scheduleCronCommand(cmd);
    return cmd;
  }

  private void scheduleCronCommand(CronCommand cmd) {
    long now = TimeUtil.now();
    long next = cmd.evaluateNextDelayTime();
    if (next == 0 || next < now) {
      return;
    }
    long delay = Math.max(1000, next - now);
//    System.out.println(delay);
    cmd.updateFuture(
        schedule(
            () -> {
              try {
                cmd.run();
              } finally {
                scheduleCronCommand(cmd);
              }
            },
            delay,
            TimeUnit.MILLISECONDS));
  }

  @Override
  public ScheduledFuture<?> schedule(@NotNull Runnable command, long delay, TimeUnit unit) {
    return scheduledExecutorService.schedule(() -> execute(command), delay, unit);
  }

  @Override
  public ScheduledFuture<?> scheduleAtFixedRate(
      @NotNull Runnable command, long initialDelay, long period, TimeUnit unit) {
    return scheduledExecutorService.scheduleAtFixedRate(
        () -> execute(command), initialDelay, period, unit);
  }

  @Override
  public ScheduledFuture<?> scheduleWithFixedDelay(
      @NotNull Runnable command, long initialDelay, long delay, TimeUnit unit) {
    return scheduledExecutorService.scheduleWithFixedDelay(
        () -> execute(command), initialDelay, delay, unit);
  }

  @Override
  public <T> Future<T> submit(@NotNull Callable<T> task) {
    FutureTask<T> futureTask = new FutureTask<>(task);
    execute(futureTask);
    return futureTask;
  }

  @Override
  public <T> Future<T> submit(@NotNull Runnable task, T result) {
    FutureTask<T> futureTask = new FutureTask<>(task, result);
    execute(futureTask);
    return futureTask;
  }

  @Override
  public Future<?> submit(@NotNull Runnable task) {
    FutureTask<?> futureTask = new FutureTask<>(task, null);
    execute(futureTask);
    return futureTask;
  }

  @Override
  public void execute(Runnable command) {
    Conditions.notNull(command, "command");
    if (thread() == Thread.currentThread()) {
      command.run();
      return;
    }
    execute0(command);
  }

  protected abstract void execute0(Runnable command);
}
