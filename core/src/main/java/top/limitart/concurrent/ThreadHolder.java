package top.limitart.concurrent;

public interface ThreadHolder {

  /**
   * 获取线程
   *
   * @return
   */
  Thread thread();
}
