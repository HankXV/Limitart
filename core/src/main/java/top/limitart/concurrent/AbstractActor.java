/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.concurrent;

import top.limitart.base.Conditions;
import top.limitart.base.WeakRefHolder;
import top.limitart.base.function.Process0;
import top.limitart.base.function.Process1;

/**
 * 抽象资源占有者
 *
 * @author hank
 */
public abstract class AbstractActor<R> implements Actor<R> {
  private final transient WeakRefHolder<R> weakRefHolder = WeakRefHolder.empty();

  @Override
  public void leave(R oldPlace) {
    Conditions.notNull(oldPlace, "no old place!");
    R where = where();
    Conditions.notNull(where, "no place to hold in current state,so can not leave!");
    Conditions.args(oldPlace == where, "incorrect place to give!old:%s,yours:%s", oldPlace, where);
    weakRefHolder.set(null);
  }

  @Override
  public void joinWhenFree(R newPlace, Process0 onSuccess, Process1<Exception> onFail) {
    Conditions.notNull(newPlace, "no new res!");
    R where = where();
    if (where != null) {
      if (where != newPlace) {
        onFail.accept(new IllegalStateException("already have a place!"));
      } else {
        onSuccess.run();
      }
    } else {
      weakRefHolder.set(newPlace);
      onSuccess.run();
    }
  }

  @Override
  public void forceJoin(R newPlace, Process0 onSuccess, Process1<Exception> onFail) {
    R where = where();
    if (where != null) {
      leave(where);
    }
    joinWhenFree(newPlace, onSuccess, onFail);
  }

  @Override
  public final R where() {
    return weakRefHolder.get();
  }
}
