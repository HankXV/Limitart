/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.concurrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.CronExpression;
import top.limitart.base.function.Function0;
import top.limitart.base.function.Process0;
import top.limitart.base.label.NotNull;
import top.limitart.util.TimeUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * 任务队列接口
 *
 * @author hank
 */
public interface TaskQueue extends Executor, ThreadHolder {

  Logger LOGGER = LoggerFactory.getLogger(TaskQueue.class);
  // 同步执行最大阻塞时间
  long SYNC_OVER_TIME = 2000;

  static TaskQueue create(@NotNull String threadName) {
    return DisruptorTaskQueue.create(threadName);
  }

  static TaskQueue create(@NotNull SingletonThreadFactory threadFactory) {
    return DisruptorTaskQueue.create(threadFactory);
  }

  /**
   * 以Cron表达式的方式启动一个任务
   *
   * @param name           该任务名称 (与表达式组成一个唯一识别)
   * @param cronExpression 该任务Cron表达式
   * @param command
   * @return
   * @throws ParseException Cron表达式解析错误
   */
  CronCommand schedule(
      @NotNull String name, @NotNull String cronExpression, @NotNull Runnable command)
      throws ParseException;

  ScheduledFuture<?> schedule(@NotNull Runnable command, long delay, TimeUnit unit);

  ScheduledFuture<?> scheduleAtFixedRate(
      @NotNull Runnable command, long initialDelay, long period, TimeUnit unit);

  ScheduledFuture<?> scheduleWithFixedDelay(
      @NotNull Runnable command, long initialDelay, long delay, TimeUnit unit);

  <T> Future<T> submit(@NotNull Callable<T> task);

  <T> Future<T> submit(@NotNull Runnable task, T result);

  Future<?> submit(@NotNull Runnable task);

  /**
   * 与其他消息队列协作任务(异步) 当前线程不会等待指定线程执行完毕
   *
   * @param another   其他消息队列
   * @param process   其他消息队里执行的任务
   * @param onSuccess 在本队列执行的成功回调
   * @param onFail    在本队列执行的失败回调
   */
  default void onAnotherAsync(
      TaskQueue another, Function0<Boolean> process, Process0 onSuccess, Process0 onFail) {
    Conditions.notNull(another, "another");
    Conditions.notNull(process, "process");
    Conditions.notNull(onSuccess, "onSuccess");
    Conditions.notNull(onFail, "onFail");
    execute(
        () ->
            another.execute(
                () -> {
                  if (process.get()) {
                    execute(onSuccess::run);
                  } else {
                    execute(onFail::run);
                  }
                }));
  }

  /**
   * 与其他消息队列协作任务(同步) 保证在响应速度要求低的调用响应线程要求高的线程，阻塞前者以确保运行在前者线程的数据正确性
   *
   * @param another
   * @param process
   * @param onSuccess
   */
  default void onAnotherSync(TaskQueue another, Function0<Boolean> process, Process0 onSuccess) {
    onAnotherSync(another, process, onSuccess, null);
  }

  /**
   * 与其他消息队列协作任务(同步) 保证在响应速度要求低的调用响应线程要求高的线程，阻塞前者以确保运行在前者线程的数据正确性
   *
   * @param another
   * @param process
   * @param onSuccess
   * @param onFail
   */
  default void onAnotherSync(
      TaskQueue another, Function0<Boolean> process, Process0 onSuccess, Process0 onFail) {
    Conditions.notNull(another, "another");
    Conditions.notNull(process, "process");
    Conditions.notNull(onSuccess, "onSuccess");
    execute(
        () -> {
          Future<Boolean> submit = another.submit(process::get);
          Boolean result = null;
          try {
            result = submit.get(SYNC_OVER_TIME, TimeUnit.MILLISECONDS);
          } catch (InterruptedException | ExecutionException | TimeoutException e) {
            LOGGER.error("task queue " + another.thread().getName(), e);
          }
          if (result == null || !result) {
            if (onFail != null) {
              onFail.run();
            }
          } else {
            onSuccess.run();
          }
        });
  }

  /**
   * 关闭
   */
  void shutdown();

  /**
   * 是否运行在此线程上，否则抛异常
   */
  default void assertSameThread() {
    Conditions.sameThread(thread(), "must call it on %s", this);
  }

  /**
   * Cron表达式定时任务
   *
   * @author hank
   * @version 2018/12/11 0011 11:21
   */
  class CronCommand implements Runnable {

    private String cmdName;
    private CronExpression cronExpression;
    private Runnable cmd;
    private ScheduledFuture<?> future;

    public static CronCommand createCommand(String cmdName, String cronExpression, Runnable cmd)
        throws ParseException {
      CronExpression expression = new CronExpression(cronExpression);
      // TODO 可能有些检查
      return new CronCommand(cmdName, expression, cmd);
    }

    private CronCommand(String cmdName, CronExpression cronExpression, Runnable cmd) {
      this.cmdName = cmdName;
      this.cronExpression = cronExpression;
      this.cmd = cmd;
    }

    public void updateFuture(ScheduledFuture<?> future) {
      this.future = Conditions.notNull(future);
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
        return true;
      }
      if (object == null || getClass() != object.getClass()) {
        return false;
      }
      CronCommand that = (CronCommand) object;
      return Objects.equals(cmdName, that.cmdName)
          && Objects.equals(cronExpression, that.cronExpression);
    }

    @Override
    public int hashCode() {
      return Objects.hash(cmdName, cronExpression);
    }

    @Override
    public String toString() {
      return name() + "/" + cron();
    }

    @Override
    public void run() {
      Conditions.notNull(this.cmd);
      this.cmd.run();
    }

    public String name() {
      return cmdName;
    }

    public String cron() {
      return this.cronExpression.getCronExpression();
    }

    /**
     * 计算出下一次触发时间到现在的延迟时间
     *
     * @return
     */
    public long evaluateNextDelayTime() {
      Date timeAfter = cronExpression.getTimeAfter(TimeUtil.date());
      return timeAfter == null ? 0 : timeAfter.getTime();
    }

    public void cancel() {
      if (this.future == null) {
        return;
      }
      if (this.future.isCancelled()) {
        return;
      }
      // 这个参数true和false都没什么卵用
      this.future.cancel(true);
      this.future = null;
    }
  }
}
