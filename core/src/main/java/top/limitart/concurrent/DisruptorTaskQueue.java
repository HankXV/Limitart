/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.concurrent;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.InsufficientCapacityException;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Alone;
import top.limitart.base.Conditions;
import top.limitart.base.UncatchableException;
import top.limitart.base.function.Process3;
import top.limitart.base.function.Processes;

/**
 * 消息队列线程
 *
 * @author Hank
 */
public class DisruptorTaskQueue extends AbstractTaskQueue {

  private static final Logger LOGGER = LoggerFactory.getLogger(DisruptorTaskQueue.class);
  private final Disruptor<Alone<Runnable>> disruptor;
  private final SingletonThreadFactory threadFactory;
  private Process3<Runnable, Throwable, Long> exception;
  private ExecuteStrategy strategy = ExecuteStrategy.BLOCK_BUT_NOTICE;

  public static DisruptorTaskQueue create(SingletonThreadFactory threadFactory) {
    return new DisruptorTaskQueue(threadFactory);
  }

  public static DisruptorTaskQueue create(String threadName) {
    return new DisruptorTaskQueue(threadName);
  }

  public static DisruptorTaskQueue create(String threadName, int bufferSize) {
    return new DisruptorTaskQueue(threadName, bufferSize);
  }

  public static DisruptorTaskQueue create(SingletonThreadFactory threadFactory, int bufferSize) {
    return new DisruptorTaskQueue(threadFactory, bufferSize);
  }

  private DisruptorTaskQueue(SingletonThreadFactory threadFactory) {
    this(threadFactory, 2 << 12); // 4096
  }

  private DisruptorTaskQueue(String threadName) {
    this(threadName, 2 << 12); // 4096
  }

  /**
   * 构造函数
   *
   * @param threadName
   * @param bufferSize 指定RingBuffer的大小
   */
  @SuppressWarnings("unchecked")
  private DisruptorTaskQueue(String threadName, int bufferSize) {
    this(
        new SingletonThreadFactory() {
          @Override
          public void onThreadCreated(Thread thread) {
            thread.setName(threadName);
          }
        },
        bufferSize);
  }

  /**
   * 构造函数
   *
   * @param threadFactory
   * @param bufferSize    指定RingBuffer的大小
   */
  @SuppressWarnings("unchecked")
  private DisruptorTaskQueue(SingletonThreadFactory threadFactory, int bufferSize) {
    this.threadFactory = threadFactory;
    disruptor =
        new Disruptor<>(
            Alone::of, bufferSize, threadFactory, ProducerType.MULTI, new BlockingWaitStrategy());
    disruptor.handleEventsWith(
        (event, sequence, endOfBatch) -> {
          try {
            event.get().run();
          } catch (Exception e) {
            LOGGER.error("invoke handler error", e);
          } finally {
            event.set(null);
          }
        });
    // prevent Worker Threads from dying
    disruptor.setDefaultExceptionHandler(
        new ExceptionHandler<Alone<Runnable>>() {

          @Override
          public void handleEventException(Throwable ex, long sequence, Alone<Runnable> event) {
            LOGGER.error("sequence " + sequence + " error!", ex);
            Processes.invoke(exception, event.get(), ex, sequence);
          }

          @Override
          public void handleOnStartException(final Throwable ex) {
            LOGGER.error("Exception during onStart()", ex);
          }

          @Override
          public void handleOnShutdownException(final Throwable ex) {
            LOGGER.error("Exception during onShutdown()", ex);
          }
        });
    disruptor.start();
    LOGGER.info("thread " + threadFactory.get().getName() + " start!");
  }

  /**
   * 错误处理
   *
   * @param exception
   * @return
   */
  public DisruptorTaskQueue exception(Process3<Runnable, Throwable, Long> exception) {
    this.exception = exception;
    return this;
  }

  /**
   * 设置执行队列满时的策略
   *
   * @param strategy
   * @return
   */
  public DisruptorTaskQueue executeStrategy(ExecuteStrategy strategy) {
    this.strategy = strategy;
    return this;
  }

  @Override
  public void execute0(Runnable runnable) {
    Conditions.notNull(runnable);
    try {
      long next = disruptor.getRingBuffer().tryNext();
      publish(runnable, next);
    } catch (InsufficientCapacityException e) {
      //这里用不用tryPublishEvent,他内部会默认阻塞当前线程，知道能插入为止。用try让他快速失败，我们好调试我们的程序到底是消费过慢还是生产过快，因为此队列假设的是消费必须比生产快
      if (this.strategy == ExecuteStrategy.FAST_FAIL
          || this.strategy == ExecuteStrategy.BLOCK_BUT_NOTICE) {
        UncatchableException uncatchableException = new UncatchableException(
            "task queue " + this.thread().getName()
                + " is not enough to execute task, maybe customer is too slowly or provider is too fast! ");
        LOGGER.error("no capacity", uncatchableException);
        Processes.invoke(exception, runnable,
            uncatchableException,
            disruptor.getCursor() + 1);
      }
      if (this.strategy == ExecuteStrategy.BLOCK_BUT_NOTICE
          || this.strategy == ExecuteStrategy.BLOCK) {
        //阻塞
        long next = disruptor.getRingBuffer().next();
        publish(runnable, next);
      }
    }
  }

  private void publish(Runnable runnable, long seq) {
    try {
      disruptor.getRingBuffer().get(seq).set(runnable);
    } finally {
      disruptor.getRingBuffer().publish(seq);
    }
  }

  @Override
  public Thread thread() {
    return threadFactory.thread();
  }

  @Override
  public void shutdown() {
    if (disruptor != null) {
      disruptor.shutdown();
      LOGGER.info("thread " + threadFactory.get().getName() + " stop!");
    }
  }

  public enum ExecuteStrategy {
    /**
     * 如果无法入队，则直接失败
     */
    FAST_FAIL,
    /**
     * 如果无法入队，则阻塞等待，但会警告
     */
    BLOCK_BUT_NOTICE,
    /**
     * 如果无法入队，则阻塞等待
     */
    BLOCK
  }
}
