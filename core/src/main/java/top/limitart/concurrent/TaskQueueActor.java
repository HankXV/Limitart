/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.concurrent;

import top.limitart.base.Conditions;
import top.limitart.base.function.Function0;
import top.limitart.base.function.Process0;

/** 消息队列占用者 */
public class TaskQueueActor extends AbstractThreadActor<TaskQueue> {

  @Override
  public boolean sameThread(TaskQueue taskQueue) {
    return taskQueue.thread() == Thread.currentThread();
  }

  /**
   * 与其他消息队列协作任务(异步) 当前线程不会等待指定线程执行完毕
   *
   * @param another 其他消息队列
   * @param process 其他消息队里执行的任务
   * @param onSuccess 在本队列执行的成功回调
   * @param onFail 在本队列执行的失败回调
   */
  public void onAnotherAsync(
      TaskQueue another, Function0<Boolean> process, Process0 onSuccess, Process0 onFail) {
    TaskQueue where = where();
    Conditions.notNull(where, "no place to hold!");
    where.onAnotherAsync(another, process, onSuccess, onFail);
  }

  /**
   * 与其他消息队列协作任务(同步) 保证在响应速度要求低的调用响应线程要求高的线程，阻塞前者以确保运行在前者线程的数据正确性
   *
   * @param another
   * @param process
   * @param onSuccess
   */
  public void onAnotherSync(TaskQueue another, Function0<Boolean> process, Process0 onSuccess) {
    onAnotherSync(another, process, onSuccess, null);
  }

  public void onAnotherSync(
      TaskQueue another, Function0<Boolean> process, Process0 onSuccess, Process0 onFail) {
    TaskQueue where = where();
    Conditions.notNull(where, "no place to hold!");
    where.onAnotherSync(another, process, onSuccess, onFail);
  }
}
