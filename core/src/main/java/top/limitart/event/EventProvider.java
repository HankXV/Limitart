/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.event;

import top.limitart.base.function.Process1;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.Nullable;

import java.util.concurrent.Executor;

/**
 * 事件提供者
 *
 * @author hank
 * @version 2019/3/25 0025 14:04
 */
public interface EventProvider {
  /**
   * 监听某事件(在指定的线程执行事件)
   *
   * @param eventType
   * @param executor
   * @param process
   */
  <E extends Event> int listen(
          @NotNull Class<E> eventType, @Nullable Executor executor, @NotNull Process1<E> process);

  /**
   * 监听某事件(在事件发出的线程执行)
   *
   * @param eventType
   * @param process
   * @param <E>
   */
  default <E extends Event> int listen(@NotNull Class<E> eventType, @NotNull Process1<E> process) {
    return listen(eventType, null, process);
  }

  /**
   * 取消监听某事件
   *
   * @param eventType
   * @param <E>
   */
  <E extends Event> void notListen(@NotNull Class<E> eventType, int code);

  /**
   * 发出某事件
   *
   * @param event
   * @param <E>
   */
  <E extends Event> void post(E event);
}
