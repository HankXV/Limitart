/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.UncatchableException;
import top.limitart.base.function.Process1;
import top.limitart.collections.ConcurrentHashSet;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * 抽象事件提供器
 *
 * @author hank
 * @version 2019/3/25 0025 14:16
 */
public abstract class AbstractEventProvider implements EventProvider {

  private static Logger LOGGER = LoggerFactory.getLogger(AbstractEventProvider.class);
  private Map<Class<? extends Event>, Set<Process>> events = new ConcurrentHashMap<>();

  @Override
  public <E extends Event> int listen(Class<E> eventType, Executor executor, Process1<E> process) {
    Process p = new Process(executor, process);
    Set<Process> processes = events.computeIfAbsent(eventType, e -> new ConcurrentHashSet<>());
    Conditions.args(processes.add(p), "duplicated listener for event %s", eventType.getName());
    return p.hashCode();
  }

  @Override
  public <E extends Event> void notListen(Class<E> eventType, int handle) {
    Set<Process> processes = events.get(eventType);
    if (processes == null) {
      throw new UncatchableException("there is no event set named %s", eventType.getName());
    }
    if (!processes.remove(new ProcessHash(handle))) {
      throw new UncatchableException("there is no event listener handled with id %s and named %s",
          handle, eventType.getName());
    }
  }

  @Override
  public <E extends Event> void post(E event) {
    Conditions.notNull(event, "post null event ??");
    Set<Process> processes = events.get(event.getClass());
    if (processes == null) {
      return;
    }
    for (Process process : processes) {
      if (process.executor == null) {
        execute(event, process);
      } else {
        process.executor.execute(() -> execute(event, process));
      }
    }
  }

  /**
   * 当事件正在发出时
   *
   * @param event
   * @return true 允许发出 false 不允许发出
   */
  protected abstract boolean onPost(Event event);

  private <E extends Event> void execute(E event, Process process) {
    try {
      if (!onPost(event)) {
        //        LOGGER.trace("event {} are not allowed to post out", event.getClass().getName());
        return;
      }
    } catch (Exception e) {
      LOGGER.error("before post error", e);
    }
    try {
      process.process.accept(event);
    } catch (Exception e) {
      LOGGER.error("event execute error", e);
    }
  }

  private static class Process {

    private Executor executor;
    private Process1 process;

    public Process(Executor executor, Process1 process) {
      this.executor = executor;
      this.process = Conditions.notNull(process, "process");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
        return true;
      }
      if (object == null || getClass() != object.getClass()) {
        return false;
      }
      Process that = (Process) object;
      return Objects.equals(process, that.process);
    }

    @Override
    public int hashCode() {
      return Objects.hash(process);
    }
  }

  private static class ProcessHash {

    private int hash;

    public ProcessHash(int hash) {
      this.hash = hash;
    }

    @Override
    public boolean equals(Object object) {
      return hashCode() == object.hashCode();
    }

    @Override
    public int hashCode() {
      return this.hash;
    }
  }
}
