/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 编码相关
 *
 * @author hank
 * @version 2018/4/20 0020 11:06
 */
public final class CodecUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(CodecUtil.class);

  private CodecUtil() {
  }

  public static String urlEncode(String str) throws UnsupportedEncodingException {
    return URLEncoder.encode(str, StandardCharsets.UTF_8.name());
  }

  public static String urlDecode(String str) throws UnsupportedEncodingException {
    return URLDecoder.decode(str, StandardCharsets.UTF_8.name());
  }

  public static byte[] toBase64Url(byte[] src) {
    return Base64.getUrlEncoder().encode(src);
  }

  public static String toBase64StrUrl(byte[] src) {
    return Base64.getUrlEncoder().encodeToString(src);
  }

  public static byte[] fromBase64Url(byte[] src) {
    return Base64.getUrlDecoder().decode(src);
  }

  public static String fromBase64Url(String src) {
    return new String(Base64.getUrlDecoder().decode(src), StandardCharsets.UTF_8);
  }

  public static byte[] fromBase64Url2(String src) {
    return Base64.getUrlDecoder().decode(src);
  }

  public static byte[] toBase64(byte[] src) {
    return Base64.getEncoder().encode(src);
  }

  public static String toBase64Str(byte[] src) {
    return Base64.getEncoder().encodeToString(src);
  }

  public static byte[] fromBase64(byte[] src) {
    return Base64.getDecoder().decode(src);
  }

  public static String fromBase64(String src) {
    return new String(Base64.getDecoder().decode(src), StandardCharsets.UTF_8);
  }

  public static byte[] fromBase642(String src) {
    return Base64.getDecoder().decode(src);
  }

  public static String toMD5(String password) {
    return toMD5(password, "");
  }

  public static String toMD5(String password, String salt) {
    return toMD5((password + salt).getBytes(StandardCharsets.UTF_8));
  }

  public static String toMD5(byte[] b) {
    MessageDigest md5 = null;
    try {
      md5 = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      LOGGER.error("impossible!", e);
    }
    byte[] bytes = md5.digest(b);
    StringBuilder ret = new StringBuilder(bytes.length << 1);
    for (byte aByte : bytes) {
      ret.append(Character.forDigit((aByte >> 4) & 0xf, 16));
      ret.append(Character.forDigit(aByte & 0xf, 16));
    }
    return ret.toString();
  }

  public static byte[] toGZip(byte[] source) throws IOException {
    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out)) {
      gzip.write(source);
      return out.toByteArray();
    }
  }

  public static byte[] fromGZip(byte[] source) throws IOException {
    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(source);
        GZIPInputStream ungzip = new GZIPInputStream(in)) {
      byte[] buffer = new byte[256];
      int n;
      while ((n = ungzip.read(buffer)) >= 0) {
        out.write(buffer, 0, n);
      }
      return out.toByteArray();
    }
  }
}
