/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.util;

import java.nio.charset.StandardCharsets;
import java.util.Map.Entry;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import top.limitart.base.UncatchableException;
import top.limitart.json.JSON;
import top.limitart.json.JSONException;

/**
 * HTTP客户端 Created by Hank on 2019/3/17
 */
public final class HTTPClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(HTTPClient.class);

  private HTTPClient() {
  }

  public static byte[] download(String uri, Header... headers) {
    return download(uri, headers);
  }

  public static byte[] download(URI uri, Header... headers) {
    try {
      return toGetRequest(uri, null, headers).execute().returnContent().asBytes();
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return null;
  }

  public static String postForm(String uri, Map<String, Object> params, Header... headers) {
    return postForm(URI.create(uri), params, headers);
  }

  public static String postForm(URI uri, Map<String, Object> params, Header... headers) {
    try {
      return toPostRequest(uri, params, WherePostParam.FORM, headers).execute().returnContent()
          .asString(Consts.UTF_8);
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return null;
  }

  public static String postJson(String uri, Map<String, Object> params, Header... headers) {
    return postJson(URI.create(uri), params, headers);
  }

  public static String postJson(URI uri, Map<String, Object> params, Header... headers) {
    try {
      return toPostRequest(uri, params, WherePostParam.BODY_JSON, headers).execute().returnContent()
          .asString(Consts.UTF_8);
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return null;
  }

  public static Future<Content> postFormAsync(
      String uri, Map<String, ?> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return postFormAsync(URI.create(uri), params, executor, callback, headers);
  }

  public static Future<Content> postFormAsync(
      URI uri, Map<String, ?> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return Async.newInstance().use(executor)
        .execute(toPostRequest(uri, params, WherePostParam.FORM, headers), callback);
  }

  public static Future<Content> postJsonAsync(
      String uri, Map<String, ?> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return postJsonAsync(URI.create(uri), params, executor, callback, headers);
  }

  public static Future<Content> postJsonAsync(
      URI uri, Map<String, ?> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return Async.newInstance().use(executor)
        .execute(toPostRequest(uri, params, WherePostParam.BODY_JSON, headers), callback);
  }

  public static String get(String uri, Map<String, Object> params, Header... headers) {
    return get(URI.create(uri), params, headers);
  }

  public static String get(URI uri, Map<String, Object> params, Header... headers) {
    try {
      return toGetRequest(uri, params, headers).execute().returnContent().asString(Consts.UTF_8);
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return null;
  }

  public static Future<Content> getAsync(
      String uri, Map<String, Object> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return getAsync(URI.create(uri), params, executor, callback, headers);
  }

  public static Future<Content> getAsync(
      URI uri, Map<String, Object> params, Executor executor, FutureCallback<Content> callback,
      Header... headers) {
    return Async.newInstance().use(executor).execute(toGetRequest(uri, params, headers), callback);
  }

  private static Request toGetRequest(String uri, Map<String, Object> params, Header... headers) {
    return toGetRequest(URI.create(uri), params, headers);
  }

  private static Request toGetRequest(URI uri, Map<String, Object> params, Header... headers) {
    StringBuilder builder = new StringBuilder();
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      builder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
    }
    if (builder.length() > 0) {
      builder.deleteCharAt(builder.length() - 1);
    }
    String url = uri + "?" + builder.toString();
    LOGGER.info(url);
    return Request.Get(url).setHeaders(headers).connectTimeout(10000).socketTimeout(600000);
  }

  private static Request toPostRequest(String uri, Map<String, ?> params,
      WherePostParam wherePostParam, Header... headers) {
    return toPostRequest(URI.create(uri), params, wherePostParam, headers);
  }

  private static Request toPostRequest(URI uri, Map<String, ?> params,
      WherePostParam wherePostParam, Header... headers) {
    Request request = Request.Post(uri).setHeaders(headers)
        .connectTimeout(60000)
        .socketTimeout(600000);
    switch (wherePostParam) {
      case BODY_JSON: {
        String paramJson;
        try {
          paramJson = JSON.getDefault().toStr(params);
        } catch (JSONException e) {
          throw new UncatchableException(e);
        }
        if (paramJson != null) {
          request.bodyString(paramJson, ContentType.APPLICATION_JSON);
        }
      }
      break;
      case FORM: {
        Form form = Form.form();
        for (Entry<String, ?> entry : params.entrySet()) {
          String key = entry.getKey();
          Object value = entry.getValue();
          if (value != null) {
            form.add(key, value.toString());
          }
        }
        request.bodyForm(form.build(), StandardCharsets.UTF_8);
      }
      break;
    }
    if (LOGGER.isDebugEnabled()) {
      try {
        LOGGER.debug(uri + "==>" + JSON.getDefault().toStr(params));
      } catch (JSONException e) {
        LOGGER.error("log error", e);
      }
    }
    return request;
  }

  private enum WherePostParam {
    BODY_JSON, FORM
  }
}
