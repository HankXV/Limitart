package top.limitart.util;

import java.lang.reflect.Field;

/**
 * 对象工具
 *
 * @author hank
 * @version 2018/12/28 0028 20:27
 */
public final class ObjectUtil {

  private ObjectUtil() {
  }

  /**
   * 通过反射字段拷贝
   *
   * @param oldOne
   * @param newOne
   * @throws IllegalArgumentException
   * @throws IllegalAccessException
   */
  public static void copyBean(Object oldOne, Object newOne)
      throws IllegalArgumentException, IllegalAccessException {
    Field[] declaredFields = oldOne.getClass().getDeclaredFields();
    for (Field field : declaredFields) {
      field.setAccessible(true);
      field.set(newOne, field.get(oldOne));
    }
  }

  /**
   * 基础类型名字转化为类类型名字（如果有的话）
   *
   * @param name
   * @return
   */
  public static String toBoxName(String name) {
    if ("byte".equals(name)) {
      return "Byte";
    } else if ("short".equals(name)) {
      return "Short";
    } else if ("int".equals(name)) {
      return "Integer";
    } else if ("long".equals(name)) {
      return "Long";
    } else if ("float".equals(name)) {
      return "Float";
    } else if ("double".equals(name)) {
      return "Double";
    } else if ("boolean".equals(name)) {
      return "Boolean";
    } else if ("char".equals(name)) {
      return "Character";
    }
    return name;
  }

  /**
   * 类类型转化为基础类型名字（如果有的话）
   *
   * @param name
   * @return
   */
  public static String toPrimitiveName(String name) {
    if ("Byte".equals(name)) {
      return "byte";
    } else if ("Short".equals(name)) {
      return "short";
    } else if ("Integer".equals(name)) {
      return "int";
    } else if ("Long".equals(name)) {
      return "long";
    } else if ("Float".equals(name)) {
      return "float";
    } else if ("Double".equals(name)) {
      return "double";
    } else if ("Boolean".equals(name)) {
      return "boolean";
    } else if ("Character".equals(name)) {
      return "char";
    }
    return name;
  }
}
