/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author hank
 */
public final class StringUtil {

  private static final Pattern PHONE_REG = Pattern.compile("^((1[0-9][0-9]))\\d{8}$");
  private static final Pattern EMAIL_REG =
      Pattern.compile(
          "^([a-z0-9A-Z]+[-|.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
  private static final Pattern IP_REG =
      Pattern.compile(
          "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
              + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
              + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
              + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$");
  private static final Pattern IP_INNER_REG =
      Pattern.compile(
          "^((192\\.168|172\\.([1][6-9]|[2]\\d|3[01]))(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){2}|10(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){3})$");
  private static final Pattern SQL_KEY_WORD =
      Pattern.compile(
          "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
              + "(\\b(select|update|and|or|delete|insert|trancate|char|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)",
          Pattern.CASE_INSENSITIVE);
  private static final Pattern USERNAME = Pattern.compile(
      "^[a-zA-Z][a-zA-Z0-9]{5,19}$");


  private StringUtil() {
  }

  /**
   * 字符串是否为""或null
   *
   * @param value
   * @return
   */
  public static boolean empty(String value) {
    return value == null || value.trim().length() == 0;
  }

  /**
   * 字符串不为""或null
   *
   * @param value
   * @return
   */
  public static boolean notEmpty(String value) {
    return !empty(value);
  }

  /**
   * 是否是手机号码
   *
   * @param value
   * @return
   */
  public static boolean isPhoneNumber(String value) {
    return !empty(value) && matchReg(PHONE_REG, value);
  }

  /**
   * 是否是邮箱
   *
   * @param value
   * @return
   */
  public static boolean isMail(String value) {
    return !empty(value) && matchReg(EMAIL_REG, value);
  }

  /**
   * 是否是IP地址
   *
   * @param value
   * @return
   */
  public static boolean isIp4(String value) {
    return !empty(value) && matchReg(IP_REG, value);
  }

  /**
   * 是否为账号(以字母开头的字母，数字，下划线且5到19位)
   *
   * @param value
   * @return
   */
  public static boolean isUsername(String value) {
    return !empty(value) && matchReg(USERNAME, value);
  }

  /**
   * 是否是内网IP
   *
   * @param value
   * @return
   */
  public static boolean isInnerIp4(String value) {
    return !empty(value) && matchReg(IP_INNER_REG, value);
  }

  /**
   * 是否与SQL相关
   *
   * @param value
   * @return
   */
  public static boolean isSQLRelative(String value) {
    return !empty(value) && SQL_KEY_WORD.matcher(value).find();
  }

  /**
   * 是否包含中文
   *
   * @param content
   * @return
   */
  public static boolean hasChinese(String content) {
    for (char c : content.toCharArray()) {
      if (isChinese(c)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 是否为中文字符
   *
   * @param c
   * @return
   */
  public static boolean isChinese(char c) {
    return ((int) c >= 0x4e00 && (int) c <= 0x9fa5);
  }

  /**
   * 是否匹配正则表达式
   *
   * @param value
   * @param reg
   * @return
   */
  public static boolean matchReg(String value, String reg) {
    Pattern p = Pattern.compile(reg);
    return matchReg(p, value);
  }

  /**
   * 是否匹配正则表达式
   *
   * @param p
   * @param target
   * @return
   */
  public static boolean matchReg(Pattern p, String target) {
    Matcher m = p.matcher(target);
    return m.matches();
  }

  public static String[] splitBy(String source, SplitReg reg) {
    return splitBy(source, reg.getReg());
  }

  public static String[] splitBy(String source, String reg) {
    return source.split(reg);
  }

  public enum SplitReg {
    /**
     * 分号
     */
    SEMICOLON(";|；"),
    /**
     * 冒号
     */
    COLON(":|："),
    /**
     * 逗号
     */
    COMMA(",|，"),
    /**
     * 左斜杠
     */
    SLANTING_BAR("/"),
    /**
     * 竖线
     */
    Y_AXIS("\\|"),
    /**
     * 下划线
     */
    UNDERLINE("_"),
    /**
     * 横线
     */
    CROSSING("-"),
    /**
     * 井号(英镑)
     */
    POUND("\\#"),
    /**
     * @
     */
    AT("@"),
    /**
     * 空格
     */
    SPACE(" "),
    /**
     * 句号
     */
    PERIOD("\\."),
    /**
     * 并且符号
     */
    AND("&");

    private String reg;

    SplitReg(String reg) {
      this.reg = reg;
    }

    public String getReg() {
      return reg;
    }
  }
}
