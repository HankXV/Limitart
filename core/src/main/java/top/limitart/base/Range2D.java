/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;



/**
 * 范围
 *
 * @author hank
 * @version 2019/4/8 0008 19:53
 */
public class Range2D {
  private int start;
  private int end;

  public Range2D(int start, int end) {
    Conditions.args(start <= end);
    this.start = start;
    this.end = end;
  }

  //  public Range2D(int endPoint, boolean isStart) {
  //    if (isStart) {
  //      this.start = endPoint;
  //      this.end = Integer.MAX_VALUE;
  //    } else {
  //      this.start = Integer.MIN_VALUE;
  //      this.end = endPoint;
  //    }
  //  }
  //
  //  public Range2D() {
  //    this.start = Integer.MIN_VALUE;
  //    this.end = Integer.MAX_VALUE;
  //  }

  /**
   * 是否在区域中
   *
   * @param value
   * @return
   */
  public boolean in(int value) {
    return value <= end && value >= start;
  }

  /**
   * 两个区域是否重复
   *
   * @return
   */
  public boolean overlap(Range2D another) {
    return in(another.start) || in(another.end) || another.in(this.start) || another.in(this.end);
  }

  /**
   * another是否为此子集
   *
   * @param another
   * @return
   */
  public boolean contains(Range2D another) {
    return this.start <= another.start && this.end >= another.end;
  }

  /**
   * 此是否为another的子集
   *
   * @param another
   * @return
   */
  public boolean contained(Range2D another) {
    return another.contains(this);
  }
  /**
   * TODO 合并另一个
   *
   * @param another
   * @return
   */
  public boolean merge(Range2D another) {
    throw new NotImplementedException();
  }

  /**
   * TODO 去除一个范围
   *
   * @param another
   * @return
   */
  public boolean cut(Range2D another) {
    throw new NotImplementedException();
  }

  @Override
  public String toString() {
    return '[' + start + "-" + end + ']';
  }
}
