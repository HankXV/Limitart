/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.base;

import top.limitart.base.label.ThreadSafe;

import java.util.ArrayList;
import java.util.List;

/** 平滑加权轮询 Created by Hank on 2019/4/26 */
@ThreadSafe
public class SmoothRobinWeight {
  private List<Entry> entries = new ArrayList<>();

  public static void main(String[] args) {
    int[] weights = new int[] {1, 2, 3};
    SmoothRobinWeight smoothWeight = new SmoothRobinWeight(weights);
    int sum = 6;
    for (int i = 0; i < sum; i++) {
      System.out.println(smoothWeight.next());
    }
  }

  public SmoothRobinWeight(int[] weights) {
    for (int i = 0; i < weights.length; i++) {
      entries.add(new Entry(i, weights[i]));
    }
  }

  public synchronized int next() {
    int index = -1;
    int total = 0;
    for (int i = 0; i < entries.size(); ++i) {
      Entry entry = entries.get(i);
      entry.curWeight += entry.weight;
      total += entry.weight;
      if (index == -1 || entries.get(index).curWeight < entry.curWeight) {
        index = i;
      }
    }
    Entry result = entries.get(index);
    result.curWeight -= total;
    return result.index;
  }

  private static class Entry {
    private final int index;
    private final int weight;
    private int curWeight;

    public Entry(int index, int weight) {
      this.index = index;
      this.weight = weight;
    }
  }
}
