/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.base.label.Nullable;
import top.limitart.base.label.ThreadUnsafe;

/**
 * 不可变的三个
 *
 * @author hank
 */
@ThreadUnsafe
public class ImmutableTriple<L, M, R> implements Triple<L, M, R> {
  private final L left;
  private final M middle;
  private final R right;

  public ImmutableTriple(@Nullable L left, @Nullable M middle, @Nullable R right) {
    this.left = left;
    this.middle = middle;
    this.right = right;
  }

  @Override
  public String toString() {
    return "ImmutableTriple{" + "left=" + left + ", middle=" + middle + ", right=" + right + '}';
  }

  @Override
  public L getLeft() {
    return left;
  }

  @Override
  public M getMiddle() {
    return middle;
  }

  @Override
  public R getRight() {
    return right;
  }
}
