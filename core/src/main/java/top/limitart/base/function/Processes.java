/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base.function;

import top.limitart.base.label.Nullable;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * 过程函数帮助类
 *
 * @author hank
 */
public class Processes {
  public static void invoke(@Nullable Runnable runnable) {
    if (runnable != null) runnable.run();
  }

  public static void invoke(@Nullable Process0 proc) {
    if (proc != null) proc.run();
  }

  public static <T1> void invoke(@Nullable Consumer<T1> consumer, T1 t1) {
    if (consumer != null) consumer.accept(t1);
  }

  public static <T1> void invoke(@Nullable Process1<T1> proc, T1 t1) {
    if (proc != null) proc.accept(t1);
  }

  public static <T1, T2> void invoke(@Nullable Process2<T1, T2> proc, T1 t1, T2 t2) {
    if (proc != null) proc.accept(t1, t2);
  }

  public static <T1, T2> void invoke(@Nullable BiConsumer<T1, T2> biConsumer, T1 t1, T2 t2) {
    if (biConsumer != null) biConsumer.accept(t1, t2);
  }

  public static <T1, T2, T3> void invoke(@Nullable Process3<T1, T2, T3> proc, T1 t1, T2 t2, T3 t3) {
    if (proc != null) proc.run(t1, t2, t3);
  }
}
