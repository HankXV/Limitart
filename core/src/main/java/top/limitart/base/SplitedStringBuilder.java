/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.base;

/** 平凑形如 xx,xx,xx形式的字符串 Created by Hank on 2019/10/9 */
public class SplitedStringBuilder {
  private StringBuilder builder;
  private String connector;

  public SplitedStringBuilder(String connector) {
    this.builder = new StringBuilder();
    this.connector = connector;
  }

  public SplitedStringBuilder append(Object obj) {
    this.builder.append(obj).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(String str) {
    this.builder.append(str).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(StringBuffer sb) {
    this.builder.append(sb).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(CharSequence s) {
    this.builder.append(s).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(char[] str) {
    this.builder.append(str).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(boolean b) {
    this.builder.append(b).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(char c) {
    this.builder.append(c).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(int i) {
    this.builder.append(i).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(long lng) {
    this.builder.append(lng).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(float f) {
    this.builder.append(f).append(this.connector);
    return this;
  }

  public SplitedStringBuilder append(double d) {
    this.builder.append(d).append(this.connector);
    return this;
  }

  @Override
  public String toString() {
    if (this.builder.length() > 0) {
      this.builder.deleteCharAt(this.builder.length() - 1);
    }
    return this.builder.toString();
  }
}
