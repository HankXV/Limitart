/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

/**
 * 仨
 *
 * @param <L>左边
 * @param <M>中间
 * @param <R>右边
 * @see Alone
 * @see Couple
 */
public interface Triple<L, M, R> {
  static <L, M, R> Triple<L, M, R> ofImmutable(L left, M middle, R right) {
    return new ImmutableTriple<>(left, middle, right);
  }

  L getLeft();

  M getMiddle();

  R getRight();
}
