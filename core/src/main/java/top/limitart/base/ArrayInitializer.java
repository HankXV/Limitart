/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package top.limitart.base;

import top.limitart.util.CollectionUtil;

import java.util.*;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 串数初始化器(用于在声明时同时初始化)
 *
 * @author hank
 * @version 2019/4/28 0028 14:21
 */
public class ArrayInitializer {
  private List<Integer> list = new LinkedList<>();

  public static ArrayInitializer create() {
    return new ArrayInitializer();
  }

  private ArrayInitializer() {}

  public ArrayInitializer of(int... values) {
    for (int value : values) {
      list.add(value);
    }
    return this;
  }

  public ArrayInitializer of(Collection<Integer> collection) {
    list.addAll(collection);
    return this;
  }

  public ArrayInitializer range(int from, int to) {
    for (int i = from; i <= to; ++i) {
      list.add(i);
    }
    return this;
  }

  public ArrayInitializer fromInt(IntSupplier supplier) {
    list.add(supplier.getAsInt());
    return this;
  }

  public ArrayInitializer fromIntList(Supplier<List<Integer>> supplier) {
    return of(supplier.get());
  }

  public ArrayInitializer fromIntArray(Supplier<int[]> supplier) {
    return of(supplier.get());
  }

  public <K, V> Map<K, V> buildMap(
      Function<Integer, ? extends K> keyMapper, Function<Integer, ? extends V> valueMapper) {
    return this.list.stream().collect(Collectors.toMap(keyMapper, valueMapper));
  }

  public List<Integer> buildList() {
    return new ArrayList<>(this.list);
  }

  public Set<Integer> buildSet() {
    return new HashSet<>(this.list);
  }

  public int[] buildArray() {
    return CollectionUtil.toIntArray(this.list);
  }
}
