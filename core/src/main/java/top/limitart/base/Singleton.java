/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.base.function.Function0;
import top.limitart.base.label.NotNull;
import top.limitart.base.label.ThreadSafe;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 单例
 *
 * @author hank
 * @version 2018/10/8 0008 14:46
 */
@ThreadSafe
public class Singleton<T> {
  private AtomicReference<T> instanceRef = new AtomicReference<>();
  private Function0<T> confirmInstance;

  public static <T> Singleton<T> create(@NotNull Function0<T> confirmInstance) {
    return new Singleton<>(false, confirmInstance);
  }

  public static <T> Singleton<T> create(boolean lazy, @NotNull Function0<T> confirmInstance) {
    return new Singleton<>(lazy, confirmInstance);
  }

  protected Singleton(boolean lazy, @NotNull Function0<T> confirmInstance) {
    this.confirmInstance = confirmInstance;
    if (!lazy) {
      get();
    }
  }

  public T get() {
    T t = instanceRef.get();
    if(t != null){
      return t;
    }
    Conditions.notNull(
            confirmInstance,
            "singleton not initialized,maybe you should give the 'confirmInstance' or use 'getOrCreate' method");
    T newInstance = confirmInstance.get();
    confirmInstance = null;
    if(instanceRef.compareAndSet(null,newInstance)){
      return newInstance;
    }else{
      return instanceRef.get();
    }
  }

  public T getOrCreate(@NotNull Function0<T> confirmInstance) {
    this.confirmInstance = confirmInstance;
    return get();
  }
}
