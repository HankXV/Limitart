package top.limitart.base;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 快速的可观察者
 */
public class FastObservable {

  private AtomicBoolean changed = new AtomicBoolean(false);
  private List<FastObserver> obs;


  public FastObservable() {
    obs = new CopyOnWriteArrayList<>();
  }

  public void addObserver(FastObserver o) {
    Conditions.notNull(o);
    if (!obs.contains(o)) {
      obs.add(o);
    }
  }

  public void deleteObserver(FastObserver o) {
    obs.remove(o);
  }

  public void notifyObserversUnsafely(Object arg) {
    if (!hasChanged()) {
      return;
    }
    clearChanged();
    for (FastObserver ob : obs) {
      ob.update(this, arg);
    }
  }

  public void notifyObserversSafely(Object arg) {
    if (!hasChanged()) {
      return;
    }
    clearChanged();
    for (FastObserver ob : obs) {
      ob.executor().execute(() -> ob.update(this, arg));
    }
  }

  public void deleteObservers() {
    obs.clear();
  }

  protected void setChanged() {
    if (hasChanged()) {
      return;
    }
    changed.compareAndSet(false, true);
  }

  protected void clearChanged() {
    if (!hasChanged()) {
      return;
    }
    changed.compareAndSet(true, false);
  }

  public boolean hasChanged() {
    return changed.get();
  }

  public int countObservers() {
    return obs.size();
  }
}
