/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.base.function.Function0;
import top.limitart.concurrent.NotSameThreadException;

import java.util.function.Function;

/**
 * 条件判定
 *
 * @author hank
 * @version 2017/11/2 0002 20:46
 */
public class Conditions {

  /**
   * 检测是否为正数
   *
   * @param value
   */
  public static int positive(int value) {
    Conditions.args(value > 0, "value must > 0");
    return value;
  }

  /**
   * 检测是否为自然数
   *
   * @param value
   */
  public static int natural(int value) {
    Conditions.args(value >= 0, "value must >= 0");
    return value;
  }

  /**
   * 判断是否为相同线程
   *
   * @param thread
   * @param <T>
   */
  public static <T extends Thread> T sameThread(T thread) {
    notNull(thread, "thread");
    if (thread != Thread.currentThread()) {
      throw new NotSameThreadException(
          "caller must be only one,yours:%s,this:%s", thread, Thread.currentThread());
    }
    return thread;
  }

  /**
   * 判断是否为相同线程
   *
   * @param thread
   * @param template
   * @param params
   * @param <T>
   */
  public static <T extends Thread> T sameThread(T thread, String template, Object... params) {
    notNull(thread, "thread");
    if (thread != Thread.currentThread()) {
      throw new NotSameThreadException(template, params);
    }
    return thread;
  }

  /**
   * 检查元素是否为空，否则抛异常
   *
   * @param obj
   * @param <T>
   * @return
   */
  public static <T> T notNull(T obj) {
    if (obj == null) {
      throw new NullPointerException();
    }
    return obj;
  }

  /**
   * 检查元素是否为空，否则抛异常
   *
   * @param obj
   * @param template
   * @param params
   * @param <T>
   * @return
   */
  public static <T> T notNull(T obj, String template, Object... params) {
    if (obj == null) {
      throw new NullPointerException(String.format(template, params));
    }
    return obj;
  }

  /**
   * 检查参数是否正确，否则抛异常
   *
   * @param isRight
   */
  public static void args(boolean isRight) {
    if (!isRight) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * 检查参数是否正确，否则抛异常
   *
   * @param isRight
   * @param template
   * @param params
   */
  public static void args(boolean isRight, String template, Object... params) {
    if (!isRight) {
      throw new IllegalArgumentException(String.format(template, params));
    }
  }

  /**
   * 检查数组边界
   *
   * @param index
   * @param size
   */
  public static int eleIndex(int index, int size) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("index:" + index + ",size:" + size);
    }
    return index;
  }

  /**
   * 检查数组边界
   *
   * @param index
   * @param size
   * @param template
   * @param params
   */
  public static int eleIndex(int index, int size, String template, Object... params) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException(String.format(template, params));
    }
    return index;
  }

  /**
   * 如果t为空则返回other<br>
   *
   * @param t
   * @param other
   * @param <T>
   * @return
   * @see java.util.Optional
   * @deprecated 如果other是一个耗时运算，建议不要使用此方法
   */
  @Deprecated
  public static <T> T nullOrElse(T t, T other) {
    //    return Optional.ofNullable(t).orElse(other);
    return t == null ? other : t;
  }

  /**
   * 如果t为空则返回other
   *
   * @param t
   * @param other
   * @param <T>
   * @return
   * @see java.util.Optional
   */
  public static <T> T nullOrElse(T t, Function0<T> other) {
    return t == null ? other.get() : t;
    //    return Optional.ofNullable(t).orElseGet(other);
  }

  /**
   * 如果t为空则返回other
   *
   * @param t
   * @param other
   * @param <T>
   * @return
   * @see java.util.Optional
   */
  public static <T> T nullOrElse(Function0<T> t, Function0<T> other) {
    //    return Optional.ofNullable(t.get()).orElseGet(other);
    T temp = t.get();
    return temp == null ? other.get() : temp;
  }

  /**
   * 如果t为空则返回creator的创建函数，否则使用transformer转化t
   *
   * @param o
   * @param creator
   * @param <T>
   * @return
   * @see java.util.Optional
   */
  public static <O, T> T nullOrElse(O o, Function<O, T> transformer, Function0<T> creator) {
    if (o == null) {
      return creator.get();
    }
    return transformer.apply(o);
  }
}
