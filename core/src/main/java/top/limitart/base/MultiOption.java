/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.util.EnumUtil;

/**
 * 多重选择<br>
 * 假设 A选项=1，B选项=2，AB一起选就是A|B=1|2
 *
 * @param <O> 强制要求使用枚举来列出选项值
 * @author hank
 * @version 2018/12/24 0024 16:57
 */
public class MultiOption<O extends Enum<O>> {
  // 最终选项数值
  private int finalOption;

  /**
   * 复制另一个复选到当前
   *
   * @param another
   * @return
   */
  public static MultiOption of(MultiOption another) {
    MultiOption of = of();
    of.finalOption = another.finalOption;
    return of;
  }

  /**
   * 创建一个空的复选
   *
   * @return
   */
  public static MultiOption of() {
    return new MultiOption<>();
  }
  /**
   * 全部勾选
   *
   * @return
   */
  public MultiOption<O> tickAll() {
    this.finalOption = 0XFFFF;
    return this;
  }

  /**
   * 同时勾选N个选项
   *
   * @param options
   */
  public MultiOption<O> tick(O... options) {
    for (O option : options) {
      tick(option);
    }
    return this;
  }
  /**
   * 勾选一个选项
   *
   * @param option
   */
  public MultiOption<O> tick(O option) {
    check(option);
    return tick(option.ordinal());
  }

  protected MultiOption<O> tick(int... options) {
    for (int option : options) {
      tick(option);
    }
    return this;
  }

  protected MultiOption<O> tick(int option) {
    check(option);
    int value = value(option);
    //    Conditions.args(!hasTick(option), "option already tick:%s", option);
    this.finalOption |= value;
    return this;
  }

  /**
   * 重置所有选项
   *
   * @return
   */
  public MultiOption<O> clear() {
    return cancelAll();
  }
  /**
   * 取消左右勾选
   *
   * @return
   */
  public MultiOption<O> cancelAll() {
    this.finalOption = 0;
    return this;
  }

  /**
   * 取消N个选项
   *
   * @param option
   * @return
   */
  public MultiOption<O> cancel(O... option) {
    for (O o : option) {
      cancel(o);
    }
    return this;
  }

  /**
   * 取消一个勾选
   *
   * @param option
   * @return
   */
  public MultiOption<O> cancel(O option) {
    check(option);
    return cancel(option.ordinal());
  }

  protected MultiOption<O> cancel(int... option) {
    for (int i : option) {
      cancel(i);
    }
    return this;
  }

  protected MultiOption<O> cancel(int option) {
    check(option);
    int value = value(option);
    this.finalOption &= ~value;
    return this;
  }

  /**
   * 是否勾选了某选项
   *
   * @param option
   * @return
   */
  public boolean hasTick(O option) {
    check(option);
    return hasTick(option.ordinal());
  }

  /**
   * 是否勾选切仅勾选了当前选项
   *
   * @param option
   * @return
   */
  public boolean hasTickOnly(O option) {
    check(option);
    return hasTickOnly(option.ordinal());
  }

  protected boolean hasTickOnly(int option) {
    check(option);
    int value = value(option);
    return this.finalOption == value;
  }

  protected boolean hasTick(int option) {
    check(option);
    int value = value(option);
    return (value & this.finalOption) == value;
  }

  /**
   * 是否没有勾选
   *
   * @param option
   * @return
   */
  public boolean notTick(O option) {
    return !hasTick(option);
  }

  /**
   * 所指定的全部未选中
   *
   * @param options
   * @return
   */
  public boolean noneTick(O... options) {
    for (O option : options) {
      if (hasTick(option)) {
        return false;
      }
    }
    return true;
  }

  private void check(O option) {
    int optionSize = EnumUtil.length(option.getClass());
    Conditions.args(optionSize <= Integer.SIZE, "option size must under %s", Integer.SIZE);
  }

  private void check(int option) {
    Conditions.args(
        option <= Integer.SIZE && option >= 0, "option must between %s and %s", 0, Integer.SIZE);
  }

  private int value(int option) {
    return 1 << option;
  }
}
