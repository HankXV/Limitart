/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.base.label.Nullable;
import top.limitart.base.label.ThreadUnsafe;

/**
 * 持有器
 *
 * @author hank
 * @version 2017/12/18 0018 19:38
 * @see Couple
 * @see Triple
 */
@ThreadUnsafe
public class Alone<T> {
  private T t;

  public static <T> Alone of(@Nullable T t) {
    return new Alone().set(t);
  }

  public static Alone of() {
    return new Alone().set(null);
  }

  private Alone() {}

  /**
   * 获取持有的值
   *
   * @return
   */
  public @Nullable T get() {
    return this.t;
  }

  /**
   * 获取持有的值，如果为空，则返回指定值
   *
   * @param def
   * @return
   */
  public T getOrDefault(T def) {
    T t = get();
    return t == null ? def : t;
  }

  /**
   * 是否没有值
   *
   * @return
   */
  public boolean empty() {
    return get() == null;
  }

  /**
   * 是否持有值
   *
   * @return
   */
  public boolean notEmpty() {
    return !empty();
  }

  /**
   * 清空值
   *
   * @return
   */
  public Alone clear() {
    return set(null);
  }

  /**
   * 设置值
   *
   * @param t
   * @return
   */
  public Alone set(@Nullable T t) {
    this.t = t;
    return this;
  }
}
