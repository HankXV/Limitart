/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

import top.limitart.base.label.Nullable;
import top.limitart.base.label.ThreadUnsafe;

/**
 * 可变的一对
 *
 * @author hank
 */
@ThreadUnsafe
public class MutableCouple<L, R> implements Couple<L, R> {
  private L left;
  private R right;

  public MutableCouple(@Nullable L left, @Nullable R right) {
    this.left = left;
    this.right = right;
  }

  public MutableCouple() {}

  @Override
  public String toString() {
    return "MutableCouple{" + "left=" + left + ", right=" + right + '}';
  }

  @Override
  public L getLeft() {
    return this.left;
  }

  @Override
  public R getRight() {
    return this.right;
  }

  @Override
  public L getKey() {
    return getLeft();
  }

  @Override
  public R getValue() {
    return getRight();
  }

  @Override
  public R setValue(R value) {
    return this.right = value;
  }

  public void setLeft(L left) {
    this.left = left;
  }

  public void setRight(R right) {
    this.right = right;
  }
}
