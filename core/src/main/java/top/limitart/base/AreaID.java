/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

/**
 * 主要ID和次要ID生成的一个KEY(满足以下条件的应用即可)  {@code 1<=majorID<=0x7F},{@code 1<=minorID<=0xFFFF}
 *
 * @author hank
 * @version 2018/3/17 0017 12:03
 */
public interface AreaID {
  /** 主要最小 */
  byte MAJOR_MIN = 1;
  /** 次要最大 */
  int MINOR_MAX = 0xFFFF;
  /** 次要最小 */
  int MINOR_MIN = 1;

  int areaID();

  static int areaID(byte majorID, int minorID) {
    Conditions.args(
            minorID >= MINOR_MIN && minorID <= MINOR_MAX,
        "%s<=minorID<=%s,yours:%s",
            MINOR_MIN,
            MINOR_MAX,
            minorID);
    Conditions.args(majorID >= MAJOR_MIN, "majorID>=%s,yours:%s", MAJOR_MIN, majorID);
    return ((majorID & 0XFF) << 16) | ((minorID & 0xFFFF));
  }

  static int minorID(AreaID key) {
    return minorID(key.areaID());
  }

  static byte majorID(AreaID key) {
    return majorID(key.areaID());
  }

  static int minorID(int key) {
    return key & 0X00FFFF;
  }

  static byte majorID(int key) {
    return (byte) (key >> 16);
  }
}
