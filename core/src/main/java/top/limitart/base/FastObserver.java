package top.limitart.base;

import java.util.concurrent.Executor;

public interface FastObserver<O extends FastObservable> {

  Executor executor();

  void update(O o,Object arg);
}
