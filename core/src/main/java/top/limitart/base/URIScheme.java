package top.limitart.base;

import top.limitart.util.FileUtil;
import top.limitart.util.HTTPClient;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;

/**
 * 统一资源定位器协议类型
 *
 * @author hank
 * @version 2019/8/2 0002 16:16
 */
public enum URIScheme {
  HTTP("http") {
    @Override
    public byte[] fetchRes0(URI uri) throws IOException {
      return HTTPClient.download(uri);
    }
  },
  HTTPS("https") {

    @Override
    public byte[] fetchRes0(URI uri) throws IOException {
      throw new NotImplementedException();
    }
  },
  FTP("ftp") {

    @Override
    public byte[] fetchRes0(URI uri) throws IOException {
      throw new NotImplementedException();
    }
  },
  FILE("file") {

    @Override
    public byte[] fetchRes0(URI uri) throws IOException {
      return FileUtil.readFile(Paths.get(uri));
    }
  },
  ;

  URIScheme(String value) {
    this.value = value;
  }

  private String value;

  public String getValue() {
    return value;
  }

  public byte[] fetchRes(URI uri) throws IOException {
    Conditions.args(value.equals(uri.getScheme()));
    return fetchRes0(uri);
  }

  protected abstract byte[] fetchRes0(URI uri) throws IOException;

  public static URIScheme find(URI uri) {
    return find(uri.getScheme());
  }

  public static URIScheme find(String value) {
    for (URIScheme uriScheme : URIScheme.values()) {
      if (uriScheme.getValue().equals(value)) {
        return uriScheme;
      }
    }
    return null;
  }
}
