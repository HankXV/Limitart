/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.base;

/**
 * 枚举接口兼容
 *
 * @author hank
 * @version 2019/5/30 0030 17:24
 */
public interface EnumBehavior {
  /**
   * Returns the name of this enum constant, exactly as declared in its enum declaration.
   *
   * <p><b>Most programmers should use the {@link #toString} method in preference to this one, as
   * the toString method may return a more user-friendly name.</b> This method is designed primarily
   * for use in specialized situations where correctness depends on getting the exact name, which
   * will not vary from release to release.
   *
   * @return the name of this enum constant
   */
  String name();
  /**
   * Returns the ordinal of this enumeration constant (its position in its enum declaration, where
   * the initial constant is assigned an ordinal of zero).
   *
   * <p>Most programmers will have no use for this method. It is designed for use by sophisticated
   * enum-based data structures, such as {@link java.util.EnumSet} and {@link java.util.EnumMap}.
   *
   * @return the ordinal of this enumeration constant
   */
  int ordinal();
}
