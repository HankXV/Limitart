package top.limitart.base;

import top.limitart.base.label.Nullable;
import top.limitart.base.label.ThreadUnsafe;

/**
 * 可变的三个
 *
 * @author hank
 */
@ThreadUnsafe
public class MutableTriple<L, M, R> implements Triple<L, M, R> {
  private L left;
  private M middle;
  private R right;

  @Override
  public L getLeft() {
    return left;
  }

  public void setLeft(L left) {
    this.left = left;
  }

  @Override
  public M getMiddle() {
    return middle;
  }

  public void setMiddle(M middle) {
    this.middle = middle;
  }

  @Override
  public R getRight() {
    return right;
  }

  public void setRight(R right) {
    this.right = right;
  }

  public MutableTriple(@Nullable L left, @Nullable M middle, @Nullable R right) {
    this.left = left;
    this.middle = middle;
    this.right = right;
  }

  public MutableTriple() {}

  @Override
  public String toString() {
    return "MutableTriple{" + "left=" + left + ", middle=" + middle + ", right=" + right + '}';
  }
}
