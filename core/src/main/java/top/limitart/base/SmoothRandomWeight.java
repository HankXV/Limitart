/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.base;

import top.limitart.base.label.ThreadSafe;
import top.limitart.util.MathUtil;
import top.limitart.util.RandomUtil;

/** 平滑加权随机Created by Hank on 2019/4/26 */
@ThreadSafe
public class SmoothRandomWeight {
  private int[] weights;
  private int[] count;
  private int sum;
  private boolean allSame;
  // 这个值越低，低权重的就越来越难出
  private float deviationFactor;
  private float curFactor;

  public SmoothRandomWeight(int[] weights) {
    this(weights, 0.5f);
  }

  public SmoothRandomWeight(int[] weights, float deviationFactor) {
    reset(weights, deviationFactor);
  }

  private void reset(int[] weights, float deviationFactor) {
    this.weights = new int[weights.length];
    this.count = new int[weights.length];
    int sum = 0;
    boolean allSame = true;
    // 降低数据量级
    int gcd = MathUtil.gcd(weights);
    for (int i = 0; i < weights.length; i++) {
      this.weights[i] = weights[i] / gcd;
      sum += this.weights[i];
      if (this.weights[i] != this.weights[0]) {
        allSame = false;
      }
    }
    this.allSame = allSame;
    this.sum = sum;
    this.deviationFactor = MathUtil.fixedBetween(deviationFactor, 0, 1);
    this.curFactor = deviationFactor;
  }

  public synchronized void changeWeights(int[] weights) {
    if (weights.length != this.weights.length) {
      // 如果长度都不一样，直接全部重置
      reset(weights, this.deviationFactor);
    } else {
      int gcd = MathUtil.gcd(weights);
      int sum = 0;
      boolean allSame = true;
      for (int i = 0; i < weights.length; i++) {
        this.weights[i] = weights[i] / gcd;
        sum += this.weights[i];
        if (this.weights[i] != this.weights[0]) {
          allSame = false;
        }
        count[i] = Math.min(count[i], this.weights[i]);
      }
      this.sum = sum;
      this.allSame = allSame;
    }
  }

  public synchronized int next() {
    if (allSame) {
      return RandomUtil.nextInt(0, weights.length - 1);
    }
    int random = RandomUtil.nextInt(0, sum - 1);
    for (int i = 0; ; ++i) {
      random -= weights[i];
      if (count[i] / (float) weights[i] < this.curFactor) {
        if (random < 0) {
          ++count[i];
          resetCheck();
          return i;
        }
      }
      if (i == weights.length - 1) {
        this.curFactor += 0.01;
        this.curFactor = Math.min(1, this.curFactor);
        i = -1;
      }
    }
  }

  private void resetCheck() {
    for (int i = 0; i < count.length; i++) {
      if (count[i] < weights[i]) {
        return;
      } else if (count[i] > weights[i]) {
        throw new IllegalStateException("count > weight ???");
      }
    }
    for (int i = 0; i < count.length; i++) {
      count[i] = 0;
    }
    this.curFactor = this.deviationFactor;
  }
}
