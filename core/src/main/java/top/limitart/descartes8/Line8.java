/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.descartes8;

/**
 * 8方向线段 Created by Hank on 2019/6/6
 */
public class Line8 extends AbstractShape8 {

  private int step;
  private Pivot pivotType;
  private Point8 start = Point8.of(), end = Point8.of();

  public static Line8 of(Pivot pivotType, Point8 pivotPoint, Direction8 direction, int step) {
    return new Line8(pivotType, pivotPoint, direction, step);
  }

  public static Line8 of(Point8 pivotPoint, Direction8 direction, int step) {
    return new Line8(pivotPoint, direction, step);
  }

  public Line8(Point8 pivotPoint, Direction8 direction, int step) {
    this(Pivot.ENDPOINT, pivotPoint, direction, step);
  }

  public Line8(Pivot pivot, Point8 pivotPoint, Direction8 direction, int step) {
    super(direction, pivotPoint);
    this.pivotType = pivot;
    this.step = step;
    recalShape();
  }

  public Line8 step(int newStep) {
    this.step = newStep;
    recalShape();
    return this;
  }

  @Override
  protected void recalShape() {
    this.start.set(pivot().x(), pivot().y());
    this.end.set(pivot().x(), pivot().y());
    Direction8 direction = direction();
    if (this.pivotType == Pivot.ENDPOINT) {
      direction.walk(this.end, step);
    } else if (this.pivotType == Pivot.CENTER) {
      direction.walk(this.end, step);
      direction.reverse().walk(this.start, step);
    } else {
      throw new IllegalArgumentException("unsupported pivot!");
    }
  }

  public int length() {
    return this.start.distance(this.end);
  }

  @Override
  public boolean isOn(int x, int y) {
    return (x - start.x()) * (start.y() - end.y()) == (start.x() - end.x()) * (y - start.y());
  }

  public enum Pivot {
    /**
     * 末端
     */
    ENDPOINT,
    /**
     * 中间
     */
    CENTER
  }
}
