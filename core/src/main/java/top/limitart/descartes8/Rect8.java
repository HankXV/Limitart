/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.descartes8;

/** 8方向方形区域 Created by Hank on 2019/6/6 */
public class Rect8 extends AbstractShape8 {
  private Pivot pivotType;
  private int widthStep;
  private int heightStep;
  private Point8 a, b, c, d;

  public Rect8(Point8 start, Direction8 direction, int widthStep, int heightStep) {
    this(Pivot.CENTER, start, direction, widthStep, heightStep);
  }

  public Rect8(Pivot pivot, Point8 start, Direction8 direction, int widthStep, int heightStep) {
    super(direction, start);
    this.pivotType = pivot;
    this.widthStep = widthStep;
    this.heightStep = heightStep;
    recalShape();
  }

  public Rect8 whStep(int newWidthStep, int heightStep) {
    this.widthStep = newWidthStep;
    this.heightStep = heightStep;
    recalShape();
    return this;
  }
  //  a-------b
  //  |       |
  //  d-------c
  @Override
  protected void recalShape() {
    this.a = Point8.of(pivot());
    this.b = Point8.of(pivot());
    this.c = Point8.of(pivot());
    this.d = Point8.of(pivot());
    Direction8 direction = direction();
    if (this.pivotType == Pivot.CENTER) {
      direction.walk(this.a, this.heightStep);
      direction.rotateReverse(Angle8._90).walk(this.a, this.widthStep);
      direction.walk(this.b, this.heightStep);
      direction.rotate(Angle8._90).walk(this.b, this.widthStep);
      direction.reverse().walk(this.c, this.heightStep);
      direction.rotate(Angle8._90).walk(this.c, this.widthStep);
      direction.reverse().walk(this.d, this.heightStep);
      direction.rotateReverse(Angle8._90).walk(this.d, this.widthStep);
    } else if (this.pivotType == Pivot.SIDE) {
      direction.walk(this.a, this.heightStep);
      direction.rotateReverse(Angle8._90).walk(this.a, this.widthStep);
      direction.walk(this.b, this.heightStep);
      direction.rotate(Angle8._90).walk(this.b, this.widthStep);
      direction.rotate(Angle8._90).walk(this.c, this.widthStep);
      direction.rotateReverse(Angle8._90).walk(this.d, this.widthStep);
    } else if (this.pivotType == Pivot.ANGLE) {
      direction.walk(this.a, this.heightStep);
      direction.walk(this.b, this.heightStep);
      direction.rotate(Angle8._90).walk(this.b, this.widthStep);
      direction.rotate(Angle8._90).walk(this.c, this.widthStep);
    } else {
      throw new IllegalArgumentException("unsupported pivot");
    }
  }

  @Override
  public boolean isOn(int x, int y) {
    return cross(a, b, x, y) * cross(c, d, x, y) >= 0 && cross(b, c, x, y) * cross(d, a, x, y) >= 0;
  }

  private int cross(Point8 p1, Point8 p2, int x, int y) {
    return (p2.x() - p1.x()) * (y - p1.y()) - (x - p1.x()) * (p2.y() - p1.y());
  }

  public enum Pivot {
    /** 矩形正中心 */
    CENTER,
    /** 矩形边 */
    SIDE,
    /** 矩形角 */
    ANGLE
  }
}
