/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.descartes8;

/** 8方向正方形 Created by Hank on 2019/6/6 */
public class Square8 extends Rect8 {
  public Square8(Point8 start, Direction8 direction, int sideStep) {
    super(start, direction, sideStep, sideStep);
  }

  public Square8(Pivot pivot, Point8 start, Direction8 direction, int sideStep) {
    super(pivot, start, direction, sideStep, sideStep);
  }
}
