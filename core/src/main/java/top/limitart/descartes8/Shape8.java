/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.descartes8;

/**
 * 8方向形状 Created by Hank on 2019/6/5
 */
public interface Shape8 {

  default boolean inOn(Point8 point) {
    return isOn(point.x(), point.y());
  }

  /**
   * 点是否在此形状上
   *
   * @param x
   * @param y
   * @return
   */
  boolean isOn(int x, int y);

  /**
   * 旋转图形
   *
   * @param angle
   */
  void rotate(Angle8 angle);

  void direction(Direction8 direction);

  default void pivot(Point8 point) {
    pivot(point.x(), point.y());
  }

  void pivot(int x, int y);
}
