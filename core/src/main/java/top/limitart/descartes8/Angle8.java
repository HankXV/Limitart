/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.descartes8;

/**
 * 8方向的角度
 *
 * @author hank
 * @version 2019/6/5 0005 16:54
 */
public enum Angle8 {
  /**
   * 0
   */
  _0(0, true),
  /**
   * 45
   */
  _45(45, false),
  /**
   * 90
   */
  _90(90, true),
  /**
   * 135
   */
  _135(135, false),
  /**
   * 180
   */
  _180(180, true),
  /**
   * 225
   */
  _225(225, false),
  /**
   * 270
   */
  _270(270, true),
  /**
   * 315
   */
  _315(315, false),
  ;
  private final int angle;
  private final boolean standard;

  Angle8(int angle, boolean standard) {
    this.angle = angle;
    this.standard = standard;
  }

  public boolean isStandard() {
    return standard;
  }

  public int value() {
    return angle;
  }

  /**
   * 角度差
   *
   * @param angle
   * @return
   */
  public Angle8 delta(Angle8 angle) {
    return valueOf(value() - angle.value());
  }

  /**
   * 反向
   *
   * @return
   */
  public Angle8 reverse() {
    return rotate(Angle8._180);
  }

  /**
   * 逆时针旋转
   *
   * @param angle
   * @return
   */
  public Angle8 rotateReverse(Angle8 angle) {
    return rotate(-angle.value() / _45.value());
  }

  /**
   * 旋转指定角度（顺时针）
   *
   * @param angle
   * @return
   */
  public Angle8 rotate(Angle8 angle) {
    return rotate(angle.value() / _45.value());
  }

  /**
   * 顺时针旋转一个单位
   *
   * @return
   */
  public Angle8 next() {
    return rotate(Angle8._45);
  }

  /**
   * 逆时针旋转一个单位
   *
   * @return
   */
  public Angle8 last() {
    return rotateReverse(Angle8._45);
  }

  /**
   * 旋转
   *
   * @param angleUnit 几个单位（45度为一个单位）
   * @return
   */
  public Angle8 rotate(int angleUnit) {
    return valueOf(value() + _45.value() * angleUnit);
  }

  private static Angle8 valueOf(int angle) {
    int t = angle % 360;
    if (t < 0) {
      t += 360;
    }
    switch (t) {
      case 0:
        return _0;
      case 45:
        return _45;
      case 90:
        return _90;
      case 135:
        return _135;
      case 180:
        return _180;
      case 225:
        return _225;
      case 270:
        return _270;
      case 315:
        return _315;
    }
    throw new IllegalArgumentException("illegal angle");
  }
}
