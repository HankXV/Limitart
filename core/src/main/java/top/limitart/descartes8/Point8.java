/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.descartes8;

/**
 * 单位点
 *
 * @author hank
 * @version 2019/6/5 0005 20:07
 */
public final class Point8 implements Descartes {

  private int x, y;

  public static Point8 of(int x, int y) {
    return new Point8(x, y);
  }

  public static Point8 of(Point8 point) {
    return new Point8(point);
  }

  public static Point8 of() {
    return new Point8();
  }

  public Point8(Point8 point) {
    this(point.x(), point.y());
  }

  public Point8(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public Point8() {
  }

  public int x() {
    return this.x;
  }

  public int y() {
    return this.y;
  }

  /**
   * 按照指定锚点旋转
   * @param pivot 锚点
   * @param angle 角度
   */
  public void rotate(Point8 pivot, Angle8 angle) {
    switch (angle){
      case _0:
        //DO NOTHING
        break;
      case _45:

        break;
      case _90:
        break;
      case _135:
        break;
      case _180:
        break;
      case _225:
        break;
      case _270:
        break;
      case _315:
        break;
    }
  }

  public Point8 x(int x) {
    this.x = x;
    return this;
  }

  public Point8 y(int y) {
    this.y = y;
    return this;
  }

  public Point8 add(int x, int y) {
    this.x += x;
    this.y += y;
    return this;
  }

  public Point8 add(Point8 another) {
    return add(another.x(), another.y());
  }

  public Point8 set(int x, int y) {
    return x(x).y(y);
  }

  public Point8 set(Point8 another) {
    return set(another.x(), another.y());
  }

  /**
   * 距离(并非斜边距离，是X坐标和Y坐标距离取最大的一个)
   *
   * @param another
   * @return
   */
  public int distance(Point8 another) {
    return distance(another.x(), another.y());
  }

  /**
   * 距离(并非斜边距离，是X坐标和Y坐标距离取最大的一个)
   *
   * @param anotherX
   * @param anotherY
   * @return
   */
  public int distance(int anotherX, int anotherY) {
    return distance(x(), y(), anotherX, anotherY);
  }

  /**
   * 距离(并非斜边距离，是X坐标和Y坐标距离取最大的一个)
   *
   * @param x1
   * @param y1
   * @param x2
   * @param y2
   * @return
   */
  public static int distance(int x1, int y1, int x2, int y2) {
    return Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2));
  }

  @Override
  public String toString() {
    return "[" +
        "x=" + x +
        ", y=" + y +
        ']';
  }
}
