/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.descartes8;

/**
 * 方向
 *
 * @author hank
 * @version 2019/6/5 0005 16:54
 */
public enum Direction8 {
  /** 上 */
  UP(Angle8._0) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(0, step);
    }
  },
  /** 右上 */
  RIGHT_UP(Angle8._45) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(step, step);
    }
  },
  /** 右 */
  RIGHT(Angle8._90) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(step, 0);
    }
  },
  /** 右下 */
  RIGHT_DOWN(Angle8._135) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(step, -step);
    }
  },
  /** 下 */
  DOWN(Angle8._180) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(0, -step);
    }
  },
  /** 左下 */
  LEFT_DOWN(Angle8._225) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(-step, -step);
    }
  },
  /** 左 */
  LEFT(Angle8._270) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(-step, 0);
    }
  },
  /** 左上 */
  LEFT_UP(Angle8._315) {
    @Override
    public void walk(Point8 point, int step) {
      point.add(-step, step);
    }
  },
  ;
  private final Angle8 angle;

  Direction8(Angle8 angle) {
    this.angle = angle;
  }

  public Angle8 angle() {
    return angle;
  }

  /**
   * 按照此方向行走
   *
   * @param point
   * @param step
   */
  public abstract void walk(Point8 point, int step);
  /**
   * 顺时针旋转
   *
   * @param angle
   * @return
   */
  public Direction8 rotate(Angle8 angle) {
    return valueOf(angle().rotate(angle));
  }

  /**
   * 逆时针旋转
   *
   * @param angle
   * @return
   */
  public Direction8 rotateReverse(Angle8 angle) {
    return valueOf(angle().rotateReverse(angle));
  }

  /**
   * 反向
   *
   * @return
   */
  public Direction8 reverse() {
    return valueOf(angle().reverse());
  }

  public static Direction8 valueOf(Angle8 angle) {
    switch (angle) {
      case _0:
        return UP;
      case _45:
        return RIGHT_UP;
      case _90:
        return RIGHT;
      case _135:
        return RIGHT_DOWN;
      case _180:
        return DOWN;
      case _225:
        return LEFT_DOWN;
      case _270:
        return LEFT;
      case _315:
        return LEFT_UP;
    }
    throw new IllegalArgumentException("illegal angle");
  }
}
