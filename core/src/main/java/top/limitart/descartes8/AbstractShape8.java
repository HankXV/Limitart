/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.descartes8;

/**
 * 抽象形状
 *
 * @author hank
 * @version 2019/6/6 0006 14:59
 */
public abstract class AbstractShape8 implements Shape8 {
  private Direction8 direction;
  private Point8 pivot;

  public AbstractShape8(Direction8 direction, Point8 pivot) {
    this.direction = direction;
    this.pivot = Point8.of(pivot);
  }

  public Direction8 direction() {
    return direction;
  }

  public Point8 pivot() {
    return pivot;
  }

  @Override
  public void rotate(Angle8 angle) {
    direction(direction.rotate(angle));
  }

  @Override
  public void direction(Direction8 direction) {
    this.direction = direction;
    recalShape();
  }

  @Override
  public void pivot(int x, int y) {
    pivot.set(x, y);
    recalShape();
  }

  protected abstract void recalShape();
}
