/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.singleton;

/**
 * 特殊单例初始化<br>
 * 需要在普通Singleton之前初始化或难以以普通方式构造的Singleton使用此方法构造<br>
 * Created by Hank on 2019/5/5
 */
public interface SingletonInitializer {
  void registerManual(Singletons.Builder builder) throws Exception;
}
