/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.singleton;

import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.function.Process1;
import top.limitart.util.ReflectionUtil;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 单例容器入口
 *
 * @author Hank
 */
public class Singletons {

  private static final Logger LOGGER = LoggerFactory.getLogger(Singletons.class);
  private final Map<Class<?>, Object> instances = new ConcurrentHashMap<>();

  public static Builder builder() {
    return new Builder();
  }

  private Singletons(Builder builder) {
    instances.putAll(builder.instances);
    injectByConstructor(builder.refConstructors, Integer.MAX_VALUE, false);
    for (Object obj : instances.values()) {
      injectByFieldAndMethod(obj);
    }
  }

  /**
   * 获取实例
   *
   * @param clazz
   * @param <T>
   * @return
   */
  public <T> T instance(Class<T> clazz) {
    if (instances.containsKey(clazz)) {
      return (T) instances.get(clazz);
    }
    return null;
  }

  /**
   * 为任意实例注入
   *
   * @param object
   */
  public void injectForInstance(Object object) {
    injectByFieldAndMethod(object);
  }

  public <T> T createBean(Class<T> clazz) throws IllegalAccessException, InstantiationException {
    Constructor result = null;
    for (Constructor<?> constructor : clazz.getConstructors()) {
      Ref ref = constructor.getAnnotation(Ref.class);
      if (ref == null) {
        continue;
      }
      Conditions.args(
          result == null,
          "class %s has more constructors with @Ref,witch to use?",
          clazz.getName());
      result = constructor;
    }
    T t;
    if (result == null) {
      t = clazz.newInstance();
    } else {
      Map<Class<?>, Constructor> map = Collections.singletonMap(clazz, result);
      t = (T) injectByConstructor(map, Integer.MAX_VALUE, true);
    }
    injectByFieldAndMethod(t);
    return t;
  }

  private Object injectByConstructor(Map<Class<?>, Constructor> classes, int lastCount,
      boolean returnFast) {
    if (classes.isEmpty()) {
      return null;
    }
    int oldCount = classes.size();
    if (lastCount <= oldCount) {
      StringBuilder builder = new StringBuilder();
      int count = 0;
      for (Class<?> aClass : classes.keySet()) {
        if (count > 2) {
          break;
        }
        builder.append(aClass.getName()).append(";");
      }
      builder.deleteCharAt(builder.length() - 1);
      Conditions.args(false, "may be circle ref on %s", builder.toString());
    }
    Iterator<Map.Entry<Class<?>, Constructor>> iterator = classes.entrySet().iterator();
    for (; iterator.hasNext(); ) {
      Map.Entry<Class<?>, Constructor> next = iterator.next();
      Class<?> key = next.getKey();
      Constructor value = next.getValue();
      // 检查参数
      Object[] args = new Object[value.getParameterCount()];
      boolean hasNull = false;
      for (int i = 0; i < value.getParameterTypes().length; i++) {
        Class<?> parameterType = value.getParameterTypes()[i];
        //        Conditions.args(
        //            parameterType.getAnnotation(Singleton.class) != null,
        //            "class %s parameter %s must be @Singleton",
        //            key.getName(),
        //            parameterType.getName());
        Object o = instances.get(parameterType);
        if (o != null) {
          args[i] = o;
        } else {
          hasNull = true;
          break;
        }
      }
      if (hasNull) {
        // 注入失败，等待下一次递归处理
        continue;
      }
      try {
        Object o = value.newInstance(args);
        if (returnFast) {
          return o;
        }
        Conditions.args(instances.put(key, o) == null, "class %s duplicated!", value.getName());
        LOGGER.trace("find singleton class:{}", key.getName());
        iterator.remove();
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
        Conditions.args(
            false,
            "can not construct class %s from constructor %s",
            key.getName(),
            value.getName());
      }
    }
    injectByConstructor(classes, oldCount, returnFast);
    return null;
  }

  private void injectByFieldAndMethod(Object obj) {
    Field[] fields = obj.getClass().getDeclaredFields();
    for (Field field : fields) {
      Ref annotation = field.getAnnotation(Ref.class);
      if (annotation == null) {
        continue;
      }
      if (!field.isAccessible()) {
        field.setAccessible(true);
      }
      Class<?> type = field.getType();
      if (!instances.containsKey(type)) {
        throw new NullPointerException("can not find singleton :" + type.getName());
      }
      try {
        field.set(obj, instances.get(type));
      } catch (IllegalAccessException e) {
        LOGGER.error("reflect error", e);
      }
      LOGGER.trace("inject field {} into {}", type.getName(), obj.getClass().getName());
    }
    for (Method method : obj.getClass().getDeclaredMethods()) {
      Ref annotation = method.getAnnotation(Ref.class);
      if (annotation == null) {
        continue;
      }
      // 检查参数
      Object[] args = new Object[method.getParameterCount()];
      for (int i = 0; i < method.getParameterTypes().length; i++) {
        Class<?> parameterType = method.getParameterTypes()[i];
        //        Conditions.args(
        //            parameterType.getAnnotation(Singleton.class) != null,
        //            "class %s mathod %s , parameter %s must be @Singleton",
        //            obj.getClass().getName(),
        //            ReflectionUtil.getMethodOverloadName(method),
        //            parameterType.getName());
        Object o = instances.get(parameterType);
        Conditions.notNull(
            o,
            "class %s mathod %s , parameter %s can not find instance to inject",
            obj.getClass().getName(),
            ReflectionUtil.getMethodOverloadName(method),
            parameterType.getName());
        args[i] = o;
      }
      if (!method.isAccessible()) {
        method.setAccessible(true);
      }
      try {
        method.invoke(obj, args);
      } catch (IllegalAccessException | InvocationTargetException e) {
        Conditions.args(
            false,
            "class %s mathod %s inject fail",
            obj.getClass().getName(),
            ReflectionUtil.getMethodOverloadName(method));
      }
    }
  }

  public void forEach(Process1<Object> proc) {
    instances.values().forEach(proc::accept);
  }

  public static class Builder {

    private final Map<Class<?>, Object> instances = new ConcurrentHashMap<>();
    private final Map<Class<?>, Constructor> refConstructors = new ConcurrentHashMap<>();

    public Singletons build() {
      return new Singletons(this);
    }

    /**
     * 绑定一个指定的实例
     *
     * @param who
     * @param t
     * @param <T>
     * @param <S>
     * @return
     */
    public <T, S extends T> T bind(Class<T> who, S t) {
      putInstance(who, t);
      return t;
    }

    /**
     * 绑定一个类并自动生成实例
     *
     * @param who
     * @param <T>
     * @return
     */
    public <T> Builder bind(Class<T> who) {
      putInstance(who);
      return this;
    }

    /**
     * 使用初始化器绑定
     *
     * @param initializer
     * @return
     */
    public Builder withInitializer(SingletonInitializer initializer) throws Exception {
      initializer.registerManual(this);
      return this;
    }

    /**
     * 扫描指定包下所有的单例
     *
     * @param packageName
     * @param classLoader
     * @return
     */
    public Builder withPackage(String packageName, ClassLoader classLoader) {
      // 寻找所有有@Singleton注解的类
      List<Class<?>> classes;
      try {
        classes = Conditions.notNull(ReflectionUtil.getClasses(packageName, classLoader, null));
      } catch (IOException | ClassNotFoundException e) {
        LOGGER.error("reflect error", e);
        return this;
      }
      for (Class<?> clazz : classes) {
        Singleton annotation = clazz.getAnnotation(Singleton.class);
        if (annotation == null) {
          continue;
        }
        putInstance(clazz);
        LOGGER.trace("find singleton class:{}", clazz.getName());
      }
      return this;
    }

    /**
     * 扫描指定类的类加载器中所有的单例
     *
     * @param initialClass
     * @return
     */
    public Builder withClassStarter(Class<?> initialClass) {
      return withPackage(initialClass.getPackage().getName(), initialClass.getClassLoader());
    }

    private void putInstance(Class<?> who, Object obj) {
      Conditions.args(instances.put(who, obj) == null, "class %s duplicated!", who.getName());
    }

    private void putInstance(Class<?> clazz) {
      boolean findRefConstructor = false;
      for (Constructor<?> constructor : clazz.getConstructors()) {
        Ref ref = constructor.getAnnotation(Ref.class);
        if (ref == null) {
          continue;
        }
        Conditions.args(
            !refConstructors.containsKey(clazz),
            "class %s has more constructors with @Ref,witch to use?",
            clazz.getName());
        refConstructors.put(clazz, constructor);
        findRefConstructor = true;
      }
      if (findRefConstructor) {
        return;
      }
      try {
        putInstance(clazz, clazz.newInstance());
      } catch (InstantiationException | IllegalAccessException e) {
        Conditions.args(
            false, "can not create instance of %s from an empty constructor", clazz.getName());
      }
    }
  }
}
