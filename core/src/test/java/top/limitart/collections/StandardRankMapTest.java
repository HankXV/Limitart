package top.limitart.collections;//package top.limitart.collections;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import top.limitart.base.CompareChain;
//
//import java.util.Comparator;
//
///**
// * @author hank
// * @version 2019/7/4 0004 14:17
// */
//public class StandardRankMapTest {
//  private Comparator<Meta> comparator =
//      CompareChain.build((o1, o2) -> CompareChain.start(o1.num, o2.num));
//
//  private class Meta implements RankMap.LongRankObj {
//    private long id;
//    private int num;
//
//    public Meta(long id, int num) {
//      this.id = id;
//      this.num = num;
//    }
//
//    public int getNum() {
//      return num;
//    }
//
//    public void setNum(int num) {
//      this.num = num;
//    }
//
//    @Override
//    public Long key() {
//      return this.id;
//    }
//  }
//
//  private RankMap<Long, Meta> map = RankMap.create(comparator);
//  //  private RankMap<Long, Meta> skipMap = SkipRankMap.create(comparator);
//
//  //  @Test
//  //  public void performance() {
//  //    int sum = 10000;
//  //    System.out.println("插入效率");
//  //    TimeUtil.performanceTest(
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            skipMap.putIfAbsent(new Meta(i, sum - i));
//  //          }
//  //        },
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            map.putIfAbsent(new Meta(i, sum - i));
//  //          }
//  //        },
//  //        1);
//  //    System.out.println("按Key更新效率");
//  //    TimeUtil.performanceTest(
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            int finalI = i;
//  //            skipMap.update((long) i, e -> e.num = finalI);
//  //          }
//  //        },
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            int finalI = i;
//  //            map.update((long) i, e -> e.num = finalI);
//  //          }
//  //        },
//  //        1);
//  //    System.out.println("按Value更新效率");
//  //    TimeUtil.performanceTest(
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            int finalI = i;
//  //            skipMap.update(new Meta(i, i), e -> e.num = (sum - finalI));
//  //          }
//  //        },
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            int finalI = i;
//  //            map.update(new Meta(i, i), e -> e.num = (sum - finalI));
//  //          }
//  //        },
//  //        1);
//  //    System.out.println("删除效率");
//  //    TimeUtil.performanceTest(
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            skipMap.remove(new Meta(i, sum - i));
//  //          }
//  //        },
//  //        () -> {
//  //          for (int i = 0; i < sum; ++i) {
//  //            map.remove((long) i);
//  //          }
//  //        },
//  //        1);
//  //    Assert.assertTrue(skipMap.size() == 0);
//  //    Assert.assertTrue(map.size() == 0);
//  //  }
//
//  @Before
//  public void setUp() throws Exception {}
//
//  @Test
//  public void get() throws Exception {
//    for (int i = 0; i < 10000000; ++i) {
//      Assert.assertNotNull(map.get((long) i));
//    }
//  }
//
//  @Test
//  public void containsKey() throws Exception {
//    for (int i = 0; i < 10000000; ++i) {
//      Assert.assertTrue(map.containsKey((long) i));
//    }
//    Assert.assertFalse(map.containsKey(-1l));
//  }
//
//  @Test
//  public void remove() throws Exception {
//    map.putIfAbsent(new Meta(-1, 0));
//    Assert.assertNotNull(map.remove(-1l));
//  }
//
//  @Test
//  public void update() throws Exception {
//    map.update(1L, v -> v.num = -1);
//    Assert.assertTrue(map.get(1L).num == -1);
//  }
//
//  @Test
//  public void putIfAbsent() throws Exception {
//    Assert.assertNotNull(map.putIfAbsent(new Meta(2, 2)));
//    Assert.assertNull(map.putIfAbsent(new Meta(-3, -3)));
//  }
//
//  @Test
//  public void updateOrPut() throws Exception {}
//
//  @Test
//  public void clear() throws Exception {}
//
//  @Test
//  public void size() throws Exception {
//    Assert.assertTrue(map.size() == 10000000);
//  }
//
//  @Test
//  public void getIndex() throws Exception {}
//
//  @Test
//  public void getAll() throws Exception {}
//
//  @Test
//  public void getRange() throws Exception {}
//
//  @Test
//  public void getAt() throws Exception {}
//}
