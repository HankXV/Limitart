package top.limitart.project;

import top.limitart.base.Singleton;
import top.limitart.concurrent.TaskQueue;
import top.limitart.redefinable.FakeRedefinableApplication;
import top.limitart.redefinable.RedefinableApplication;

import java.io.File;
import java.net.URI;
import java.util.concurrent.TimeUnit;

/**
 * 该类为程序启动的入口
 */
public class MainApplication {
    private static MainApplicationHolder INSTANCE;

    //由main函数来hold住全场
    public static void main(String[] args) throws Exception {
        //这里建议设置一个参数，boolean debug = args[0]  来判定当前是生产环境还是开发环境.
        //因为开发环境需要断点调式，所以我们用此项目直接引用project-logic，然后不通过接口调用，直接new出logic模块的入口类，手动调用，如下
        boolean debug = false;
        if (args.length > 0) {
            debug = args[0].equals("debug");
        }
        INSTANCE = new MainApplicationHolder(debug);
        //启动程序！
        getRedefinable().run(args);
        //这里启动一个线程来不停重加载一遍测试，这里只是演示，实际请不要这么做
        TaskQueue queue = TaskQueue.create("redefine-test");
        queue.scheduleAtFixedRate(()->{
            getRedefinable().reload();
        },0L, 5, TimeUnit.SECONDS);
    }

    public static RedefinableApplication getRedefinable() {
        return INSTANCE.get();
    }

    public static class MainApplicationHolder extends Singleton<RedefinableApplication> {

        protected MainApplicationHolder(boolean debug) {
            //您可以用其他方式构造，反正达到这个目的就行
            super(false, () -> {
                if (debug) {
                    //直接在本ClassLoader中加载入口类
                    return new FakeRedefinableApplication("top.limitart.project.LogicEntrance");
                } else {
                    //先加载jar包，再加载入口类
                    return new RedefinableApplication(new File(System.getProperty("user.dir")+ File.separator+"project-logic.jar").toURI(), "top.limitart.project.LogicEntrance");
                }
            });
        }
    }
}
